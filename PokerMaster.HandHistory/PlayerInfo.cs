﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokerMaster.HandHistory
{
    public class PlayerInfo
    {
        public int Seat { get; set; }
        public string Name { get; set; }
        public bool IsHero { get; set; }
        public bool IsButton { get; set; }
        public Card[] DealtCards { get; set; }
        public long InitialStack { get; set; }
        public long TotalBet { get; set; }
        public long EndStack { get; set; }
        public bool SurvivedTillShowdown { get; set; }
        public bool Acted { get; set; }
    }
}
