﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokerMaster.HandHistory
{
    public class Card
    {
        public static readonly char[] Suits = {'s', 'h', 'c', 'd'};
        public static readonly string[] Ranks = { "A", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K" };

        public int Suit { get; set; } = -1;
        public int Rank { get; set; } = -1;

        public override string ToString()
        {
            if (Suit < 0 || Rank < 0 || Suit >= Suits.Length || Rank >= Ranks.Length) return "None";

            return $"{Ranks[Rank]}{Suits[Suit]}";
        }
    }
}
