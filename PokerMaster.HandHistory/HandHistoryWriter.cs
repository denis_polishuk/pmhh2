﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokerMaster.HandHistory
{
    public class HandHistoryWriter
    {
        private const string DateTimeFormat = "yyyy/M/d H:mm:ss";
        private bool clearHero = false;

        private void WriteHeader(HandInfo hand, StringBuilder builder)
        {
            // Game started at: 2017/7/10 4:27:10
            builder.Append("Game started at: ");
            builder.AppendLine(hand.StartedAt.ToString(DateTimeFormat, CultureInfo.InvariantCulture));

            // Game ID: 954832911 750/1500 (PRR) PokerMaster (Hold'em)
            builder.Append("Game ID: ");
            builder.Append(hand.Id);
            builder.Append(" ");
            builder.Append(hand.SmallBlind);
            builder.Append("/");
            builder.Append(hand.BigBlind);
            builder.Append(" (PRR) PokerMaster id");
            builder.Append(hand.Id/10000);
            if (hand.isOmaha)
            {
                builder.AppendLine(" (Omaha)");
            }
            else
            {
                builder.AppendLine(" (Hold'em)");
            }
            

            // Seat 3 is the button
            builder.Append("Seat ");
            builder.Append(hand.Players.Single(x => x.IsButton).Seat);
            builder.AppendLine(" is the button");
        }

        private void WritePlayers(HandInfo hand, StringBuilder builder)
        {
            // Seat 1: bigotes1957(57386).
            foreach (var p in hand.Players)
            {
                builder.Append("Seat ");
                builder.Append(p.Seat);
                builder.Append(": ");
                builder.Append(p.Name);
                builder.Append(" (");
                builder.Append(p.InitialStack);
                builder.AppendLine(").");
            }
        }

        private void WriteBlinds(HandInfo hand, StringBuilder builder)
        { 
            // Player sabberrr ante(150)
            foreach (var a in hand.Actions.Where(x => x.Action == ActionType.PostAnte))
            {
                builder.Append("Player ");
                builder.Append(a.Player.Name);
                builder.Append(" ante (");
                builder.Append(a.Amount);
                builder.AppendLine(")");
            }

            // Player sabberrr has small blind(750)
            var sb = hand.Actions.SingleOrDefault(x => x.Action == ActionType.PostSB);
            if (sb != null)
            {
                builder.Append("Player ");
                builder.Append(sb.Player.Name);
                builder.Append(" has small blind (");
                builder.Append(sb.Amount);
                builder.AppendLine(")");
            }

            // Player Ogun 88 has big blind (1500)
            var bb = hand.Actions.SingleOrDefault(x => x.Action == ActionType.PostBB);
            if (bb != null)
            {
                builder.Append("Player ");
                builder.Append(bb.Player.Name);
                builder.Append(" has big blind (");
                builder.Append(bb.Amount);
                builder.AppendLine(")");
            }
            else throw new Exception("No PostBB action found");

            // Player JacksBnimble13 straddle (4)
            var straddle = hand.Actions.SingleOrDefault(x => x.Action == ActionType.PostStraddle);
            if (straddle != null)
            {
                builder.Append("Player ");
                builder.Append(straddle.Player.Name);
                builder.Append(" straddle (");
                builder.Append(straddle.Amount);
                builder.AppendLine(")");
            }
        }

        private void WriteCardsDealt(HandInfo hand, StringBuilder builder)
        {
            int startWith = -1;
            for (int i = 0; i < hand.Players.Length; i++)
            {
                if (hand.Players[i].IsButton)
                {
                    startWith = (i + 1) % hand.Players.Length;
                    break;
                }
            }

            int playerCardsAmount = 2;
            if (hand.isOmaha)
            {
                playerCardsAmount = 4;
            }

            for (int i = 0; i < hand.Players.Length; i++)
            {
                var index = (startWith + i)%hand.Players.Length;
                var player = hand.Players[index];

                // TODO check if cards were actually dealt
                if ((player.DealtCards != null)&&(!clearHero))
                {
                    for (int card = 0; card < playerCardsAmount; card++)
                    {
                        builder.Append("Player ");
                        builder.Append(player.Name);
                        builder.Append(" received card: [");
                        builder.Append(player.DealtCards[card]);
                        builder.AppendLine("]");
                    }
                }
                else
                {
    

                    for (int card = 0; card < playerCardsAmount; card++)
                    {
                        // Player missBahamas received a card.
                        builder.Append("Player ");
                        builder.Append(player.Name);
                        builder.AppendLine(" received a card.");
                    }
                }
            }
        }

        private void WriteBoard(HandInfo hand, GameStage stage, StringBuilder builder)
        {
            switch (stage)
            {
                // *** FLOP ***: [6s 8c 4h]
                case GameStage.Flop:
                    if (hand.Flop != null)
                        builder.AppendLine(
                            $"*** FLOP ***: [{string.Join(" ", hand.Flop.Select(x => x.ToString()))}]");
                    break;
                // *** TURN ***: [6s 8c 4h] [9s]
                case GameStage.Turn:
                    if (hand.Turn != null)
                    {
                        builder.Append(
                            $"*** TURN ***: [{string.Join(" ", hand.Flop.Select(x => x.ToString()))}]");
                        builder.Append(" [");
                        builder.Append(hand.Turn);
                        builder.AppendLine("]");
                    }
                    break;
                // *** RIVER ***: [6s 8c 4h 9s] [9d]
                case GameStage.River:
                    if (hand.River != null)
                    {
                        builder.Append(
                            $"*** RIVER ***: [{string.Join(" ", hand.Flop.Select(x => x.ToString()))}");
                        builder.Append(" ");
                        builder.Append(hand.Turn);
                        builder.Append("] [");
                        builder.Append(hand.River);
                        builder.AppendLine("]");
                    }
                    break;
            }
        }

        private void WriteMucks(HandInfo hand, StringBuilder builder)
        {
            foreach (var s in hand.ShowDowns)
            {
                if (!s.Shows)
                {
                    // Player imAAllin mucks cards
                    builder.Append("Player ");
                    builder.Append(s.Player.Name);
                    builder.AppendLine(" mucks cards");
                }
            }
        }

        private long GetUncalledBet(HandInfo hand, out PlayerInfo player)
        {
            var maxBet = hand.Players.Max(x => x.TotalBet);
            var playersWithMaxBet = hand.Players.Where(x => x.TotalBet == maxBet).ToList();

            player = null;

            if (playersWithMaxBet.Count == 1)
            {
                player = playersWithMaxBet[0];
                var secondLargestBet = hand.Players.Where(x => x != playersWithMaxBet[0]).Max(x => x.TotalBet);

                return player.TotalBet - secondLargestBet;
            }

            return 0;
        }

        private void WriteUncalledBet(HandInfo hand, StringBuilder builder)
        {
            PlayerInfo player;
            var uncalledBet = GetUncalledBet(hand, out player);

            if (uncalledBet > 0)
            {
                // Uncalled bet (16) returned to AlexPoc
                builder.Append("Uncalled bet (");
                builder.Append(uncalledBet);
                builder.Append(") returned to ");
                builder.AppendLine(player.Name);
            }
        }

        private void WriteStageActions(List<PlayerAction> stageActions, StringBuilder builder)
        {
            // Player JustFoldBaby raises(3015)
            foreach (var a in stageActions)
            {
                builder.Append("Player ");
                builder.Append(a.Player.Name);
                builder.Append(" ");

                switch (a.Action)
                {
                    case ActionType.Fold:
                        builder.Append("folds");
                        break;
                    case ActionType.Bet:
                        builder.Append("bets");
                        break;
                    case ActionType.Check:
                        builder.Append("checks");
                        break;
                    case ActionType.Call:
                        builder.Append("calls");
                        break;
                    case ActionType.Raise:
                        builder.Append("raises");
                        break;
                    case ActionType.AllIn:
                        builder.Append("allin");
                        break;
                    default:
                        throw new Exception($"Unexpected action: {a.Action}");
                }

                if (a.Amount > 0)
                {
                    builder.Append(" ");
                    builder.Append("(");
                    builder.Append(a.Amount);
                    builder.Append(")");
                }

                builder.AppendLine();
            }
        }

        private void WriteActions(HandInfo hand, StringBuilder builder)
        {
            var stages = new[] { GameStage.Preflop, GameStage.Flop, GameStage.Turn, GameStage.River };

            var actions = hand.Actions.Where(x =>
                   x.Action != ActionType.PostAnte &&
                   x.Action != ActionType.PostSB &&
                   x.Action != ActionType.PostBB &&
                   x.Action != ActionType.PostStraddle
                   ).ToList();

            int actionsHandled = 0;

            bool footerWritten = false;

            foreach (var stage in stages)
            {
                if (!footerWritten && actionsHandled >= actions.Count)
                {
                    // Uncalled bet (16) returned to AlexPoc
                    WriteUncalledBet(hand, builder);

                    // Player imAAllin mucks cards
                    WriteMucks(hand, builder);

                    footerWritten = true;
                }

                // *** TURN ***: [6s 8c 4h] [9s]
                WriteBoard(hand, stage, builder);

                var stageActions = actions.Where(x => x.Stage == stage).ToList();

                // Player JustFoldBaby raises(3015)
                WriteStageActions(stageActions, builder);

                actionsHandled += stageActions.Count;
            }

            if (!footerWritten)
            {
                // Uncalled bet (16) returned to AlexPoc
                WriteUncalledBet(hand, builder);

                // Player imAAllin mucks cards
                WriteMucks(hand, builder);
            }
        }

        private string GetCombination(HandInfo hand, Card[] cards, bool withKicker = false)
        {
            if (cards.Length == 4)
            {
                return Combinations.GetOmahaCombination(cards, hand.Board, withKicker);
            }
            return Combinations.GetCombination(cards, hand.Board, withKicker);
        }

        private void WriteSummary(HandInfo hand, StringBuilder builder)
        {
            builder.AppendLine("------ Summary ------");

            PlayerInfo uncalledBetPlayer;
            var uncalledBet = GetUncalledBet(hand, out uncalledBetPlayer);

            // Pot: 22140
            builder.Append("Pot: ");
            builder.Append(hand.Pot - uncalledBet);
            builder.AppendLine();

            if (hand.Board != null)
            {
                // Board: [6h 5d 7c 6d 10c]
                builder.Append("Board: [");
                builder.Append(string.Join(" ", hand.Board.Select(x => x.ToString())));
                builder.AppendLine("]");
            }

            var allCombinations = hand.ShowDowns
                .Where(x => x.Cards != null)
                .Select(x => GetCombination(hand, x.Cards))
                .ToList();

            foreach (var player in hand.Players)
            {
                // *Player LeitenantGlan mucks (does not show cards). Bets: 2. Collects: 4. Wins: 2.
                if (player.EndStack > player.InitialStack)
                    builder.Append("*");

                builder.Append("Player ");
                builder.Append(player.Name);
                builder.Append(" ");

                if ((player.SurvivedTillShowdown)||
                    (player.IsHero && clearHero))
                {
                    var showdown = hand.ShowDowns.FirstOrDefault(x => x.Player == player);
                    if (showdown == null || !showdown.Shows)
                    {
                        if (player.DealtCards == null)
                        {
                            // *Player LeitenantGlan mucks (does not show cards). Bets: 2. Collects: 4. Wins: 2.
                            builder.Append("mucks (does not show cards)");
                        }
                        else
                        {
                            // Player JustFoldBaby mucks (does not show: [6c 4c]). Bets: 46694. Collects: 0. Loses: 46694.
                            builder.Append("mucks (does not show: [");
                            builder.Append(player.DealtCards[0]);
                            builder.Append(" ");
                            builder.Append(player.DealtCards[1]);

                            if (hand.isOmaha)
                            {
                                builder.Append(" ");
                                builder.Append(player.DealtCards[2]);
                                builder.Append(" ");
                                builder.Append(player.DealtCards[3]);
                            }

                            builder.Append("])");
                        }
                    }
                    else
                    {
                        // *Player AlexPoc shows: Two pairs. 10s and 6s [10h Qh]. Bets: 9. Collects: 19. Wins: 10.
                        var combination = GetCombination(hand, showdown.Cards);

                        if (allCombinations.Count(x => x == combination) > 1 && player.EndStack > player.InitialStack)
                        {
                            combination = GetCombination(hand, showdown.Cards, true);
                        }

                        builder.Append("shows: ");
                        builder.Append(combination);
                        builder.Append(" [");
                        builder.Append(showdown.Cards[0]);
                        builder.Append(" ");
                        builder.Append(showdown.Cards[1]);

                        if (hand.isOmaha)
                        {
                            builder.Append(" ");
                            builder.Append(showdown.Cards[2]);
                            builder.Append(" ");
                            builder.Append(showdown.Cards[3]);
                        }
                        builder.Append("]");
                    }
                }
                else
                {
                    // Player AlterMann does not show cards.Bets: 2. Collects: 0. Loses: 2.
                    builder.Append("does not show cards");
                }

                long bet = player.TotalBet;
                if (player == uncalledBetPlayer)
                    bet -= uncalledBet;

                builder.Append(".Bets: ");
                builder.Append(bet);
                builder.Append(". Collects: ");
                builder.Append(player.EndStack - (player.InitialStack - bet));
                builder.Append(". ");

                var diff = player.EndStack - player.InitialStack;
                if (diff >= 0)
                {
                    // *Player AlexPoc shows: Two pairs. 10s and 6s [10h Qh]. Bets: 9. Collects: 19. Wins: 10.
                    builder.Append("Wins: ");
                    builder.Append(diff);
                }
                else
                {
                    // Player AlterMann does not show cards.Bets: 2. Collects: 0. Loses: 2.
                    builder.Append("Loses: ");
                    builder.Append(-diff);
                }

                builder.AppendLine(".");
            }

            builder.Append("Game ended at: ");
            builder.AppendLine(hand.EndedAt.ToString(DateTimeFormat, CultureInfo.InvariantCulture));
            builder.AppendLine();
        }

        public void Write(HandInfo hand, StringBuilder builder, bool clearHero_ = false)
        {
            clearHero = clearHero_;

            WriteHeader(hand, builder);

            WritePlayers(hand, builder);

            WriteBlinds(hand, builder);

            WriteCardsDealt(hand, builder);

            WriteActions(hand, builder);

            WriteSummary(hand, builder);
        }
    }
}
