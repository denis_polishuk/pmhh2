﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PokerMaster.HandHistory
{
    public static class HHSender
    {

        private static string HUBuffer;
        private static int HUBufferCounter = 0;
        private static string sixMaxBuffer1020_50100;
        private static int sixMaxBuffer1020_50100Counter = 0;
        private static string sixMaxBuffer510_lower;
        private static int sixMaxBuffer510_lowerCounter = 0;
        private static string sixMaxBuffer100200_higher;
        private static int sixMaxBuffer100200_higherCounter = 0;
        private static string omahaBuffer;
        private static int omahaBufferCounter = 0;
        private static string ftpAddress = "ftp://ftp.pokermasterhandhistory.com/";
        private static string dlFtpPath = "ftp://ftp.pokermasterhandhistory.com/download";
        private static string dlFtpPathStraddle = "ftp://ftp.pokermasterhandhistory.com/download_straddle";
        public static string ftpUser;// = "ChaosGen@pokermasterhandhistory.com";
        public static string ftpPass;// = ")ZB;Oogjr&tp";
                                     //private static string ftpPath = "/home/pokermasterhandhistory.com/ChaosGen";
#if DEBUG
        private static int sendThreshold = 0;
#else
        private static int sendThreshold = 15;
#endif
        private static string hhReceiveDirectoryPath = Environment.CurrentDirectory + "//hh/received";
        private static string hhReceiveDirectoryPathStraddle = Environment.CurrentDirectory + "//hh_straddle/received";
        private static int ftpTimeout = 7000;
        public static Nullable<bool> testConnectionResult = null;
        public static Nullable<bool> dlFromFTPError = null;
        private static string ftpData = "";
        private static string ftpFileName = "";
        private static object locker = new object();



        public static void testConnection()
        {

            FtpWebRequest requestDir = (FtpWebRequest)FtpWebRequest.Create(ftpAddress);
            requestDir.Credentials = new NetworkCredential(ftpUser, ftpPass);
            requestDir.Method = WebRequestMethods.Ftp.ListDirectoryDetails;
            requestDir.Timeout = ftpTimeout;
            try
            {
                WebResponse response = requestDir.GetResponse();
                //return true;
                testConnectionResult = true;
            }
            catch(Exception e)
            {
                testConnectionResult = false;
            }

            File.AppendAllText("log2.txt", testConnectionResult.ToString() + Environment.NewLine);


        }

        static HHSender()
        {
            System.IO.Directory.CreateDirectory(Environment.CurrentDirectory+"//hh/to_send");
            System.IO.Directory.CreateDirectory(hhReceiveDirectoryPath);
            System.IO.Directory.CreateDirectory(hhReceiveDirectoryPathStraddle);
        }

        public static void prepareHandForSending(string handString, HandInfo handInfo)
        {
            if (handInfo.isOmaha)
            {
                omahaBuffer += handString;
                omahaBufferCounter++;
                if(omahaBufferCounter> sendThreshold)
                {
                    sendFilesToFTP();
                }
            }
            else
            {
                if (handInfo.Players.Length == 2)
                {
                    HUBuffer += handString;
                    HUBufferCounter++;
                    if (HUBufferCounter > sendThreshold) {
                        sendFilesToFTP();
                    }
                }
                else if ((handInfo.Players.Length > 2) &&(handInfo.BigBlind<11))
                {
                    sixMaxBuffer510_lower += handString;
                    sixMaxBuffer510_lowerCounter++;
                    if (sixMaxBuffer510_lowerCounter > sendThreshold)
                    {
                        sendFilesToFTP();
                    }
                }
                else if ((handInfo.Players.Length > 2) && (handInfo.BigBlind > 199))
                {
                    sixMaxBuffer100200_higher += handString;
                    sixMaxBuffer100200_higherCounter++;
                    if (sixMaxBuffer100200_higherCounter > sendThreshold)
                    {
                        sendFilesToFTP();
                    }
                }
                else if (handInfo.Players.Length > 2)
                {
                    sixMaxBuffer1020_50100 += handString;
                    sixMaxBuffer1020_50100Counter++;
                    if (sixMaxBuffer1020_50100Counter > sendThreshold) {
                        sendFilesToFTP();
                    }
                }
            }
        }



        public static void sendFilesToFTP(bool forceSend = false)
        {
            string timeStamp = DateTime.Now.Year.ToString() + "_" +
                                        DateTime.Now.Month.ToString() + "_" +
                                        DateTime.Now.Day.ToString() + "__" +
                                        DateTime.Now.Hour.ToString() + "_" +
                                        DateTime.Now.Minute.ToString() + "_" +
                                        DateTime.Now.Second.ToString() + "_" +
                                        DateTime.Now.Millisecond.ToString();

            if ((HUBufferCounter > sendThreshold)|| 
                (forceSend &&(HUBufferCounter>0)))
            {
                string tempFileName = "HU_"+ timeStamp +".txt";
                string tempFilePath = Environment.CurrentDirectory + "/hh/to_send/" + tempFileName;


                ftpData = HUBuffer;
                ftpFileName = tempFileName;
                Thread ftp = new Thread(new ThreadStart(uploadFileToFTP));
                ftp.Start();
                File.AppendAllText(tempFilePath, HUBuffer);

                HUBufferCounter = 0;
                HUBuffer = "";
            }

            if((sixMaxBuffer1020_50100Counter  > sendThreshold)||
                (forceSend && (sixMaxBuffer1020_50100Counter > 0)))
            {
                string tempFileName = "sixMax102050100_" + timeStamp + ".txt";
                string tempFilePath = Environment.CurrentDirectory + "/hh/to_send/" + tempFileName;


                ftpData = sixMaxBuffer1020_50100;
                ftpFileName = tempFileName;
                Thread ftp = new Thread(new ThreadStart(uploadFileToFTP));
                ftp.Start();
                
                File.AppendAllText(tempFilePath, sixMaxBuffer1020_50100);

                sixMaxBuffer1020_50100Counter = 0;
                sixMaxBuffer1020_50100 = "";
            }


            if ((sixMaxBuffer100200_higherCounter > sendThreshold) ||
               (forceSend && (sixMaxBuffer100200_higherCounter > 0)))
            {
                string tempFileName = "sixMax100200higher_" + timeStamp + ".txt";
                string tempFilePath = Environment.CurrentDirectory + "/hh/to_send/" + tempFileName;


                ftpData = sixMaxBuffer100200_higher;
                ftpFileName = tempFileName;
                Thread ftp = new Thread(new ThreadStart(uploadFileToFTP));
                ftp.Start();

                File.AppendAllText(tempFilePath, sixMaxBuffer100200_higher);

                sixMaxBuffer100200_higherCounter = 0;
                sixMaxBuffer100200_higher = "";
            }


           if ((sixMaxBuffer510_lowerCounter > sendThreshold) ||
           (forceSend && (sixMaxBuffer510_lowerCounter > 0)))
            {
                string tempFileName = "sixMax510lower_" + timeStamp + ".txt";
                string tempFilePath = Environment.CurrentDirectory + "/hh/to_send/" + tempFileName;


                ftpData = sixMaxBuffer510_lower;
                ftpFileName = tempFileName;
                Thread ftp = new Thread(new ThreadStart(uploadFileToFTP));
                ftp.Start();

                File.AppendAllText(tempFilePath, sixMaxBuffer510_lower);

                sixMaxBuffer510_lowerCounter = 0;
                sixMaxBuffer510_lower = "";
            }



            if ((omahaBufferCounter > sendThreshold) ||
               (forceSend && (omahaBufferCounter > 0)))
            {
                string tempFileName = "omaha_" + timeStamp + ".txt";
                string tempFilePath = Environment.CurrentDirectory + "/hh/to_send/" + tempFileName;


                ftpData = omahaBuffer;
                ftpFileName = tempFileName;
                Thread ftp = new Thread(new ThreadStart(uploadFileToFTP));
                ftp.Start();

                
                File.AppendAllText(tempFilePath, omahaBuffer);

                omahaBufferCounter = 0;
                omahaBuffer = "";
            }

        }


        private static void uploadFileToFTP()//string data, string fileName)
        {
            lock (locker)
            {
                string data = ftpData;
                string fileName = ftpFileName;
                try
                {
                    FtpWebRequest ftpClient = (FtpWebRequest)FtpWebRequest.Create(ftpAddress + "upload/" + Path.GetFileName(fileName));
                    ftpClient.Credentials = new System.Net.NetworkCredential(ftpUser, ftpPass);
                    ftpClient.Method = System.Net.WebRequestMethods.Ftp.UploadFile;
                    ftpClient.UseBinary = false;
                    ftpClient.KeepAlive = true;
                    ftpClient.ContentLength = data.Length;


                    byte[] bytes = Encoding.UTF8.GetBytes(data);
                    Stream requestStream = ftpClient.GetRequestStream();
                    requestStream.Write(bytes, 0, bytes.Length);
                    requestStream.Close();

                    FtpWebResponse uploadResponse = (FtpWebResponse)ftpClient.GetResponse();
                    string value = uploadResponse.StatusDescription;
                    uploadResponse.Close();
                }
                catch (Exception e)
                {
                    File.AppendAllText("error.txt", "Upload to FTP failed"+Environment.NewLine);
                }
                
            }
        }


        public static void receiveFilesFromFTP() {


            System.Net.NetworkCredential credentials = new System.Net.NetworkCredential(ftpUser, ftpPass);

            FtpWebRequest listRequest = (FtpWebRequest)WebRequest.Create(dlFtpPath);
            listRequest.Method = WebRequestMethods.Ftp.ListDirectoryDetails;
            listRequest.Credentials = credentials ;

            try
            {
                List<string> lines = new List<string>();

                using (FtpWebResponse listResponse = (FtpWebResponse)listRequest.GetResponse())
                using (Stream listStream = listResponse.GetResponseStream())
                using (StreamReader listReader = new StreamReader(listStream))
                {
                    while (!listReader.EndOfStream)
                    {
                        lines.Add(listReader.ReadLine());
                    }
                }

                foreach (string line in lines)
                {
                    string[] tokens =
                        line.Split(new[] { ' ' }, 9, StringSplitOptions.RemoveEmptyEntries);
                    string name = tokens[8];
                    string permissions = tokens[0];

                    string localFilePath = Path.Combine(hhReceiveDirectoryPath, name);
                    string fileUrl = dlFtpPath + "/" + name;

                    if ((permissions[0] != 'd') && (Path.GetExtension(fileUrl) == ".txt"))
                    {
                        FtpWebRequest downloadRequest = (FtpWebRequest)WebRequest.Create(fileUrl);
                        downloadRequest.Method = WebRequestMethods.Ftp.DownloadFile;
                        downloadRequest.Credentials = credentials;

                        bool dlOk = true;

                        try
                        {
                            FtpWebResponse downloadResponse = (FtpWebResponse)downloadRequest.GetResponse();
                            using (Stream sourceStream = downloadResponse.GetResponseStream())
                            using (Stream targetStream = File.Create(localFilePath))
                            {
                                byte[] buffer = new byte[10240];
                                int read;
                                while ((read = sourceStream.Read(buffer, 0, buffer.Length)) > 0)
                                {
                                    targetStream.Write(buffer, 0, read);
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            dlFromFTPError = true;
                            dlOk = false;
                        }


                        if (dlOk)
                        {
                            FtpWebRequest deleteRequest = (FtpWebRequest)WebRequest.Create(fileUrl);
                            deleteRequest.Method = WebRequestMethods.Ftp.DeleteFile;
                            deleteRequest.Credentials = credentials;
                            deleteRequest.GetResponse();
                        }


                    }
                }
            }
            catch (Exception e){
                dlFromFTPError = true;
            }
            dlFromFTPError = false;
        }


        public static void receiveFilesFromFTPStraddle()
        {


            System.Net.NetworkCredential credentials = new System.Net.NetworkCredential(ftpUser, ftpPass);

            FtpWebRequest listRequest = (FtpWebRequest)WebRequest.Create(dlFtpPathStraddle);
            listRequest.Method = WebRequestMethods.Ftp.ListDirectoryDetails;
            listRequest.Credentials = credentials;

            try
            {
                List<string> lines = new List<string>();

                using (FtpWebResponse listResponse = (FtpWebResponse)listRequest.GetResponse())
                using (Stream listStream = listResponse.GetResponseStream())
                using (StreamReader listReader = new StreamReader(listStream))
                {
                    while (!listReader.EndOfStream)
                    {
                        lines.Add(listReader.ReadLine());
                    }
                }

                foreach (string line in lines)
                {
                    string[] tokens =
                        line.Split(new[] { ' ' }, 9, StringSplitOptions.RemoveEmptyEntries);
                    string name = tokens[8];
                    string permissions = tokens[0];

                    string localFilePath = Path.Combine(hhReceiveDirectoryPathStraddle, name);
                    string fileUrl = dlFtpPathStraddle + "/" + name;

                    if ((permissions[0] != 'd') && (Path.GetExtension(fileUrl) == ".txt"))
                    {
                        FtpWebRequest downloadRequest = (FtpWebRequest)WebRequest.Create(fileUrl);
                        downloadRequest.Method = WebRequestMethods.Ftp.DownloadFile;
                        downloadRequest.Credentials = credentials;

                        bool dlOk = true;

                        try
                        {
                            FtpWebResponse downloadResponse = (FtpWebResponse)downloadRequest.GetResponse();
                            using (Stream sourceStream = downloadResponse.GetResponseStream())
                            using (Stream targetStream = File.Create(localFilePath))
                            {
                                byte[] buffer = new byte[10240];
                                int read;
                                while ((read = sourceStream.Read(buffer, 0, buffer.Length)) > 0)
                                {
                                    targetStream.Write(buffer, 0, read);
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            dlFromFTPError = true;
                            dlOk = false;
                        }


                        if (dlOk)
                        {
                            FtpWebRequest deleteRequest = (FtpWebRequest)WebRequest.Create(fileUrl);
                            deleteRequest.Method = WebRequestMethods.Ftp.DeleteFile;
                            deleteRequest.Credentials = credentials;
                            deleteRequest.GetResponse();
                        }


                    }
                }
            }
            catch (Exception e)
            {
                dlFromFTPError = true;
            }
            dlFromFTPError = false;
        }



    }
}
