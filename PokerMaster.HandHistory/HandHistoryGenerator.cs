﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Schema;
using PokerMaster.Domain.Model;

namespace PokerMaster.HandHistory
{
    public class HandHistoryGenerator
    {
        private static readonly Dictionary<USER_GAME_STATE, ActionType> PlayerActionsMap = new Dictionary<USER_GAME_STATE, ActionType>
        {
            {USER_GAME_STATE.USER_GAME_STATE_CALL, ActionType.Call},
            {USER_GAME_STATE.USER_GAME_STATE_CHECK, ActionType.Check},
            {USER_GAME_STATE.USER_GAME_STATE_FOLD, ActionType.Fold},
            {USER_GAME_STATE.USER_GAME_STATE_RAISE, ActionType.Raise}
        };

        private static readonly Dictionary<GAME_ROOM_GAME_STATE, GameStage> GameStageMap = new Dictionary
            <GAME_ROOM_GAME_STATE, GameStage>
        {
            {GAME_ROOM_GAME_STATE.ROOM_GAME_STATE_PreFlop, GameStage.Preflop},
            {GAME_ROOM_GAME_STATE.ROOM_GAME_STATE_Flop, GameStage.Flop},
            {GAME_ROOM_GAME_STATE.ROOM_GAME_STATE_Turn, GameStage.Turn},
            {GAME_ROOM_GAME_STATE.ROOM_GAME_STATE_River, GameStage.River}
        };

        private static readonly USER_GAME_STATE[] IngameStates =
            {
                USER_GAME_STATE.USER_GAME_STATE_ALLIN, USER_GAME_STATE.USER_GAME_STATE_BETTING,
                USER_GAME_STATE.USER_GAME_STATE_BLIND, USER_GAME_STATE.USER_GAME_STATE_CALL,
                USER_GAME_STATE.USER_GAME_STATE_CHECK, USER_GAME_STATE.USER_GAME_STATE_RAISE
            };

        private static readonly TimeZoneInfo DefaultTimeZone = TimeZoneInfo.FindSystemTimeZoneById("UTC");

        private void InputCleanup(List<GameRoomInfo> states)
        {
            // костыль под десятый апдейт ПМ
            if(states.Last().EGameState == GAME_ROOM_GAME_STATE.ROOM_GAME_STATE_GameStart)
            {
                states.RemoveAt(states.Count - 1);
            }
            
        }

        private void ValidateInput(List<GameRoomInfo> states)
        {
            if (states == null || !states.Any())
                throw new Exception("Empty state set: nothing to parse");

            var gameState = states[0].EGameState;

            if (gameState != GAME_ROOM_GAME_STATE.ROOM_GAME_STATE_GameStart && gameState != GAME_ROOM_GAME_STATE.ROOM_GAME_STATE_GameWait)
                throw new Exception($"First state is supposed to be GameStart or GameWait, got {gameState} instead");

            // TODO validation
        }

        private void SetButton(HandInfo info, List<GameRoomInfo> states)
        {
            var state =
                states.FirstOrDefault(
                    x =>
                        x.EGameState == GAME_ROOM_GAME_STATE.ROOM_GAME_STATE_Ante ||
                        x.EGameState == GAME_ROOM_GAME_STATE.ROOM_GAME_STATE_PreFlop);

            if (state == null) 
                throw new Exception("Failed to find Ante or PreFlop stage to set button");

            foreach (var userInfo in state.VUserGameInfos)
            {
                if (userInfo.BGameDealer == 1)
                {
                    var player = FindPlayer(info, userInfo);
                    player.IsButton = true;
                }
            }
        }

        private Card ParseCard(int id)
        {
            if (id < 0 || id >= 52) throw new ArgumentException($"Invalid card id {id}");

            return new Card
            {
                Rank = id%13,
                Suit = id/13
            };
        }

        private void UpdateBoard(HandInfo info, GameRoomInfo state)
        {
            var cards = state.VCurrentCards;

            if (cards != null && cards.Any())
            {
                if (cards.Length < 3 || cards.Length > 5) throw new Exception($"Invalid board cards count: {cards.Length}. Possibly not a Hold'em game");

                info.Flop = new[]
                {
                    ParseCard(cards[0]),
                    ParseCard(cards[1]),
                    ParseCard(cards[2])
                };

                if (cards.Length > 3)
                    info.Turn = ParseCard(cards[3]);

                if (cards.Length > 4)
                    info.River = ParseCard(cards[4]);
            }
            else 
            {
                info.Flop = null;
                info.Turn = null;
                info.River = null;
            }
        }

        private PlayerInfo[] InitializePlayers(GameRoomInfo firstState)
        {
            var allPlayers = firstState.VUserGameInfos.ToList();

            var activePlayers = allPlayers
                .Where(x => x.StUserInfo.Uuid != 0)
                .ToList();

            var result = new List<PlayerInfo>();

            foreach (var ap in activePlayers)
            {
                var player = new PlayerInfo
                {
                    InitialStack = ap.LRemainStacks,
                    Name = ap.StUserInfo.SShowID,
                    Seat = allPlayers.IndexOf(ap) + 1,
                };

                result.Add(player);
            }

            return result.ToArray();
        }

        private DateTime GetDateTimeFromMs(long ms)
        {
            var dateTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Unspecified).AddMilliseconds(ms);

            return TimeZoneInfo.ConvertTime(dateTime, DefaultTimeZone, TimeZoneInfo.Local);
        }

        private PlayerInfo FindPlayer(HandInfo hand, UserBaseInfoNet userInfo, bool safe = true)
        {
            var result = hand.Players.SingleOrDefault(x => x.Name == userInfo.SShowID);

            if (safe && result == null)
                throw new Exception($"Failed to find player by SShowID: {userInfo.SShowID}");

            return result;
        }

        private PlayerInfo FindPlayer(HandInfo hand, UserGameInfoNet userInfo, bool safe = true)
        {
            return FindPlayer(hand, userInfo.StUserInfo, safe);
        }

        private HandInfo InitializeHandInfo(GameRoomInfo firstState)
        {
            var hand = new HandInfo();

            var baseInfo = firstState.StGameRoomBaseInfo;

            var maxActTime = firstState.VUserGameInfos.Max(x => x.LActTime);

            hand.SmallBlind = baseInfo.LSmallBlinds;
            hand.BigBlind = baseInfo.LBigBlinds;
            hand.Ante = baseInfo.IAnte;
            hand.Straddle = baseInfo.BStraddle * baseInfo.LBigBlinds * 2;
            hand.StartedAt = GetDateTimeFromMs(maxActTime);
            hand.Id = baseInfo.LGameRoomId*10000 + firstState.LGameHandID;
            if((firstState.StGameRoomBaseInfo.EGameRoomType==GAME_ROOM_TYPE.GAME_ROOM_OMAHA)||(firstState.StGameRoomBaseInfo.EGameRoomType == GAME_ROOM_TYPE.GAME_ROOM_OMAHA_INSURANCE))
            {
                hand.isOmaha = true;
            }
            


            return hand;
        }

        private void SummarizeHandInfo(HandInfo hand, GameRoomInfo lastState)
        {
            var maxActTime = lastState.VUserGameInfos.Max(x => x.LActTime);

            hand.EndedAt = GetDateTimeFromMs(maxActTime);

            hand.Pot = hand.Actions.Sum(x => x.Amount);
            foreach (var p in hand.Players)
            {
                p.TotalBet = hand.Actions.Where(x => x.Player == p).Sum(x => x.Amount);
                p.EndStack = p.InitialStack - p.TotalBet;
            }

            foreach (var result in lastState.StGameResultInfo.StGamePotResultInfo)
            {
                var pot = result.LUserPots.Sum();
                foreach (var u in result.StUserWiner)
                {
                    var player = FindPlayer(hand, u);

                    player.EndStack += pot/result.StUserWiner.Length;
                }
            }
        }

        private void GeneratePostAnteActions(HandInfo info, GameRoomInfo state, int anteSize = -1)
        {
            var dealer = info.Players.SingleOrDefault(x => x.IsButton);
            if (dealer == null)
                throw new Exception("Button not set, cannot proceed");

            var dealerIndex = info.Players.ToList().IndexOf(dealer);
            int playersCount = info.Players.Length;

            for (int i = 0; i < playersCount; i++)
            {
                var index = (dealerIndex + i + 1)%playersCount;
                var player = info.Players[index];

                var userInfo = state.VUserGameInfos[player.Seat - 1];

                var bet = anteSize == -1 ? userInfo.LBetStacks : anteSize;

                if (bet > 0)
                {
                    if (info.Ante < bet) throw new Exception($"PostAnte ({bet}) cannot be bigger than Ante value ({info.Ante})");

                    var action = new PlayerAction
                    {
                        Action = ActionType.PostAnte,
                        Amount = bet,
                        Player = player,
                        Stage = GameStage.Ante
                    };

                    info.Actions.Add(action);
                }
            }
        }

        private void GenerateBlinds(HandInfo hand, GameRoomInfo state, long[] prevBettingIds, long[] prevBets, USER_GAME_STATE[] prevUserStates)
        {
            var postedBlinds = state.VUserGameInfos.Where(x => 
                x.EGameRole == USER_GAME_ROLE.USER_GAME_ROLE_SMALL_BLIND || 
                x.EGameRole == USER_GAME_ROLE.USER_GAME_ROLE_BIG_BLIND || 
                x.EGameRole == USER_GAME_ROLE.USER_GAME_ROLE_STRADDLE)
                .OrderBy(x => x.EGameRole);

            foreach (var userInfo in postedBlinds)
            {
                var action = new PlayerAction
                {
                    Player = FindPlayer(hand, userInfo),
                    Stage = GameStage.Preflop
                };

                var bet = userInfo.LBetStacks;

                var playerIndex = action.Player.Seat - 1;

                switch (userInfo.EGameRole)
                {
                    case USER_GAME_ROLE.USER_GAME_ROLE_SMALL_BLIND:
                        if (bet > hand.SmallBlind)
                            bet = hand.SmallBlind;
                        if (bet < 0)
                            throw new Exception($"Invalid PostSB value ({bet}). SmallBlind value is {hand.SmallBlind}");

                        action.Action = ActionType.PostSB;
                        break;
                    case USER_GAME_ROLE.USER_GAME_ROLE_BIG_BLIND:
                        if (bet > hand.BigBlind)
                            bet = hand.BigBlind;
                        if (bet < 0)
                            throw new Exception($"Invalid PostBB value ({bet}). BigBlind value is {hand.BigBlind}");

                        action.Action = ActionType.PostBB;
                        break;
                    case USER_GAME_ROLE.USER_GAME_ROLE_STRADDLE:
                        if (bet > hand.Straddle)
                            bet = hand.Straddle;
                        if (bet < 0)
                            throw new Exception($"Invalid PostStraddle value ({bet}). Straddle value is {hand.Straddle}");

                        action.Action = ActionType.PostStraddle;
                        break;
                }

                prevBettingIds[playerIndex] = userInfo.EGameState == USER_GAME_STATE.USER_GAME_STATE_BLIND
                    ? userInfo.LBettingID
                    : -1;
                prevBets[playerIndex] = bet;
                prevUserStates[playerIndex] = USER_GAME_STATE.USER_GAME_STATE_BLIND;

                action.Amount = bet;
                action.Player.Acted = true;
                hand.Actions.Add(action);
            }
        }

        private long[] GetBettingIds(GameRoomInfo state)
        {
            return state.VUserGameInfos.Select(x => x.LBettingID).ToArray();
        }

        private long[] GetBets(GameRoomInfo state)
        {
            return state.VUserGameInfos.Select(x => x.LBetStacks).ToArray();
        }

        private USER_GAME_STATE[] GetUserStates(GameRoomInfo state)
        {
            return state.VUserGameInfos.Select(x => x.EGameState).ToArray();
        }

        private int GetBettingPlayerIndex(HandInfo hand, GameRoomInfo state)
        {
            var players = state.VUserGameInfos;

            var playerToBet =
                players.SingleOrDefault(
                    x => x.EGameState == USER_GAME_STATE.USER_GAME_STATE_BETTING);

            if (playerToBet != null)
                return players.ToList().IndexOf(playerToBet);

            var dealer = hand.Players.SingleOrDefault(x => x.IsButton);

            if (dealer == null) throw new Exception("No dealer");

            if (hand.Players.Length == 2)
            {
                for (int i = 0; i < players.Length; i++)
                {
                    if (dealer.Name != players[i].StUserInfo.SShowID && players[i].StUserInfo.Uuid != 0)
                        return i;
                }
            }
            else
            {
                int button = dealer.Seat - 1;

                for (int i = 0; i < players.Length; i++)
                {
                    var index = (button + i + 1)%players.Length;
                    var role = players[index].EGameRole;

                    if (role == USER_GAME_ROLE.USER_GAME_ROLE_NORMAL || role == USER_GAME_ROLE.USER_GAME_ROLE_DEAL)
                        return index;
                }
            }

            return -1;
        }

        private void GenerateDealtCards(HandInfo hand, GameRoomInfo state)
        {
            foreach (var u in state.VUserGameInfos)
            {
                if (u.VCurrentHands != null)
                {
                    var player = FindPlayer(hand, u);
                    if (hand.isOmaha) {
                        player.DealtCards = new[] { ParseCard(u.VCurrentHands[0]), ParseCard(u.VCurrentHands[1]), ParseCard(u.VCurrentHands[2]), ParseCard(u.VCurrentHands[3]) };
                    }
                    else
                    {
                        player.DealtCards = new[] { ParseCard(u.VCurrentHands[0]), ParseCard(u.VCurrentHands[1]) };
                    }
                    
                }
            }
        }

        private bool IsInGame(UserGameInfoNet user)
        {
            return IngameStates.Contains(user.EGameState);
        }

        private void GenerateBettingActions(HandInfo hand, GameRoomInfo state, long[] prevBettingIds, long[] prevBets, USER_GAME_STATE[] prevUserStates, int prevBettingPlayerIndex)
        {
            if (prevBettingPlayerIndex < 0 || prevBettingPlayerIndex >= state.VUserGameInfos.Length)
                throw new ArgumentException($"Invalid value for {nameof(prevBettingPlayerIndex)}: {prevBettingPlayerIndex}");
            if (prevBettingIds == null || prevBettingIds.Length != state.VUserGameInfos.Length)
                throw new ArgumentException($"Invalid value for {nameof(prevBettingIds)}: length {prevBettingIds?.Length}");
            if (prevBets == null || prevBets.Length != state.VUserGameInfos.Length)
                throw new ArgumentException($"Invalid value for {prevBets}: length {prevBets?.Length}");

            GameStage gameStage;
            if (!GameStageMap.TryGetValue(state.EGameState, out gameStage))
                throw new Exception($"Invalid game state: {state.EGameState}. Failed to map it to game stage");

            var userInfos = state.VUserGameInfos;

            bool firstBet = prevBets.Sum() == 0;

            for (int i = 0; i < userInfos.Length; i++)
            {
                int index = (i + prevBettingPlayerIndex)%userInfos.Length;

                var userInfo = userInfos[index];

                // Player not active
                if (userInfo.StUserInfo.Uuid == 0) continue;

                if (prevBettingIds[index] != userInfo.LBettingID || 
                    prevBets[index] != userInfo.LBetStacks || 
                    prevUserStates[index] != userInfo.EGameState)
                {
                    var betAmount = userInfo.LBetStacks - prevBets[index];
                    if (betAmount < 0) betAmount = 0;

                    ActionType actionType;
                    if (!PlayerActionsMap.TryGetValue(userInfo.EGameState, out actionType))
                    {
                        // TODO Log error?
                        continue;
                    }

                    if (actionType == ActionType.Raise)
                    {
                        if (userInfo.LRemainStacks == 0) actionType = ActionType.AllIn;
                        else if (firstBet)
                        {
                            actionType = ActionType.Bet;
                        }

                        firstBet = false;
                    }

                    var player = FindPlayer(hand, userInfo, false);

                    if (player == null) continue;

                    player.Acted = true;

                    var action = new PlayerAction
                    {
                        Action = actionType,
                        Amount = betAmount,
                        Player = player,
                        Stage = gameStage
                    };

                    hand.Actions.Add(action);
                }
            }
        }

        private void GenerateShowdowns(HandInfo hand, GameRoomInfo state, int prevBettingPlayerIndex)
        {
            var gameState = state.EGameState;
            if (gameState != GAME_ROOM_GAME_STATE.ROOM_GAME_STATE_SHOWCARD)
                throw new Exception($"Invalid game state: {gameState}, expected: SHOWCARD");

            var userInfos = state.VUserGameInfos;

            for (int i = 0; i < userInfos.Length; i++)
            {
                int index = (i + prevBettingPlayerIndex)%userInfos.Length;

                var user = userInfos[index];

                if (IsInGame(user))
                {
                    var player = FindPlayer(hand, user);
                    player.SurvivedTillShowdown = true;

                    var showdown = new ShowDown
                    {
                        Player = player
                    };

                    if (user.VCurrentHands != null && ((user.VCurrentHands.Length == 2) || (user.VCurrentHands.Length == 4)) )
                    {
                        showdown.Cards = user.VCurrentHands.Select(ParseCard).ToArray();
                        showdown.Shows = true;
                    }

                    hand.ShowDowns.Add(showdown);
                }
            }
        }

        //private bool IsHeadsUpSpecialCase(List<GameRoomInfo> states)
        //{
        //    return states.Count == 3 && states[1].EGameState == GAME_ROOM_GAME_STATE.ROOM_GAME_STATE_Result;
        //}

        //private HandInfo HeadsUpSpecialCase(List<GameRoomInfo> states)
        //{
        //    var hand = InitializeHandInfo(states[0]);

        //    hand.Players = InitializePlayers(states[0]);

        //    var sb =
        //        states[1].VUserGameInfos.Single(x => x.EGameRole == USER_GAME_ROLE.USER_GAME_ROLE_SMALL_BLIND);

        //    if (hand.Ante > 0)
        //        GeneratePostAnteActions(hand, states[0], hand.Ante);

            
        //    var bb =
        //        states[0].VUserGameInfos.Single(x => x.EGameRole == USER_GAME_ROLE.USER_GAME_ROLE_BIG_BLIND);


        //    SummarizeHandInfo(hand, states.Last());



        //    return hand;
        //}

        public HandInfo GenerateHand(List<GameRoomInfo> states)
        {
            InputCleanup(states);
            ValidateInput(states);

            //if (IsHeadsUpSpecialCase(states))
            //{
            //    return HeadsUpSpecialCase(states);
            //}

            var hand = InitializeHandInfo(states[0]);

            hand.Players = InitializePlayers(states[0]);

            SetButton(hand, states);

            var bettingIds = GetBettingIds(states[0]);
            var bets = GetBets(states[0]);
            var userStates = GetUserStates(states[0]);
            int bettingPlayerIndex = GetBettingPlayerIndex(hand, states[0]);
            GameStage prevStage = 0;

            bool blindsSet = false;
            bool anteSet = false;

            foreach (var curState in states.Skip(1))
            {
                GameStage gameStage;
                if (GameStageMap.TryGetValue(curState.EGameState, out gameStage))
                {
                    if (prevStage != gameStage)
                        bets = new long[bets.Length];
                }

                UpdateBoard(hand, curState);

                switch (curState.EGameState)
                {
                    case GAME_ROOM_GAME_STATE.ROOM_GAME_STATE_Ante:
                        GeneratePostAnteActions(hand, curState);
                        anteSet = true;
                        break;
                    case GAME_ROOM_GAME_STATE.ROOM_GAME_STATE_PreFlop:

                        if (!anteSet && hand.Ante > 0)
                        {
                            // If we missed the ante update, fake it
                            GeneratePostAnteActions(hand, curState, hand.Ante);
                            anteSet = true;
                        }

                        if (blindsSet)
                        {
                            GenerateDealtCards(hand, curState);
                        }
                        else
                        {
                            GenerateBlinds(hand, curState, bettingIds, bets, userStates);
                            bettingPlayerIndex = GetBettingPlayerIndex(hand, curState);
                            blindsSet = true;
                        }

                        GenerateBettingActions(hand, curState, bettingIds, bets, userStates, bettingPlayerIndex);
                        break;
                    case GAME_ROOM_GAME_STATE.ROOM_GAME_STATE_Flop:
                    case GAME_ROOM_GAME_STATE.ROOM_GAME_STATE_Turn:
                    case GAME_ROOM_GAME_STATE.ROOM_GAME_STATE_River:
                        GenerateBettingActions(hand, curState, bettingIds, bets, userStates, bettingPlayerIndex);
                        break;
                    case GAME_ROOM_GAME_STATE.ROOM_GAME_STATE_SHOWCARD:
                        GenerateShowdowns(hand, curState, bettingPlayerIndex);
                        break;
                }

                bettingIds = GetBettingIds(curState);
                bets = GetBets(curState);

                userStates = GetUserStates(curState);
                prevStage = gameStage;

                if (curState.EGameState != GAME_ROOM_GAME_STATE.ROOM_GAME_STATE_Result)
                    bettingPlayerIndex = GetBettingPlayerIndex(hand, curState);
            }

            hand.Players = hand.Players.Where(x => x.Acted).ToArray();
            hand.Actions = hand.Actions.Where(x => x.Player.Acted).ToList();

            SummarizeHandInfo(hand, states.Last());

            return hand;
        }
    }
}
