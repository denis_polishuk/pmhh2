﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokerMaster.HandHistory
{
    public class HandInfo
    {
        public long Id { get; set; }

        public DateTime StartedAt { get; set; }
        public DateTime EndedAt { get; set; }

        public long SmallBlind { get; set; }
        public long BigBlind { get; set; }
        public int Ante { get; set; }
        public long Straddle { get; set; }

        public long Pot { get; set; }

        public Card[] Flop { get; set; }
        public Card Turn { get; set; }
        public Card River { get; set; }

        public string handHistory;


        public Card[] Board
        {
            get
            {
                if (Flop == null) return null;

                var board = Flop.ToList();

                if (Turn != null)
                    board.Add(Turn);

                if (River != null)
                    board.Add(River);

                return board.ToArray();
            }
        }

        public PlayerInfo[] Players { get; set; }
        public List<PlayerAction> Actions { get; set; } = new List<PlayerAction>();
        public List<ShowDown> ShowDowns { get; set; } = new List<ShowDown>();

        public bool isOmaha;



        public PlayerInfo getNextExpectedBettingPlayer() {


            return null;
        }

        public bool isHeroInHand()
        {
            PlayerInfo p = Players.FirstOrDefault(x => x.IsHero==true);
            if (p != null)
            {
                return true;
            }
            return false;
        }

        public static HandInfo createFromHH(string hh)
        {
            HandInfo res = new HandInfo();
            res.handHistory = hh;

            int idStart = hh.IndexOf("Game ID:") + 9;
            int idEnd = hh.IndexOf(" ", idStart + 10);

            long id;

            long.TryParse(hh.Substring(idStart, idEnd - idStart), out id);

            res.Id = id;



            return res;
        }

    }
}
