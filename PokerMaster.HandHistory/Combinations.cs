﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HoldemHand;

namespace PokerMaster.HandHistory
{
    public class Combinations
    {
        private static readonly int TOP_CARD_SHIFT = 16;
        private static readonly int SECOND_CARD_SHIFT = 12;
        private static readonly int THIRD_CARD_SHIFT = 8;
        private static readonly uint CARD_MASK = 0x0F;

        private static readonly string[] CardTable =
        {
            "2c", "3c", "4c", "5c", "6c", "7c", "8c", "9c", "Tc", "Jc", "Qc", "Kc", "Ac",
            "2d", "3d", "4d", "5d", "6d", "7d", "8d", "9d", "Td", "Jd", "Qd", "Kd", "Ad",
            "2h", "3h", "4h", "5h", "6h", "7h", "8h", "9h", "Th", "Jh", "Qh", "Kh", "Ah",
            "2s", "3s", "4s", "5s", "6s", "7s", "8s", "9s", "Ts", "Js", "Qs", "Ks", "As",
        };

        private static string GetRank(uint card)
        {
            return (CardTable[card][0] + "").Replace("T", "10");
        }

        private static string GetSuit(uint card)
        {
            return CardTable[card][1] + "";
        }

        private static string CardsToString(Card[] cards)
        {
            if (cards == null) return "";

            return string.Join(" ", cards.Select(x => x.ToString())).Replace("10", "T");
        }

        public static uint TopCard(uint hv)
        {
            return ((hv >> TOP_CARD_SHIFT) & CARD_MASK);
        }

        private static uint SecondCard(uint hv)
        {
            return (((hv) >> SECOND_CARD_SHIFT) & CARD_MASK);
        }

        private static uint ThirdCard(uint hv)
        {
            return (((hv) >> THIRD_CARD_SHIFT) & CARD_MASK);
        }


        public static string GetOmahaCombination(Card[] pocket, Card[] board, bool withKicker = false)
        {
            var boardStr = CardsToString(board);
            Hand maxHand = new Hand();
            int iMax=0, jMax=1;


            for(int i = 0; i < 3; i++)
            {
                for (int j = i+1; j < 4; j++)
                {


                    Card[] currCards = new Card[2];
                    currCards[0] = pocket[i];
                    currCards[1] = pocket[j];

                    var pocketStr = CardsToString(currCards);
                    var hand = new Hand(pocketStr, boardStr);

                    if (maxHand.ToString()=="")
                    {
                        maxHand = hand;
                    }
                    else
                    {
                        if (hand.HandTypeValue > maxHand.HandTypeValue)
                        {
                            maxHand = hand;
                            iMax = i;
                            jMax = j;
                        }
                    }

                }
            }


            Card[] currentCards = new Card[2];
            currentCards[0] = pocket[iMax];
            currentCards[1] = pocket[jMax];




            return GetCombination(currentCards, board, withKicker);
        }

        public static string GetCombination(Card[] pocket, Card[] board, bool withKicker = false)
        {
            var pocketStr = CardsToString(pocket);
            var boardStr = CardsToString(board);
            var hand = new Hand(pocketStr, boardStr);

            StringBuilder b = new StringBuilder();

            switch (hand.HandTypeValue)
            {
                case Hand.HandTypes.HighCard:
                    b.Append("High card ");
                    b.Append(GetRank(TopCard(hand.HandValue)));

                    if (withKicker)
                    {
                        b.Append(" (kicker ");
                        b.Append(GetRank(SecondCard(hand.HandValue)));
                        b.Append(")");
                    }
                   
                    return b.ToString();
                case Hand.HandTypes.Pair:
                    b.Append("One pair of ");
                    b.Append(GetRank(TopCard(hand.HandValue)));
                    b.Append("s");

                    if (withKicker)
                    {
                        b.Append(" (kicker ");
                        b.Append(GetRank(SecondCard(hand.HandValue)));
                        b.Append(")");
                    }

                    return b.ToString();
                case Hand.HandTypes.TwoPair:
                    b.Append("Two pairs. ");
                    b.Append(GetRank(TopCard(hand.HandValue)));
                    b.Append("s and ");
                    b.Append(GetRank(SecondCard(hand.HandValue)));
                    b.Append("s");

                    if (withKicker)
                    {
                        b.Append(" (kicker ");
                        b.Append(GetRank(ThirdCard(hand.HandValue)));
                        b.Append(")");
                    }

                    return b.ToString();
                case Hand.HandTypes.Trips:
                    b.Append("Three of Kind of ");
                    b.Append(GetRank(TopCard(hand.HandValue)));
                    b.Append("s");

                    if (withKicker)
                    {
                        b.Append(" (kicker ");
                        b.Append(GetRank(SecondCard(hand.HandValue)));
                        b.Append(")");
                    }

                    return b.ToString();
                case Hand.HandTypes.Straight:
                    b.Append("Straight to ");
                    b.Append(GetRank(TopCard(hand.HandValue)));

                    if (withKicker)
                    {
                        b.Append(" (kicker ");
                        b.Append(GetRank(SecondCard(hand.HandValue)));
                        b.Append(")");
                    }

                    return b.ToString();
                case Hand.HandTypes.Flush:
                    b.Append("Flush, ");
                    b.Append(GetRank(TopCard(hand.HandValue)));
                    b.Append(" high");

                    if (withKicker)
                    {
                        b.Append(" (kicker ");
                        b.Append(GetRank(SecondCard(hand.HandValue)));
                        b.Append(")");
                    }

                    return b.ToString();
                case Hand.HandTypes.FullHouse:
                    b.Append("Full House (");
                    b.Append(GetRank(TopCard(hand.HandValue)));
                    b.Append("/");
                    b.Append(GetRank(SecondCard(hand.HandValue)));
                    b.Append(")");

                    if (withKicker)
                    {
                        b.Append(" (kicker ");
                        b.Append(GetRank(ThirdCard(hand.HandValue)));
                        b.Append(")");
                    }

                    return b.ToString();
                case Hand.HandTypes.FourOfAKind:
                    b.Append("Four of kind of ");
                    b.Append(GetRank(TopCard(hand.HandValue)));
                    b.Append("s");

                    if (withKicker)
                    {
                        b.Append(" (kicker ");
                        b.Append(GetRank(SecondCard(hand.HandValue)));
                        b.Append(")");
                    }

                    return b.ToString();
                case Hand.HandTypes.StraightFlush:
                    b.Append("Straight Flush to ");
                    b.Append(GetRank(TopCard(hand.HandValue)));
                    b.Append(" ");
                    b.Append(GetSuit(TopCard(hand.HandValue)));

                    if (withKicker)
                    {
                        b.Append(" (kicker ");
                        b.Append(GetRank(SecondCard(hand.HandValue)));
                        b.Append(")");
                    }

                    return b.ToString();
            }

            throw new Exception($"Invalid combination: pocket {pocketStr}, board {boardStr}");
        }
    }
}
