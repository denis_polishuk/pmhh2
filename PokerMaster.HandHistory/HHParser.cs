﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokerMaster.HandHistory
{
    public static class HHParser
    {


        public static List<HandInfo> parseDir(string dirPath)
        {
            List<HandInfo> res = new List<HandInfo>();
            DirectoryInfo d = new DirectoryInfo(dirPath);
            FileInfo[] files = d.GetFiles("*.txt");

            foreach(FileInfo f in files)
            {
                List<HandInfo> temp = parseFile(dirPath + "/" + f.Name);
                res.AddRange(temp);
            }
            return res;

        }

        public static List<HandInfo> parseFile(string filePath)
        {
            List<HandInfo> res = new List<HandInfo>();

     
            string currentHand = "";
            foreach (string line in File.ReadLines(filePath, Encoding.UTF8))
            {
                if (line == "") { continue; }
                if (currentHand != "")
                {
                    currentHand += line + Environment.NewLine;
                    if (line.StartsWith("Game ended at:")) {
                        
                        res.Add(HandInfo.createFromHH(currentHand));
                        currentHand = "";
                    }
                }

                if (line.StartsWith("Game started at:"))
                {
                    currentHand += line + Environment.NewLine;
                }
            }

            return res;
        }

        public static HandInfo parseHand (string handString)
        {
            return null;
        }

    }
}
