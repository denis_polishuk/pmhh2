﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokerMaster.HandHistory
{
    public class ShowDown
    {
        public PlayerInfo Player { get; set; }
        public Card[] Cards { get; set; }
        public bool Shows { get; set; }
    }
}
