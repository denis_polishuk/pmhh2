﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokerMaster.HandHistory
{
    public enum GameStage
    {
        Ante,
        Preflop,
        Flop,
        Turn,
        River
    };

    public enum ActionType
    {
        PostSB,
        PostBB,
        PostAnte,
        PostStraddle,
        Fold,
        Check,
        Call,
        Bet, 
        Raise,
        AllIn
    }

    public class PlayerAction
    {
        public PlayerInfo Player { get; set; }
        public ActionType Action { get; set; }
        public long Amount { get; set; }
        public GameStage Stage { get; set; }
    }
}
