﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.Caching;
using System.Runtime.ExceptionServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace PokerMaster.Overlay
{
    public class ProcessTracker
    {
        private class ProcessInfo
        {
            public int HeadlessMemuProcessId { get; set; }
            public int EmulatorProcessId { get; set; }
            public IntPtr Window { get; set; }
        }

        private class TableInfo
        {
            public ProcessInfo Process { get; set; }
            public int Port { get; set; }
            public string TableName { get; set; }
        }

        private Dictionary<int, TableInfo> Tables { get; } = new Dictionary<int,TableInfo>();
        private Dictionary<int, ProcessInfo> Processes { get; } = new Dictionary<int,ProcessInfo>();
        private WindowManager WindowManager { get; }
        private MemoryCache ProcessCache { get; set; }
        private object CacheLock { get; } = new object();

        public ProcessTracker(WindowManager wm)
        {
            WindowManager = wm;
            ProcessCache = new MemoryCache("Processes");
        }


        
        public void ScanProcesses()
        {
            //уже зарегистрированные программой процессы
            var existing = Processes.Where(x => x.Value.EmulatorProcessId != 0 && x.Value.HeadlessMemuProcessId != 0);

            var headlessProcesses = Process.GetProcessesByName("MEmuHeadless")
                .Where(x => existing.All(y => y.Key != x.Id))
                .OrderBy(x => x.StartTime)
                .Concat(Process.GetProcessesByName("LdBoxHeadless")
                .Where(x => existing.All(y => y.Key != x.Id))
                .OrderBy(x => x.StartTime))
                .ToList();

            var memuProcesses = Process.GetProcessesByName("MEmu")
                .Where(x => existing.All(y => y.Value.EmulatorProcessId != x.Id))
                .OrderBy(x => x.StartTime)
                .Concat(Process.GetProcessesByName("dnplayer")
                .Where(x => existing.All(y => y.Value.EmulatorProcessId != x.Id))
                .OrderBy(x => x.StartTime))
                .ToList();

         

            if (headlessProcesses.Count != memuProcesses.Count)
                return;

            for (int i = 0; i < headlessProcesses.Count; i++)
            {
                var headlessId = headlessProcesses[i].Id;
                var memuId = memuProcesses[i].Id;

                var window = memuProcesses[i].MainWindowHandle;

                ProcessInfo info;
                if (!Processes.TryGetValue(headlessId, out info))
                {
                    info = new ProcessInfo();
                    Processes[headlessId] = info;
                }

                info.EmulatorProcessId = memuId;
                info.HeadlessMemuProcessId = headlessId;
                info.Window = window;
            }
        }

        private int? ProcessByPort(long userId, int port)
        {
            int? process;

            lock (CacheLock)
            {
                process = (int?) ProcessCache.Get(userId + "");
            }

            if ((process == null) || (process==0))
            {
                process = ProcessPorts.GetProcessByPort(port);
                if (process != null)
                {
                    lock (CacheLock)
                    {
                        ProcessCache.Add(userId + "", process, new CacheItemPolicy
                        {
                            AbsoluteExpiration = new DateTimeOffset(DateTime.UtcNow.AddMinutes(10))
                        });
                    }
                }
            }

            return process;
        }

        public void ClearCache()
        {
            lock (CacheLock)
            {
                Processes.Clear();
                ProcessCache?.Dispose();
                ProcessCache = new MemoryCache("Processes");
                
            }
        }

        public void PortUpdate(int port, long userId, string name, long sb, long bb)
        {
            var process = ProcessByPort(userId, port);

            if (process != null)
            {
                ProcessInfo info;
                if (!Processes.TryGetValue(process.Value, out info))
                {
                    info = new ProcessInfo {HeadlessMemuProcessId = process.Value};
                    Processes[process.Value] = info;
                }

                TableInfo table;

                if (!Tables.TryGetValue(port, out table))
                {
                    table = new TableInfo {Port = port};
                    Tables[port] = table;
                }

                if (table.Process != info)
                {
                    table.TableName = null;
                    table.Process = info;
                }

                if (!string.IsNullOrEmpty(name))
                {
                    string tableName = "(PRR) PokerMaster id"+name.ToString()+" - No Limit - "+sb.ToString()+" / "+bb.ToString()+" Hold'em Money in play: $100";
                    if (table.TableName != name)
                    {
                        table.TableName = name;

                        if (table.Process.Window != default(IntPtr))
                            WindowManager.UpdateText(table.Process.Window, new IntPtr(info.EmulatorProcessId), tableName);
                    }
                    else if(table.Process.Window != default(IntPtr))
                    {
                        WindowManager.checkWindowCreated(table.Process.Window, tableName);
                    }
                    
                }
            }
        }
    }
}
