﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Threading;
using WinAPI;

namespace PokerMaster.Overlay
{
    public class WindowManager : IDisposable
    {
        // "ATL:09B24C38", " (PRR) PokerMaster id24280030 - No Limit - $50 / $100 Hold'em Money in play: $0"

        private readonly Action<Action> _uiInvoke;

        private Dictionary<IntPtr, CustomWindow> Windows { get; } = new Dictionary<IntPtr, CustomWindow>();
        private string ClassName { get; }

        public bool clearCacheNeeded = false;
        public WindowManager(string windowClassName, Action<Action> uiInvoke)
        {
            _uiInvoke = uiInvoke;
            ClassName = windowClassName;
        }

        public void Maintenance()
        {
            var windows = Windows.ToList();

            foreach (var w in windows)
            {
                var win = w.Key;
                
                if (WindowAttrib.Exists(win))
                {
                    var rect = WindowAttrib.GetWindowRect(win);
                    w.Value.Move(rect);
                    //File.AppendAllText("log.txt", "123 " + w.Value._hwnd+Environment.NewLine);
                }
                else
                {
                    if (w.Value.isCreated)
                    {
                        w.Value.destroyWindow();
                        clearCacheNeeded = true;
                    }
                    //Windows.Remove(win);
                }
            }
        }

        public void UpdateText(IntPtr win, IntPtr process, string text)
        {
            _uiInvoke(() =>
            {
                CustomWindow cWin;
                
                if (!Windows.TryGetValue(win, out cWin))
                {
                    var rect = WindowAttrib.GetWindowRect(win);
                    //cWin = new CustomWindow(ClassName, text, win, process, rect);
                    cWin = new CustomWindow(ClassName, text, rect);
                    Windows[win] = cWin;
                }
                else if (!cWin.isCreated)
                {
                    var rect = WindowAttrib.GetWindowRect(win);

                    cWin.createWindow(rect);
                }

                cWin.SetText(text);
            });
        }

        public void checkWindowCreated(IntPtr win, string winName)
        {
            CustomWindow cWin;
            var rect = WindowAttrib.GetWindowRect(win);
            if (!Windows.TryGetValue(win, out cWin))
            {
                cWin = new CustomWindow(ClassName, winName, rect);
                Windows[win] = cWin;
            }
            else if(!cWin.isCreated)
            {
                
                cWin.createWindow(rect);
                Windows[win] = cWin;
            }
        }


        public void Dispose()
        {
            foreach (var w in Windows)
                w.Value.Dispose();
        }
    }
}
