﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Google.ProtocolBuffers;
using Google.ProtocolBuffers.Descriptors;
using Google.ProtocolBuffers.Serialization;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json.Linq;
using PokerMaster.Domain;
using PokerMaster.HandHistory;
using PokerMaster.Packets;
using PokerMaster.Domain.Model;

namespace PokerMaster.Tests
{
    [TestClass]
    public class HandHistoryTests
    {
        private List<SCGameRoomStateChange> ReadStates(string jsonPath)
        {
            var parser = new ProtoParser();
            var messageType = parser.Descriptor.MessageTypes.First(x => x.Name == nameof(SCGameRoomStateChange));

            var json = File.ReadAllText(jsonPath);
            var array = JArray.Parse(json);

            var states = new List<SCGameRoomStateChange>();

            foreach (var package in array)
            {
                var reader = JsonFormatReader.CreateInstance("[" + package["packet"]["data"] + "]");

                var dm = new List<DynamicMessage>();
                reader.ReadMessageArray(null, dm, DynamicMessage.GetDefaultInstance(messageType), ExtensionRegistry.Empty);

                var state = parser.Parse<SCGameRoomStateChange>(dm[0]);
                states.Add(state);
            }

            return states;
        } 

        public void Test(string jsonPath, string outputPath)
        {
            var states = ReadStates(jsonPath);
            var expected = File.ReadAllText(outputPath);

            var generator = new HandHistoryGenerator();
            var writer = new HandHistoryWriter();
            var hand = generator.GenerateHand(states.Select(x => x.StGameRoomInfo).ToList());

            var builder = new StringBuilder();
            writer.Write(hand, builder);

            var actual = builder.ToString();

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Test88()
        {
            Test("HHTests\\HH_24043827-88.json", "HHTests\\HH_24043827-88.txt");
        }

        [TestMethod]
        public void Test101()
        {
            Test("HHTests\\HH_24043827-101.json", "HHTests\\HH_24043827-101.txt");
        }
    }
}
