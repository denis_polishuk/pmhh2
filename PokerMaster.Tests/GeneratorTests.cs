﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Google.ProtocolBuffers;
using Google.ProtocolBuffers.Descriptors;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PokerMaster.Domain;
using PokerMaster.Packets;
using PokerMaster.Domain.Proto;
using ProtoToCsGenerator;

namespace PokerMaster.Tests
{
    [TestClass]
    public class GeneratorTests
    {
        [TestMethod]
        public void TestPackage()
        {
            Generator generator = new Generator();

            ProtoParser parser = new ProtoParser();
            var message = parser.Descriptor.FindTypeByName<MessageDescriptor>("Package");

            var res = generator.Generate(message);

            Assert.AreEqual("using Google.ProtocolBuffers;\r\n\r\nnamespace PokerMaster.Domain.Model\r\n{\r\n\tpublic class Package : DomainModel\r\n\t{\r\n\t\tpublic long Uuid { get; set; }\r\n\t\tpublic byte[] Head { get; set; }\r\n\t\tpublic long LMisSystemTime { get; set; }\r\n\t\tpublic byte[] Body { get; set; }\r\n\t\tpublic long LCurrentSystemTime { get; set; }\r\n\t\tpublic long ISeqNo { get; set; }\r\n\t\tpublic TEXAS_CMD ECmd { get; set; }\r\n\t\tpublic int CEncodeType { get; set; }\r\n\t\tpublic int Version { get; set; }\r\n\r\n\t\tpublic override void Parse(DynamicMessage message, ProtoParser parser)\r\n\t\t{\r\n\t\t\tUuid = parser.ParseField<long>(message, \"uuid\");\r\n\t\t\tHead = parser.ParseByteArray(message, \"head\");\r\n\t\t\tLMisSystemTime = parser.ParseField<long>(message, \"lMisSystemTime\");\r\n\t\t\tBody = parser.ParseByteArray(message, \"body\");\r\n\t\t\tLCurrentSystemTime = parser.ParseField<long>(message, \"lCurrentSystemTime\");\r\n\t\t\tISeqNo = parser.ParseField<long>(message, \"iSeqNo\");\r\n\t\t\tECmd = parser.ParseEnum<TEXAS_CMD>(message, \"eCmd\");\r\n\t\t\tCEncodeType = parser.ParseField<int>(message, \"cEncodeType\");\r\n\t\t\tVersion = parser.ParseField<int>(message, \"version\");\r\n\t\t}\r\n\t}\r\n}\r\n", res);
        }

        
        [TestMethod]
        public void TestGameRoomStateChange()
        {
            //LuaParser parser = new LuaParser();
            //parser.ParseAll(@"d:\temp\cocos\assets\src\proto");

            //ProtoBuilder builder = new ProtoBuilder();
            //var result = builder.Build();

            //generator.GenerateAll(result, "D:\\temp\\newclasses");

            Generator generator = new Generator();

            ProtoParser parser = new ProtoParser();
            var message = parser.Descriptor.FindTypeByName<MessageDescriptor>("SCGameRoomStateChange");

            var res = generator.Generate(message);

            Assert.AreEqual("using Google.ProtocolBuffers;\r\n\r\nnamespace PokerMaster.Domain.Model\r\n{\r\n\tpublic class SCGameRoomStateChange : DomainModel\r\n\t{\r\n\t\tpublic GameRoomInfo StGameRoomInfo { get; set; }\r\n\t\tpublic int[] VGivenCards { get; set; }\r\n\t\tpublic int[] VGivenHands { get; set; }\r\n\t\tpublic long Uuid { get; set; }\r\n\r\n\t\tpublic override void Parse(DynamicMessage message, ProtoParser parser)\r\n\t\t{\r\n\t\t\tStGameRoomInfo = parser.ParseMessage<GameRoomInfo>(message, \"stGameRoomInfo\");\r\n\t\t\tVGivenCards = parser.ParseFieldRepeated<int>(message, \"vGivenCards\");\r\n\t\t\tVGivenHands = parser.ParseFieldRepeated<int>(message, \"vGivenHands\");\r\n\t\t\tUuid = parser.ParseField<long>(message, \"uuid\");\r\n\t\t}\r\n\t}\r\n}\r\n", res);
        }
    }
}
