﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Remoting.Channels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PokerMaster.Packets;
using PokerMaster.Utils;

namespace PokerMaster.Tests
{
    [TestClass]
    public class DecryptorTests
    {
        [TestMethod]
        public void TestDecrypt()
        {
            string input = "b6:c0:e8:41:40:8d:7d:eb:96:b4:96:98:d3:9b:06:06:c4:72:87:5f:fd:35:2f:e0:85:18:37:4a:c6:17:b8:3f:b8:30:d9:2c:8f:82:1e:fb:57:57:a8:40:08:dd:b2:89:7f:a9:08:0b:24:d5:1e:56:cb:52:31:09:08:44:a9:5e:4e:07:3a:60:6d:fd:a9:78:7f:de:8b:0d:5b:9a:de:12:02:02:2a:9f:ad:66:68:23:74:fc:83:b8:8d:08:f4:ae:02:cc:84:43:a3:d5:60:6f:28:23:22:ac:03:72:a4:db:9d:20:6c:de:9c:04:0d:45:58:40:9e:8d:7d:18:1f:aa:e0:de:c6:65:b1:9e:94:27:70:4e:0a:39:42:b7:20:c6:ff:87:01:68:b2:a6:3b:88:5f:15:48:06:ab:9c:8c:77:27:87:a4:6d:a1:39:01:d9:c6:46:12:2d:33:02:ac:8f:3c:3d:88:0b:d3:28:b4:38:17:21:a0:74:23:87:32:6a:72:e6:23:53:a0:5d:b2:64:93:84:49:d5:09:83:2b:70:80:df:f3:88:47:5e:e4:b6:3c:33:75:c2:85:10:1f:1d:86:fa:d3:83:af:25:33:38:70:bb:c8:5d:98:da:ff:ec:f7:1e:d9:24:c1:dd:59:cf:1d:5d:97:7f:66:c0:0c:04:40:26:c4:2b:2b:79:15:75:af:61:d1:df:50:26:75:0c:fd:82:66:32:24:54:e0:ce:4a:95:6b:f1:0e:e6:71:62:6b:52:81:a8:3c:81:02:25:72:13:8e:29:56:74:d9:b6:08:31:73:ef:ac:74:a1:90:62:0c:65:38:6a:49:0d:33:54:0f:cf:a3:cc:19:bd:9a:95:9c:68:05:f0:bd:90:a3:d4:ac:dc:20:83:9e:1a:a8:63:92:d2:24:ff:3f:4e:db:77:60:ed:c7:89:b8:66:ff:e7:80:e3:9a:4a:d7:71:7f:42:c5:8e:95:45:0e:51:da:65:7f:a7:97:c0:df:c9:04:75:de:d6:fd:18:ed:58:36:5d:a2:f2:d7:53:84:33:6e:4e:60:66:e0:f2:69:25:f2:99:31:bc:36:7b:b4:fa:18:94:02:75:ff:7d:e3:18:24:04:17:0d:f0:a3:60:65:bd:35:1a:fa:8b:b1:6f:91:86:6a:07:c9:f0:80:12:8f:c5:6b:4e:6e:04:03:29:73:78:7d:fa:ff:3e:34:42:de:a9:47:44:26:db:db:00:73:73:87:b2:7f:35:d4:f7:49:f5:de:3b:b8:b2:8e:db:f4:72:37:f9:c6:0b:66:27:c8:85:bc:5b:d0:75:a8:f8:c4:bd:71:f7:89:3a:cf:dd:77:9b:ff:04:e2:c6:9a:57:f0:0b:db:d6:68:49:13:aa:3f:58:6e:df:3a:b0:57:4f:61:b2:57:b4:6e:80:71:8b:77:2e:8e:fe:bc:25:94:d2:33:46:1d:58:08:d3:c4:bf:54:a1:d1:97:b8:a8:e1:1f:37:d3:03:89:c3:87:c0:3f:a2:29:1f:a3:8a:6f:57:8a:85:c3:23:8c:ae:17:ee:54:94:6d:59:fa:fe:eb:f9:d6:3b:7c:86:a2:43:a9:e0:be:36:6a:89:8f:df:57:c9:b0:99:6e:09:d5:df:0c:95:de:8b:52:79:a2:cb:82:77:b1:58:61:d4:bc:6f:5d:a1:5e:e3:ec:21:5e:63:91:f6:38:00:8c:f3:3c:ce:80:a1:ad:82:e8:93:91:6f:61:cf:26:01:b4:ba:ce:e4:a5:49:37:78:24:a6:eb:ba:dc:03:d2:8d:2c:1b:49:df:d7:8b:11:4d:87:6f:3b:02:81:97:bb:73:da:45:e2:d6:06:56:1c:5c:98:06:06:36:ce:2f:58:7e:31:92:50:a2:8d:7b:c7:90:27:2e:b5:2d:80:b4:26:8d:06:36:4d:9f:22:6b:f8:4e:79:a4:7d:de:69:1c:84:1d:f7:60:09:2e:94:eb:4e:60:5c:f2:b8:d2:c4:a6:f1:7c:4f:3b:a2:a5:7d:18:24:36:ff:a2:28:36:b5:22:f6:d4:ec:d2:cc:e3:b7:b3:cc:79:f3:43:ed:42:36:dc:49:7d:16:da:ee:0e:f6:19:1a:27:07:18:e5:6a:7c:d2:d8:66:30:56:3b:b6:57:0d:7c:a6:23:87:63:05:db:48:3b:dc:78:df:af:c2:54:dc:d8:41:48:15:f5:9c:90:00:c9:be:ed:1e:a6:f1:08:54:99:b1:3d:d6:ee:6f:9c:b3:cc:78:23:d0:a8:b5:c9:48:d2:86:e3:01:f8:31:d2:ac:df:88:ea:c9:3e:e0:1d:84:7e:bc:b8:6f:4f:65:2c:d0:be:5e:2c:1f:96:aa:6b:87:5e:cd:9f:53:6a:53:b4:2f:8c:13:d0:a7:da:4d:0e:e0:d8:b3:f4:ce:54:8e:ac:36:a4:9a:36:6a:f2:aa:17:89:9a:e4:5c:65:56:34:eb:df:7e:29:f2:06:28:62:f2:51:03:8e:fc:68:ab:6b:75:d7:b2:de:9f:34:d3:ea:12:b0:0b:6c:f1:17:8e:77:75:60:46:5d:23:6a:62:61:a5:e0:90:49:70:93:2e:26:c5:e1:6b:b8:58:bd:f5:b7:24:99:1f:12:03:89:f5:ef:76:45:4d:65:19:4a:fa:fc:f6:cc:76:e0:e2:5f:9e:e2:e8:58:a0:8a:65:61:67:66:57:4e:4f:aa:11:5e:40:bc:e2:42:25:bd:97:8f:64:5f:20:50:53:b4:98:52:e9:f8:93:b6:4e:86:51:";
            string key = "116ff58c0b178429";
            string expectedOutput = "08:00:12:d7:01:0a:c8:01:08:a3:e1:24:10:04:1a:0b:31:37:31:38:37:37:37:38:35:34:32:22:2f:68:74:74:70:3a:2f:2f:75:70:79:75:6e:2e:70:6f:6b:65:72:6d:61:74:65:2e:6e:65:74:2f:69:6d:61:67:65:73:2f:6d:61:6c:65:5f:68:65:61:64:2e:70:6e:67:2a:09:e8:8d:b7:e5:85:b0:e8:b1:86:32:0b:31:37:31:38:37:37:37:38:35:34:32:38:01:40:01:48:81:ca:ae:d1:e1:2b:52:00:58:f0:15:60:b8:30:68:00:70:00:78:00:80:01:00:88:01:00:92:01:2f:68:74:74:70:3a:2f:2f:75:70:79:75:6e:2e:70:6f:6b:65:72:6d:61:74:65:2e:6e:65:74:2f:69:6d:61:67:65:73:2f:6d:61:6c:65:5f:68:65:61:64:2e:70:6e:67:b8:01:02:c0:01:02:ca:01:02:38:36:d8:01:01:e2:01:0a:31:34:34:37:30:35:36:38:34:32:10:02:18:04:20:01:28:00:38:00:48:00:22:88:06:08:0c:10:01:18:00:20:0f:28:01:28:02:28:05:28:0a:28:14:28:19:28:32:28:64:28:c8:01:28:ac:02:28:f4:03:28:e8:07:30:88:0e:30:90:1c:30:98:2a:30:a0:38:30:b0:54:30:e0:a8:01:30:c0:d1:02:30:80:a3:05:30:80:e9:0f:30:80:f5:24:38:05:40:01:48:c8:01:52:06:08:0a:10:14:18:00:52:06:08:0f:10:1e:18:00:52:06:08:14:10:28:18:00:52:06:08:1e:10:3c:18:00:52:06:08:28:10:50:18:00:52:06:08:32:10:64:18:00:52:07:08:4b:10:96:01:18:00:52:07:08:5a:10:b4:01:18:00:52:07:08:64:10:c8:01:18:00:52:07:08:7d:10:fa:01:18:00:52:08:08:96:01:10:ac:02:18:00:52:08:08:c8:01:10:90:03:18:00:52:08:08:fa:01:10:f4:03:18:00:52:08:08:ac:02:10:d8:04:18:00:52:08:08:90:03:10:a0:06:18:00:52:08:08:f4:03:10:e8:07:18:00:52:08:08:d8:04:10:b0:09:18:00:52:08:08:bc:05:10:f8:0a:18:00:52:08:08:a0:06:10:c0:0c:18:00:52:08:08:e8:07:10:d0:0f:18:00:52:08:08:b0:09:10:e0:12:18:00:52:08:08:dc:0b:10:b8:17:18:00:52:08:08:88:0e:10:90:1c:18:00:52:08:08:d0:0f:10:a0:1f:18:00:52:08:08:c4:13:10:88:27:18:00:52:08:08:b8:17:10:f0:2e:18:00:52:08:08:a0:1f:10:c0:3e:18:00:52:08:08:88:27:10:90:4e:18:00:52:08:08:c0:3e:10:80:7d:18:00:52:09:08:90:4e:10:a0:9c:01:18:00:52:09:08:e0:5d:10:c0:bb:01:18:00:52:09:08:98:75:10:b0:ea:01:18:00:52:0a:08:a0:9c:01:10:c0:b8:02:18:00:5a:05:08:01:10:9c:18:5a:05:08:02:10:c0:0c:5a:05:08:03:10:e8:07:5a:05:08:04:10:a0:06:5a:05:08:05:10:d8:04:5a:05:08:06:10:f4:03:5a:05:08:07:10:90:03:5a:05:08:08:10:de:02:5a:05:08:09:10:ac:02:5a:05:08:0a:10:fa:01:5a:05:08:0b:10:e6:01:5a:05:08:0c:10:c8:01:5a:05:08:0d:10:b4:01:5a:05:08:0e:10:a0:01:5a:05:08:0f:10:8c:01:5a:05:08:10:10:82:01:5a:04:08:11:10:78:5a:04:08:12:10:6e:5a:04:08:13:10:64:5a:04:08:14:10:50:60:01:6a:06:08:e8:07:18:c8:01:6a:04:08:32:18:46:6a:04:08:01:18:0a:6a:04:08:64:18:50:6a:04:08:02:18:14:6a:05:08:c8:01:18:5a:6a:04:08:14:18:32:6a:04:08:05:18:1e:6a:05:08:f4:03:18:78:6a:04:08:19:18:3c:6a:04:08:0a:18:28:6a:05:08:ac:02:18:64:72:06:08:e8:07:18:a0:1f:72:05:08:32:18:c8:01:72:04:08:01:18:1e:72:05:08:64:18:ac:02:72:04:08:02:18:3c:72:06:08:c8:01:18:90:03:72:05:08:14:18:c8:01:72:04:08:05:18:78:72:06:08:f4:03:18:dc:0b:72:05:08:19:18:c8:01:72:05:08:0a:18:c8:01:72:06:08:ac:02:18:84:07:7a:05:08:e8:07:18:0a:7a:04:08:32:18:0a:7a:04:08:01:18:0a:7a:04:08:64:18:0a:7a:04:08:02:18:0a:7a:05:08:c8:01:18:0a:7a:04:08:14:18:0a:7a:04:08:05:18:0a:7a:05:08:f4:03:18:0a:7a:04:08:19:18:0a:7a:04:08:0a:18:0a:7a:05:08:ac:02:18:0a:80:01:01:2a:10:35:66:34:61:32:39:35:30:37:32:37:34:36:66:35:65:32:10:61:38:35:35:35:66:39:66:39:64:37:33:38:32:36:34";

            Decryptor decryptor = new Decryptor();

            var inputBytes = input.ToByteArray();

            var keyBytes = key.ToKeyByteArray();

            var decrypted = decryptor.Decrypt(keyBytes, inputBytes);

            var result = decrypted.ToHexString();

            Assert.AreEqual(expectedOutput, result);
        }

        [TestMethod]
        public void TestEncrypt()
        {
            string input = "b0481938c601e9ff";
            string key = "116ff58c0b178429";
            string expectedOutput = "0e:50:8d:7d:55:8e:9b:1d:29:ca:b9:03:ba:3b:5e:ff:25:d0:97:7b:a6:97:43:a5:5d:c7:b9:f3:87:e4:32:8e";

            Decryptor decryptor = new Decryptor(18222);

            var inputBytes = input.ToKeyByteArray();

            var keyBytes = key.ToKeyByteArray();

            var encrypted = decryptor.Encrypt(keyBytes, inputBytes);

            var result = encrypted.ToHexString();

            Assert.AreEqual(expectedOutput, result);
        }

        [TestMethod]
        public void TestKeyEncrypt()
        {
            Decryptor decryptor = new Decryptor(18222);

            string key = "2760e9904b883c65";
            var encrypted = decryptor.Encrypt(null, key.ToKeyByteArray());
            var actual = encrypted.ToHexString().Replace(":", "").ToUpper();

            Assert.AreEqual("0E508D7D558E9B1DD135892904D77D7B33C7314F49C33499AB7980E26355AB92", actual);
        }
    }
}
