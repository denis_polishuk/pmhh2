﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokerMaster.Utils
{
    public static class ByteHelper
    {
        public static byte[] ToByteArray(this string hex)
        {
            hex = hex.Replace(":", "");
            return Enumerable.Range(0, hex.Length)
                             .Where(x => x % 2 == 0)
                             .Select(x => Convert.ToByte(hex.Substring(x, 2), 16))
                             .ToArray();
        }

        public static byte[] ToKeyByteArray(this string str)
        {
            return str.Select(x => (byte) x).ToArray();
        }

        public static string ToHexString(this byte[] ba)
        {
            string hex = BitConverter.ToString(ba);
            return hex.Replace("-", ":").ToLower();
        }

        public static uint Ntohl(uint value)
        {
            return (value << 24) | ((value & 0xFF00) << 8) | ((value & 0xFF0000u) >> 8) | (value >> 24);
        }

        public static uint Htonl(uint value)
        {
            return (value << 24) | ((value & 0xFF00) << 8) | ((value & 0xFF0000) >> 8) | (value >> 24);
        }
    }
}
