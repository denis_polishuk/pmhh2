﻿using System;
using System.Collections.Generic;
using System.Linq;
using PokerMaster.Utils;

namespace PokerMaster.Packets
{ 
    public class Decryptor
    {
        private const string DefaultKey = "116ff58c0b178429";

        private const int SALT_LEN = 2;
        private const int ZERO_LEN = 7;
        private const int ROUNDS = 16;
        private const int LOG_ROUNDS = 4;
        private const int DELTA = -1640531527;

        private Random Random { get; }

        public Decryptor(int randomSeed = -1)
        {
            if (randomSeed == -1) randomSeed = Environment.TickCount;

            Random = new Random(randomSeed);
        }

        public byte[] Decrypt(byte[] sIn)
        {
            return Decrypt(null, sIn);
        }

        public byte[] Decrypt(byte[] key, byte[] sIn)
        {
            if (key == null) key = DefaultKey.ToKeyByteArray();

            List<byte> v = new List<byte>();

            Oi_symmetry_decrypt(sIn, key, v);

            return v.ToArray();
        }

        public byte[] Encrypt(byte[] sIn)
        {
            return Encrypt(null, sIn);
        }

        public byte[] Encrypt(byte[] key, byte[] sIn)
        {
            if (key == null) key = DefaultKey.ToKeyByteArray();

            List<byte> v = new List<byte>();

            Oi_symmetry_encrypt(sIn, key, v);

            return v.ToArray();
        }

        private byte Rand()
        {
            var result = new byte[1];
            Random.NextBytes(result);
            return result[0];
        }

        private void TeaDecryptEcb(byte[] pInBuf, int offset, byte[] pKey, List<byte> outBuf)
        {
            unchecked
            {
                outBuf.Clear();

                uint y, z, sum;
                uint[] k = new uint[4];
                int i;

                /*now encrypted buf is TCP/IP-endian;*/
                /*TCP/IP network byte order (which is big-endian).*/
                y = ByteHelper.Ntohl(BitConverter.ToUInt32(pInBuf, offset));
                z = ByteHelper.Ntohl(BitConverter.ToUInt32(pInBuf, offset + 4));

                for (i = 0; i < 4; i++)
                {
                    /*key is TCP/IP-endian;*/
                    k[i] = ByteHelper.Ntohl(BitConverter.ToUInt32(pKey, i * 4));
                }

                sum = (uint)(DELTA << LOG_ROUNDS);
                for (i = 0; i < ROUNDS; i++)
                {
                    z -= (y << 4) + (k[2] ^ y) + (sum ^ (y >> 5)) + k[3];
                    y -= (z << 4) + (k[0] ^ z) + (sum ^ (z >> 5)) + k[1];
                    sum = (uint)(sum - DELTA);
                }

                outBuf.AddRange(BitConverter.GetBytes(ByteHelper.Htonl(y)));
                outBuf.AddRange(BitConverter.GetBytes(ByteHelper.Htonl(z)));

                /*now plain-text is TCP/IP-endian;*/
            }
        }

        private void TeaEncryptECB(byte[] pInBuf, int offset, byte[] pKey, List<byte> outBuf)
        {
            unchecked
            {
                uint y, z;
                uint sum;
                uint[] k = new uint[4];
                int i;

                /*plain-text is TCP/IP-endian;*/

                /*GetBlockBigEndian(in, y, z);*/
                y = ByteHelper.Ntohl(BitConverter.ToUInt32(pInBuf, offset));
                z = ByteHelper.Ntohl(BitConverter.ToUInt32(pInBuf, offset + 4));
                /*TCP/IP network byte order (which is big-endian).*/

                for (i = 0; i < 4; i++)
                {
                    /*key is TCP/IP-endian;*/
                    k[i] = ByteHelper.Ntohl(BitConverter.ToUInt32(pKey, i*4));
                }

                sum = 0;
                for (i = 0; i < ROUNDS; i++)
                {
                    sum = (uint) (sum + DELTA);
                    y += (z << 4) + (k[0] ^ z) + (sum ^ (z >> 5)) + k[1];
                    z += (y << 4) + (k[2] ^ y) + (sum ^ (y >> 5)) + k[3];
                }


                outBuf.AddRange(BitConverter.GetBytes(ByteHelper.Htonl(y)));
                outBuf.AddRange(BitConverter.GetBytes(ByteHelper.Htonl(z)));

                /*now encrypted buf is TCP/IP-endian;*/
            }
        }

        private void Oi_symmetry_encrypt(byte[] pInBuf, byte[] pKey, List<byte> pOutBuf)
        {
            int nPadSaltBodyZeroLen /*PadLen(1byte)+Salt+Body+Zero的长度*/;
            byte nPadlen;
            byte[] src_buf = new byte[8];
            byte[] zero_iv = new byte[8];
            byte[] iv_buf = new byte[8];
            ;
            int src_i, i, j;



            /*根据Body长度计算PadLen,最小必需长度必需为8byte的整数倍*/
            nPadSaltBodyZeroLen = pInBuf.Length /*Body长度*/+ 1 + SALT_LEN + ZERO_LEN
                /*PadLen(1byte)+Salt(2byte)+Zero(7byte)*/;
            if ((nPadlen = (byte) (nPadSaltBodyZeroLen%8)) != 0) /*len=nSaltBodyZeroLen%8*/
            {
                /*模8余0需补0,余1补7,余2补6,...,余7补1*/
                nPadlen = (byte) (8 - nPadlen);
            }

            /*srand( (unsigned)time( NULL ) ); 初始化随机数*/
            /*加密第一块数据(8byte),取前面10byte*/
            src_buf[0] = (byte) ((Rand() & 0x0f8) /*最低三位存PadLen,清零*/| nPadlen);
            src_i = 1; /*src_i指向src_buf下一个位置*/

            while (nPadlen-- > 0)
                src_buf[src_i++] = Rand(); /*Padding*/

            /*come here, i must <= 8*/

            Array.Copy(zero_iv, iv_buf, 8);

            int outOffset = 0;

            for (i = 1; i <= SALT_LEN;) /*Salt(2byte)*/
            {
                if (src_i < 8)
                {
                    src_buf[src_i++] = Rand();
                    i++; /*i inc in here*/
                }

                if (src_i == 8)
                {
                    /*src_i==8*/

                    for (j = 0; j < 8; j++) /*CBC XOR*/
                        src_buf[j] ^= iv_buf[j];
                    /*pOutBuffer、pInBuffer均为8byte, pKey为16byte*/
                    TeaEncryptECB(src_buf, 0, pKey, pOutBuf);
                    src_i = 0;
                    iv_buf = pOutBuf.GetRange(outOffset, 8).ToArray();
                    outOffset += 8;
                }
            }

            /*src_i指向src_buf下一个位置*/
            int nInBufLen = pInBuf.Length;
            int inOffset = 0;

            while (nInBufLen > 0)
            {
                if (src_i < 8)
                {
                    src_buf[src_i++] = pInBuf[inOffset++];
                    nInBufLen--;
                }

                if (src_i == 8)
                {
                    /*src_i==8*/

                    for (i = 0; i < 8; i++) /*CBC XOR*/
                        src_buf[i] ^= iv_buf[i];
                    /*pOutBuffer、pInBuffer均为8byte, pKey为16byte*/
                    TeaEncryptECB(src_buf, 0, pKey, pOutBuf);
                    src_i = 0;
                    iv_buf = pOutBuf.GetRange(outOffset, 8).ToArray();
                    outOffset += 8;
                }
            }

            /*src_i指向src_buf下一个位置*/

            for (i = 1; i <= ZERO_LEN;)
            {
                if (src_i < 8)
                {
                    src_buf[src_i++] = 0;
                    i++; /*i inc in here*/
                }

                if (src_i == 8)
                {
                    /*src_i==8*/

                    for (j = 0; j < 8; j++) /*CBC XOR*/
                        src_buf[j] ^= iv_buf[j];
                    /*pOutBuffer、pInBuffer均为8byte, pKey为16byte*/
                    TeaEncryptECB(src_buf, 0, pKey, pOutBuf);
                    src_i = 0;
                    iv_buf = pOutBuf.GetRange(outOffset, 8).ToArray();
                    outOffset += 8;
                }
            }
        }

        private void Oi_symmetry_decrypt(byte[] pInBuf, byte[] pKey, List<byte> pOutBuf)
        {
            int nPadLen, nPlainLen;
            List<byte> dest_buf = new List<byte>();
            byte[] iv_buf = new byte[8];
            int dest_i, i, j;
            int offset = 0;

            if ((pInBuf.Length % 8 != 0) || (pInBuf.Length < 16)) return;

            TeaDecryptEcb(pInBuf, offset, pKey, dest_buf);

            nPadLen = dest_buf[0] & 0x7 /*只要最低三位*/;

            /*密文格式:PadLen(1byte)+Padding(var,0-7byte)+Salt(2byte)+Body(var byte)+Zero(7byte)*/
            i = pInBuf.Length - 1 /*PadLen(1byte)*/- nPadLen - SALT_LEN - ZERO_LEN; /*明文长度*/
            int outLen = i;

            Array.Copy(pInBuf, offset, iv_buf, 0, 8); /*init iv*/
            offset += 8;

            dest_i = 1; /*dest_i指向dest_buf下一个位置*/

            /*把Padding滤掉*/
            dest_i += nPadLen;

            /*dest_i must <=8*/

            /*把Salt滤掉*/
            for (i = 1; i <= SALT_LEN;)
            {
                if (dest_i < 8)
                {
                    dest_i++;
                    i++;
                }

                if (dest_i == 8)
                {
                    /*dest_i==8*/
                    TeaDecryptEcb(pInBuf, offset, pKey, dest_buf);
                    for (j = 0; j < 8; j++)
                        dest_buf[j] ^= iv_buf[j];

                    Array.Copy(pInBuf, offset, iv_buf, 0, 8);
                    offset += 8;

                    dest_i = 0; /*dest_i指向dest_buf下一个位置*/
                }
            }

            /*还原明文*/

            nPlainLen = outLen;
            while (nPlainLen > 0)
            {
                if (dest_i < 8)
                {
                    pOutBuf.Add(dest_buf[dest_i++]);
                    nPlainLen--;
                }
                else if (dest_i == 8)
                {
                    /*dest_i==8*/
                    TeaDecryptEcb(pInBuf, offset, pKey, dest_buf);
                    for (i = 0; i < 8; i++)
                        dest_buf[i] ^= iv_buf[i];

                    Array.Copy(pInBuf, offset, iv_buf, 0, 8);
                    offset += 8;

                    dest_i = 0; /*dest_i指向dest_buf下一个位置*/
                }
            }

            for (i = 1; i <= ZERO_LEN;)
            {
                if (dest_i < 8)
                {
                    if (dest_buf[dest_i++] != 0) return;
                    i++;
                }
                else if (dest_i == 8)
                {
                    /*dest_i==8*/
                    TeaDecryptEcb(pInBuf, offset, pKey, dest_buf);
                    for (j = 0; j < 8; j++)
                        dest_buf[j] ^= iv_buf[j];

                    Array.Copy(pInBuf, offset, iv_buf, 0, 8);
                    offset += 8;

                    dest_i = 0;
                }
            }
        }
    }
}
