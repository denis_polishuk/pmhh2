﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokerMaster.Packets
{
    public class Replay
    {
        public void ReplayPackets(string processorId)
        {
            using (var messageProcessor = new MessageProcessor(null, processorId, true, false, false))
            using (var dump = new Dump(processorId + ".bin"))
            {
                var packets = dump.ReadAll();
                int packet = 0;

                foreach (var p in packets)
                {
                    if ((packet < 0))//  || (packet > 10000))
                    {
                        packet++;
                        continue;
                    }

                    Console.WriteLine("Packet: " + packet);
                        //File.AppendAllText("error.txt", $"error 1 Packet: " + packet + "\r\n");
                        messageProcessor.OnPacket(p.Port, p.Bytes);

                    packet++;
                }
            }
        }
    }
}
