﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokerMaster.Packets
{
    public class PacketDump
    {
        public byte[] Bytes { get; set; }
        public int Port { get; set; }
    }
}
