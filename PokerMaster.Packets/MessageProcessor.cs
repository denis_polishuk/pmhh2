﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Google.ProtocolBuffers;
using Google.ProtocolBuffers.Descriptors;
using Google.ProtocolBuffers.Serialization;
using PokerMaster.Domain;
using PokerMaster.Domain.Model;
using PokerMaster.Domain.Proto;
using PokerMaster.HandHistory;
using PokerMaster.Overlay;
using PokerMaster.Utils;
using WinAPI;
using System.Runtime.InteropServices;

namespace PokerMaster.Packets
{
    public class MessageProcessor : IDisposable
    {
        private bool sendHHtoFTP;
        private bool sendHeroHHtoFTP;
        private const string KeysDirectory = "keys";
        private string HhDirectory;
        private string hhDirectoryStraddle;
        //жестчайший костыль под 10-ый апдейт почему-то первый пакет следующей раздачи начал приходить последним в текущей раздаче. пытаюсь перекинуть его в следующий пак апдейтов.
        private List<GameRoomInfo> auxFirstUpdate = new List<GameRoomInfo>();

        private readonly TEXAS_CMD[] _noAuthCmds =
        {
            TEXAS_CMD.Cmd_SCLoginRsp, TEXAS_CMD.Cmd_SCLoginThirdPartyRsp, TEXAS_CMD.Cmd_SCRegisterRsp,
            TEXAS_CMD.Cmd_SCUploadVerifyCodeRsp,
            TEXAS_CMD.Cmd_SCRegetVerifyCodeRsp, TEXAS_CMD.Cmd_SCResetPwdUploadPhonenumberRsp,
            TEXAS_CMD.Cmd_SCResetPwdVerifyCodeRsp,
            TEXAS_CMD.Cmd_SCHelloRsp, TEXAS_CMD.Cmd_SCHelloGameRsp, TEXAS_CMD.Cmd_SCReconnectRsp,
            TEXAS_CMD.Cmd_SCKickOffPush,
            TEXAS_CMD.Cmd_SCGetFlashPageInfosRsp, TEXAS_CMD.Cmd_SCGetFestivalModeRsp,
            TEXAS_CMD.Cmd_SCGetGlobalDefaultSettingRsp,
            TEXAS_CMD.Cmd_CSLogin, TEXAS_CMD.Cmd_CSLoginThirdParty, TEXAS_CMD.Cmd_CSRegister,
            TEXAS_CMD.Cmd_CSUploadVerifyCode,
            TEXAS_CMD.Cmd_CSRegetVerifyCode, TEXAS_CMD.Cmd_CSResetPwdUploadPhonenumber,
            TEXAS_CMD.Cmd_CSResetPwdVerifyCode,
            TEXAS_CMD.Cmd_CSHello, TEXAS_CMD.Cmd_CSHelloGame, TEXAS_CMD.Cmd_CSReconnect,
            TEXAS_CMD.Cmd_CSGetFlashPageInfos,
            TEXAS_CMD.Cmd_CSGetFestivalMode, TEXAS_CMD.Cmd_CSGetGlobalDefaultSetting
        };

        private readonly Dictionary<int, List<byte>> _streams = new Dictionary<int, List<byte>>();
        private readonly Dump _dump;
        private readonly ProtocolReader _reader = new ProtocolReader();
        private readonly ProtoParser _parser = new ProtoParser();
        private readonly ProcessTracker _processTracker;
        private readonly Decryptor _decryptor = new Decryptor();
        private readonly HandHistoryGenerator _hhGenerator = new HandHistoryGenerator();
        private readonly HandHistoryWriter _hhWriter = new HandHistoryWriter();
        private readonly Dictionary<long, List<GameRoomInfo>> _currentHandUpdates =
            new Dictionary<long, List<GameRoomInfo>>();

        private Dictionary<long, byte[]> _key = new Dictionary<long, byte[]>();
        private readonly string _processorId;



        [DllImport("PMHelperDLL.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int getKeysAmount();
        [DllImport("PMHelperDLL.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        [return: MarshalAs(UnmanagedType.LPStr)]
        public static extern string getKeyByNumber(int n);
        [DllImport("PMHelperDLL.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        [return: MarshalAs(UnmanagedType.LPStr)]
        public static extern string getUuidByNumber(int n);
        [DllImport("PMHelperDLL.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int installKey(string key_, string uuid_);

        public MessageProcessor(ProcessTracker processTracker, string processorId, bool sendHH_ = false, bool sendHeroHH_ = false, bool dumpPackets = false)
        {
            sendHeroHHtoFTP = sendHeroHH_;
            sendHHtoFTP = sendHH_;

            _processTracker = processTracker;

            if (!Directory.Exists(KeysDirectory))
                Directory.CreateDirectory(KeysDirectory);

            HhDirectory = ConfigurationManager.AppSettings["hh_dir"] ?? "hh";

            if (!Directory.Exists(HhDirectory))
                Directory.CreateDirectory(HhDirectory);


            hhDirectoryStraddle = "hh_straddle";

            if (!Directory.Exists(hhDirectoryStraddle))
                Directory.CreateDirectory(hhDirectoryStraddle);


            _processorId = processorId;


            Encoding enc = Encoding.ASCII;
            int amount = getKeysAmount();
            for (int i = 0; i < amount; i++)
            {

                string s = getKeyByNumber(i);
                int uuid = int.Parse(getUuidByNumber(i));
                if (s != null)
                {
                    _key[uuid] = enc.GetBytes(s);
                }

            }


            //#if DEBUG
                        if (dumpPackets)
                            _dump = new Dump(processorId + ".bin");
            //#endif
        }

        private void ReadKey(string filePath)
        {
            int uuid = int.Parse(Path.GetFileNameWithoutExtension(filePath));
            _key[uuid] = File.ReadAllBytes(filePath);
        }

        private string GetMessageType(TEXAS_CMD cmd)
        {
            return cmd.ToString().Substring(4);
        }

        private List<GameRoomInfo> GetUpdates(long id)
        {
            List<GameRoomInfo> result;
            if (_currentHandUpdates.TryGetValue(id, out result))
            {
                return result;
            }

            result = new List<GameRoomInfo>();
            _currentHandUpdates[id] = result;
            return result;
        } 

 
        private void HandleLogin(byte[] decryptedBytes)
        {
            //Console.WriteLine("New key arrived");

            var login = new SCLoginRsp();
            login.Parse(decryptedBytes, _parser);

            var id = login.StUserBaseInfoNet.Uuid;

            installKey(login.SEncryptKey.ToString(),id.ToString());
            //File.WriteAllText($"{KeysDirectory}\\{id}.txt", login.SEncryptKey);
            
            _key[id] = login.SEncryptKey.ToKeyByteArray();
        }

        private bool IsValidHand(long id)
        {
            var updates = GetUpdates(id);

            if (updates.Count < 3)
            {
                File.AppendAllText("error_hands.txt", "Warning. Updates Count < 3" + Environment.NewLine);
                return false;
            }

            if((!(updates.ElementAt(updates.Count - 1).EGameState == GAME_ROOM_GAME_STATE.ROOM_GAME_STATE_SHOWCARD ||
                updates.ElementAt(updates.Count - 2).EGameState == GAME_ROOM_GAME_STATE.ROOM_GAME_STATE_SHOWCARD)) ||
                    !((updates.ElementAt(updates.Count-2).EGameState == GAME_ROOM_GAME_STATE.ROOM_GAME_STATE_Result ||
                   updates.ElementAt(updates.Count - 3).EGameState == GAME_ROOM_GAME_STATE.ROOM_GAME_STATE_Result
                   )))
            {
                File.AppendAllText("error_hands.txt", "Warning. The hand didn't contain proper ending."+Environment.NewLine);
            }


            var result = updates.Any() &&
                   (updates[0].EGameState == GAME_ROOM_GAME_STATE.ROOM_GAME_STATE_GameStart ||
                   updates[0].EGameState == GAME_ROOM_GAME_STATE.ROOM_GAME_STATE_GameWait) &&
                   (updates.Last().EGameState == GAME_ROOM_GAME_STATE.ROOM_GAME_STATE_SHOWCARD ||
                    updates.ElementAt(updates.Count-2).EGameState == GAME_ROOM_GAME_STATE.ROOM_GAME_STATE_SHOWCARD ||
                    updates.Last().EGameState == GAME_ROOM_GAME_STATE.ROOM_GAME_STATE_Result    
                    ) &&
                   updates.Any(x => x.VUserGameInfos.Any(y => y.LBetStacks > 0));

            return result;
        }

        private string GetFileName(HandInfo hand)
        {
            string hhDir = HhDirectory;

            if (hand.Straddle > 0)
                hhDir = hhDirectoryStraddle;

            if (hand.isOmaha)
            {
                return $"{hhDir}\\PokerMaster id{hand.Id / 10000}-{hand.SmallBlind}-{hand.BigBlind}-PotLimitOmaha-WinningPoker-{DateTime.Today.ToString("MM-dd-yyyy")}.txt";
            }
            return $"{hhDir}\\PokerMaster id{hand.Id / 10000}-{hand.SmallBlind}-{hand.BigBlind}-NoLimitHoldem-WinningPoker-{DateTime.Today.ToString("MM-dd-yyyy")}.txt";
        }

        private void HandleStateUpdate(int port, long id, GameRoomInfo state)
        {
            //Console.WriteLine($"Table update arrived: {state.StGameRoomBaseInfo.LGameRoomId} on {port}");
            Console.WriteLine($"Table update arrived: {state.StGameRoomBaseInfo.LGameRoomId}");

            _processTracker?.PortUpdate(port, id, state.StGameRoomBaseInfo.LGameRoomId + "", state.StGameRoomBaseInfo.LSmallBlinds, state.StGameRoomBaseInfo.LBigBlinds);
            //id = user id
            var updates = GetUpdates(id);

            



            if (updates.Any())
            {
                var previousUpdate = updates[0];

                if (previousUpdate.LGameHandID == state.LGameHandID &&
                    previousUpdate.StGameRoomBaseInfo.LGameRoomId ==
                    state.StGameRoomBaseInfo.LGameRoomId)
                {

                }
                else
                {
#if DEBUG
                    if (updates[0] != null)
                    {

                        if (updates[0].LGameHandID == 131)
                        {
                            Console.WriteLine("adasas");
                        }

                    }
#endif


                    //продолжаем костыль десятой версии
                    if (auxFirstUpdate.Count>0)
                    {
                        GameRoomInfo tempUpdate =  auxFirstUpdate.FirstOrDefault(x => (x.LGameHandID == updates[0].LGameHandID)&&(x.StGameRoomBaseInfo.LGameRoomId==updates[0].StGameRoomBaseInfo.LGameRoomId));

                        if (tempUpdate != null)
                        {
                            updates.Insert(0, tempUpdate);
                            auxFirstUpdate.Remove(tempUpdate);
                        }
                    }

                    if (updates.Last().EGameState == GAME_ROOM_GAME_STATE.ROOM_GAME_STATE_GameStart)
                    {
                        auxFirstUpdate.Add(updates.Last()); 
                        auxFirstUpdate.Last().LGameHandID++;
                        //updates.RemoveAt(updates.Count - 1);
                    }



                    if (IsValidHand(id))
                    {
                        try
                        {


                            var hand = _hhGenerator.GenerateHand(updates);
                            var builder = new StringBuilder();
                            _hhWriter.Write(hand, builder);

                            var s = builder.ToString();
                            var name = GetFileName(hand);
                            File.AppendAllText(name, s);

                            if (sendHHtoFTP)
                            {
                                builder = new StringBuilder();
                                _hhWriter.Write(hand, builder, true);
                                s = builder.ToString();
                                

                                if (sendHeroHHtoFTP || (!hand.isHeroInHand()))
                                HHSender.prepareHandForSending(s, hand);
                            }
                        }
                        catch (Exception e)
                        {
                            //Console.WriteLine($"Failed to generate hand. Exception {e}");

                        }
                    }

                    _currentHandUpdates[id].Clear();
                }
            }

            updates.Add(state);
        }

        private void HandlePackage(int port, Package package)
        {
            var id = package.Uuid;

            var messageType = GetMessageType(package.ECmd);

            byte[] key = null;

            if (!_noAuthCmds.Contains(package.ECmd) && id > 0)
                _key.TryGetValue(id, out key);

            byte[] decryptedBytes;

            if (messageType == "SCUpdate" || messageType == "CSUpdate")
            {
                decryptedBytes = package.Body;
            }
            else
            {
                decryptedBytes = _decryptor.Decrypt(key, package.Body);
            }

            switch (package.ECmd)
            {
                case TEXAS_CMD.Cmd_SCLoginRsp:
                    HandleLogin(decryptedBytes);
                    break;

                case TEXAS_CMD.Cmd_SCGameRoomStateChange:

                    var stateChange = new SCGameRoomStateChange();
                    stateChange.Parse(decryptedBytes, _parser);

                    HandleStateUpdate(port, id, stateChange.StGameRoomInfo);
                    break;
            }
        }

        public void OnPacket(int dstPort, byte[] bytes)
        {
            _dump?.Write(dstPort, bytes);

            List<byte> stream;

            if (!_streams.TryGetValue(dstPort, out stream))
            {
                stream = new List<byte>();
                _streams[dstPort] = stream;
            }

            stream.AddRange(bytes);

            try
            {
                var data = _reader.ReadPacket(stream);

                if (data == null) return;

                stream.RemoveRange(0, 5 + data.Length);

                if (stream.Count == 0)
                    _streams.Remove(dstPort);

                TEXAS_CMD cmd = 0;
                try
                {
                    var package = new Package();
                    package.Parse(data, _parser);

                    cmd = package.ECmd;

                    Console.WriteLine($"Received package with {cmd} command");

                    HandlePackage(dstPort, package);
                }
                catch (Exception ex)
                {
                    string exString = ex.ToString();
                    //Console.WriteLine($"Error: Message type: {cmd}\r\nException: {ex.Message}\r\n");

                    if (exString.Contains("Google"))
                    {
                       // File.AppendAllText("error.txt",
                       //   $"Time: {DateTime.Now} error 3 Google\r\n\r\n");
                    }
                    else
                    {
#if DEBUG
                            File.AppendAllText("error.txt",
                              $"Time: {DateTime.Now}\r\nMessage error 3: {cmd}\r\nException: {ex}\r\n\r\n");
#endif
                    }

                }
            }
            catch (Exception ex)
            {
                string exString = ex.ToString();
#if DEBUG
                if (exString.Contains("Google"))
                {
                    File.AppendAllText("error.txt",
                      $"Time: {DateTime.Now} error 4 Google\r\n\r\n");
                }
                else
                {
                    File.AppendAllText("error.txt", $"Time: {DateTime.Now}\r\nException error 4: {ex}\r\n\r\n");
                }

                    //Console.WriteLine($"Error. Exception {ex.Message}");
                    File.AppendAllText("error.txt", $"Time: {DateTime.Now} error 4\r\nException: {ex}\r\n\r\n");
#endif
            }
        }

        public void Dispose()
        {
            try
            {
                _dump?.Dispose();
            }
            catch
            {
                // ignore
            }
        }
    }
}
