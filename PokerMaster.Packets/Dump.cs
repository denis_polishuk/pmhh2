﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokerMaster.Packets
{
    public class Dump : IDisposable
    {
        private readonly string _filePath;
        private readonly Stream _stream;

        public Dump(string filePath)
        {
            _filePath = filePath;
            _stream = new FileStream(filePath, FileMode.Append);
        }

        public void Dispose()
        {
            try
            {
                _stream?.Dispose();
            }
            catch
            {
                // ignore
            }
        }

        public void Write(PacketDump dump)
        {
            Write(dump.Port, dump.Bytes);
        }

        public void Write(int port, byte[] bytes)
        {
            _stream.Write(new byte[] { 0x42 }, 0, 1);
            _stream.Write(BitConverter.GetBytes(port), 0, 4);
            _stream.Write(BitConverter.GetBytes(bytes.Length), 0, 4);
            _stream.Write(bytes, 0, bytes.Length);
            _stream.Flush();
        }

        public IEnumerable<PacketDump> ReadAll()
        {
            _stream.Close();

            using (var stream = new FileStream(_filePath, FileMode.Open))
            {
                while (stream.ReadByte() == 0x42)
                {
                    byte[] portBytes = new byte[4];
                    if (stream.Read(portBytes, 0, 4) == 0) break;

                    int port = BitConverter.ToInt32(portBytes, 0);

                    byte[] lengthBytes = new byte[4];
                    if (stream.Read(lengthBytes, 0, 4) == 0) break;

                    int length = BitConverter.ToInt32(lengthBytes, 0);

                    byte[] bytes = new byte[length];
                    if (stream.Read(bytes, 0, length) == 0) break;

                    var packet = new PacketDump
                    {
                        Bytes = bytes,
                        Port = port
                    };

                    yield return packet;
                }
            }   
        }
    }
}
