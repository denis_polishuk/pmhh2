﻿using System.IO;
using System.Text;
using Google.ProtocolBuffers;
using Google.ProtocolBuffers.Descriptors;
using PokerMaster.Utils;

namespace PokerMaster.Packets
{
    public class Package
    {
        public long Uuid { get; private set; }
        public long SeqNo { get; private set; }
        public string Command { get; private set; }
        public byte[] Body { get; private set; }

        public void Parse(byte[] bytes, ProtoParser parser)
        {
            var parsed = parser.ParseMessage(bytes, "TexasPokerCommon", "Package");

            Uuid = (long)parsed["uuid"];
            SeqNo = (long)parsed["iSeqNo"];
            Command = ((EnumValueDescriptor) parsed["eCmd"]).Name;
            Body = ((ByteString) parsed["body"]).ToByteArray();
        }

        public void Dump(string filePath)
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendLine("[Package]");
            builder.AppendLine($"Uuid: {Uuid}");
            builder.AppendLine($"SeqNo: {SeqNo}");
            builder.AppendLine($"Command: {Command}");
            builder.AppendLine($"Body: {Body.ToHexString()}");

            File.AppendAllText(filePath, builder.ToString());
        }
    }
}
