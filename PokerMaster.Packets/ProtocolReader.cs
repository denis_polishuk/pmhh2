﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Google.ProtocolBuffers;
using PokerMaster.Utils;

namespace PokerMaster.Packets
{
    public class ProtocolReader
    {
        private long RemainingBytes(List<byte> bytes, int position)
        {
            return bytes.Count - position;
        }

        private int ReadInt(List<byte> bytes, ref int position)
        {
            byte[] value = bytes.GetRange(position, 4).ToArray();

            uint t = BitConverter.ToUInt32(value, 0);

            var res = (int)ByteHelper.Ntohl(t);

            position += 4;

            return res;
        }

        public byte[] ReadPacket(List<byte> bytes)
        {
            unchecked
            {
                int position = 0;

                long remaining = RemainingBytes(bytes, position);

                if (remaining < 5 || remaining <= 1)
                {
                    return null;
                }

                var checkByte = bytes[position++];
                if (checkByte != (byte) -2)
                {
                    bytes.Clear();
                    return null;
                }

                int i = ReadInt(bytes, ref position);

                // sanity check, doubt we're gonna get an update bigger than 5kb
                if (i > 1024 * 5 || i < 4)
                {
                    bytes.Clear();
                    return null;
                }

                if (i > RemainingBytes(bytes, position) + 5)
                {
                    return null;
                }

                byte[] res = bytes.GetRange(position, i - 5).ToArray();

                return res;
            }
        }
    }
}
