using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class CSSetChatRoomInfo : DomainModel
	{
		

		public long Uuid { get; set; }
		public long LRoomID { get; set; }
		public string SRoomName { get; set; }
		public int NOwnerInvite { get; set; }
		public int NOwnerCreate { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			Uuid = parser.ParseField<long>(message, "uuid");
			LRoomID = parser.ParseField<long>(message, "lRoomID");
			SRoomName = parser.ParseField<string>(message, "sRoomName");
			NOwnerInvite = parser.ParseField<int>(message, "nOwnerInvite");
			NOwnerCreate = parser.ParseField<int>(message, "nOwnerCreate");
		}
	}
}
