using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class CSShowCard : DomainModel
	{
		

		public long Uuid { get; set; }
		public long LGameRoomID { get; set; }
		public long LGameID { get; set; }
		public int BShowCardOne { get; set; }
		public int BShowCardTwo { get; set; }
		public int BShowCardThree { get; set; }
		public int BShowCardFour { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			Uuid = parser.ParseField<long>(message, "uuid");
			LGameRoomID = parser.ParseField<long>(message, "lGameRoomID");
			LGameID = parser.ParseField<long>(message, "lGameID");
			BShowCardOne = parser.ParseField<int>(message, "bShowCardOne");
			BShowCardTwo = parser.ParseField<int>(message, "bShowCardTwo");
			BShowCardThree = parser.ParseField<int>(message, "bShowCardThree");
			BShowCardFour = parser.ParseField<int>(message, "bShowCardFour");
		}
	}
}
