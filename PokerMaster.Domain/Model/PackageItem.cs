using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class PackageItem : DomainModel
	{
		

		public TicketInfo VTicketInfos { get; set; }
		public int INum { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			VTicketInfos = parser.ParseMessage<TicketInfo>(message, "vTicketInfos");
			INum = parser.ParseField<int>(message, "iNum");
		}
	}
}
