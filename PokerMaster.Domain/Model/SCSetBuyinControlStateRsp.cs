using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class SCSetBuyinControlStateRsp : DomainModel
	{
		

		public ERROR_CODE_TYPE EErrCode { get; set; }
		public GameRoomBaseInfo StGameRoomBaseInfo { get; set; }
		public long LGameRoomID { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			EErrCode = parser.ParseEnum<ERROR_CODE_TYPE>(message, "eErrCode");
			StGameRoomBaseInfo = parser.ParseMessage<GameRoomBaseInfo>(message, "stGameRoomBaseInfo");
			LGameRoomID = parser.ParseField<long>(message, "lGameRoomID");
		}
	}
}
