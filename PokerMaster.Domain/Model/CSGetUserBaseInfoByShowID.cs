using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class CSGetUserBaseInfoByShowID : DomainModel
	{
		

		public long Uuid { get; set; }
		public string SShowID { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			Uuid = parser.ParseField<long>(message, "uuid");
			SShowID = parser.ParseField<string>(message, "sShowID");
		}
	}
}
