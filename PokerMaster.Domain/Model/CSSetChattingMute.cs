using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class CSSetChattingMute : DomainModel
	{
		

		public long Uuid { get; set; }
		public int IEnabled { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			Uuid = parser.ParseField<long>(message, "uuid");
			IEnabled = parser.ParseField<int>(message, "iEnabled");
		}
	}
}
