namespace PokerMaster.Domain.Model
{
	public enum BUYIN_CONTROL_TYPE
	{
		BUYIN_CONTROL_TYPE_ACCEPT = 1,
		BUYIN_CONTROL_TYPE_DENY = 2
	}
}
