using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class SCSystemMsg : DomainModel
	{
		

		public SystemPushMsg[] VMsgs { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			VMsgs = parser.ParseMessageRepeated<SystemPushMsg>(message, "vMsgs");
		}
	}
}
