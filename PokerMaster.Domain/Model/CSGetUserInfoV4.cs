using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class CSGetUserInfoV4 : DomainModel
	{
		

		public long Uuid { get; set; }
		public long LGetUuid { get; set; }
		public long LClubID { get; set; }
		public int BRemark { get; set; }
		public string STimeZoneId { get; set; }
		public long LGameRoomID { get; set; }
		public GAME_ROOM_TYPE EGameRoomType { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			Uuid = parser.ParseField<long>(message, "uuid");
			LGetUuid = parser.ParseField<long>(message, "lGetUuid");
			LClubID = parser.ParseField<long>(message, "lClubID");
			BRemark = parser.ParseField<int>(message, "bRemark");
			STimeZoneId = parser.ParseField<string>(message, "sTimeZoneId");
			LGameRoomID = parser.ParseField<long>(message, "lGameRoomID");
			EGameRoomType = parser.ParseEnum<GAME_ROOM_TYPE>(message, "eGameRoomType");
		}
	}
}
