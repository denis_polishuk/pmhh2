using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class CSDeleteUserCover : DomainModel
	{
		

		public long Uuid { get; set; }
		public int IPos { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			Uuid = parser.ParseField<long>(message, "uuid");
			IPos = parser.ParseField<int>(message, "iPos");
		}
	}
}
