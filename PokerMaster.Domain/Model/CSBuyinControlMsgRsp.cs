using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class CSBuyinControlMsgRsp : DomainModel
	{
		

		public long Uuid { get; set; }
		public long LMsgId { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			Uuid = parser.ParseField<long>(message, "uuid");
			LMsgId = parser.ParseField<long>(message, "lMsgId");
		}
	}
}
