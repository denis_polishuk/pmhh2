using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class SCGetGlobalDefaultSettingRsp : DomainModel
	{
		

		public ERROR_CODE_TYPE EErrCode { get; set; }
		public GlobalDefaultSettingInfoNet StGlobalDefaultSettingInfo { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			EErrCode = parser.ParseEnum<ERROR_CODE_TYPE>(message, "eErrCode");
			StGlobalDefaultSettingInfo = parser.ParseMessage<GlobalDefaultSettingInfoNet>(message, "stGlobalDefaultSettingInfo");
		}
	}
}
