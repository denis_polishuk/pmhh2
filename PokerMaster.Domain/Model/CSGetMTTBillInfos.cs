using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class CSGetMTTBillInfos : DomainModel
	{
		

		public long Uuid { get; set; }
		public long LNewestTime { get; set; }
		public int INum { get; set; }
		public string STimeZoneId { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			Uuid = parser.ParseField<long>(message, "uuid");
			LNewestTime = parser.ParseField<long>(message, "lNewestTime");
			INum = parser.ParseField<int>(message, "iNum");
			STimeZoneId = parser.ParseField<string>(message, "sTimeZoneId");
		}
	}
}
