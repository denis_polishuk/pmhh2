namespace PokerMaster.Domain.Model
{
	public enum VIP_TYPE
	{
		BLUE_CARD = 1,
		GOLD_CARD = 2,
		PLATINUM_CARD = 3,
		BLACK_CARD = 4
	}
}
