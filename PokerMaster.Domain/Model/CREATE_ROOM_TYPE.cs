namespace PokerMaster.Domain.Model
{
	public enum CREATE_ROOM_TYPE
	{
		CAHT_ROOM = 1,
		CLUB_ROOM = 2,
		QUICK_GAME_ROOM = 3
	}
}
