using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class PushMsg : DomainModel
	{
		

		public long LMsgID { get; set; }
		public CHAT_TYPE ETalkType { get; set; }
		public CHAT_MESSAGE_TYPE EChatMessageType { get; set; }
		public int BChatRoom { get; set; }
		public UserBaseInfoNet StFromUser { get; set; }
		public UserBaseInfoNet StToUser { get; set; }
		public ChatRoomBaseInfo StChatRoomBaseInfo { get; set; }
		public long LGameRoomID { get; set; }
		public ClubBaseInfo StCluBasebInfo { get; set; }
		public string SMsg { get; set; }
		public byte[] VVoiceDatas { get; set; }
		public ShareBillInfoNet StShareBillInfo { get; set; }
		public PokerProcessInfo StPokerProcessBaseInfo { get; set; }
		public string StrPicUrl { get; set; }
		public long LCreateTime { get; set; }
		public long LDuration { get; set; }
		public int IMute { get; set; }
		public int IVibrate { get; set; }
		public NetPageShareInfoNet StNetPageShareInfoNet { get; set; }
		public SharePokerHistoryInfo StSharePokerHistoryInfo { get; set; }
		public string[] VExtras { get; set; }
		public ShareMTTBillInfoNet StShareMTTBillInfoNet { get; set; }
		public SharePokerHistoryInfo StShareRewardingInfo { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			LMsgID = parser.ParseField<long>(message, "lMsgID");
			ETalkType = parser.ParseEnum<CHAT_TYPE>(message, "eTalkType");
			EChatMessageType = parser.ParseEnum<CHAT_MESSAGE_TYPE>(message, "eChatMessageType");
			BChatRoom = parser.ParseField<int>(message, "bChatRoom");
			StFromUser = parser.ParseMessage<UserBaseInfoNet>(message, "stFromUser");
			StToUser = parser.ParseMessage<UserBaseInfoNet>(message, "stToUser");
			StChatRoomBaseInfo = parser.ParseMessage<ChatRoomBaseInfo>(message, "stChatRoomBaseInfo");
			LGameRoomID = parser.ParseField<long>(message, "lGameRoomID");
			StCluBasebInfo = parser.ParseMessage<ClubBaseInfo>(message, "stCluBasebInfo");
			SMsg = parser.ParseField<string>(message, "sMsg");
			VVoiceDatas = parser.ParseByteArray(message, "vVoiceDatas");
			StShareBillInfo = parser.ParseMessage<ShareBillInfoNet>(message, "stShareBillInfo");
			StPokerProcessBaseInfo = parser.ParseMessage<PokerProcessInfo>(message, "stPokerProcessBaseInfo");
			StrPicUrl = parser.ParseField<string>(message, "strPicUrl");
			LCreateTime = parser.ParseField<long>(message, "lCreateTime");
			LDuration = parser.ParseField<long>(message, "lDuration");
			IMute = parser.ParseField<int>(message, "iMute");
			IVibrate = parser.ParseField<int>(message, "iVibrate");
			StNetPageShareInfoNet = parser.ParseMessage<NetPageShareInfoNet>(message, "stNetPageShareInfoNet");
			StSharePokerHistoryInfo = parser.ParseMessage<SharePokerHistoryInfo>(message, "stSharePokerHistoryInfo");
			VExtras = parser.ParseFieldRepeated<string>(message, "vExtras");
			StShareMTTBillInfoNet = parser.ParseMessage<ShareMTTBillInfoNet>(message, "stShareMTTBillInfoNet");
			StShareRewardingInfo = parser.ParseMessage<SharePokerHistoryInfo>(message, "stShareRewardingInfo");
		}
	}
}
