using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class CSUploadAPNToken : DomainModel
	{
		

		public long Uuid { get; set; }
		public DEVICE_TYPE EDeviceType { get; set; }
		public string StrIosAPNToken { get; set; }
		public string StrAndroidXingeAPNToken { get; set; }
		public string StrAndroidFCMAPNToken { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			Uuid = parser.ParseField<long>(message, "uuid");
			EDeviceType = parser.ParseEnum<DEVICE_TYPE>(message, "eDeviceType");
			StrIosAPNToken = parser.ParseField<string>(message, "strIosAPNToken");
			StrAndroidXingeAPNToken = parser.ParseField<string>(message, "strAndroidXingeAPNToken");
			StrAndroidFCMAPNToken = parser.ParseField<string>(message, "strAndroidFCMAPNToken");
		}
	}
}
