using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class SCShowTableCardsRsp : DomainModel
	{
		

		public ERROR_CODE_TYPE EErrCode { get; set; }
		public long LGameRoomID { get; set; }
		public long LGameHandID { get; set; }
		public int[] VTableCards { get; set; }
		public long LPopularity { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			EErrCode = parser.ParseEnum<ERROR_CODE_TYPE>(message, "eErrCode");
			LGameRoomID = parser.ParseField<long>(message, "lGameRoomID");
			LGameHandID = parser.ParseField<long>(message, "lGameHandID");
			VTableCards = parser.ParseFieldRepeated<int>(message, "vTableCards");
			LPopularity = parser.ParseField<long>(message, "lPopularity");
		}
	}
}
