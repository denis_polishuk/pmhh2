namespace PokerMaster.Domain.Model
{
	public enum USER_STATE
	{
		USER_STATE_OFFLINE = 1,
		USER_STATE_ONLINE = 2,
		USER_STATE_GAMING = 3,
		USER_STATE_ROOM = 4
	}
}
