using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class CSCreateChatRoom : DomainModel
	{
		

		public long Uuid { get; set; }
		public string SRoomName { get; set; }
		public int NMaxMembers { get; set; }
		public int NCurMembers { get; set; }
		public long LCreateUser { get; set; }
		public int IRoomType { get; set; }
		public int NOwnerInvite { get; set; }
		public int NOwnerCreate { get; set; }
		public UserBaseInfoNet[] VUserBaseInfos { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			Uuid = parser.ParseField<long>(message, "uuid");
			SRoomName = parser.ParseField<string>(message, "sRoomName");
			NMaxMembers = parser.ParseField<int>(message, "nMaxMembers");
			NCurMembers = parser.ParseField<int>(message, "nCurMembers");
			LCreateUser = parser.ParseField<long>(message, "lCreateUser");
			IRoomType = parser.ParseField<int>(message, "iRoomType");
			NOwnerInvite = parser.ParseField<int>(message, "nOwnerInvite");
			NOwnerCreate = parser.ParseField<int>(message, "nOwnerCreate");
			VUserBaseInfos = parser.ParseMessageRepeated<UserBaseInfoNet>(message, "vUserBaseInfos");
		}
	}
}
