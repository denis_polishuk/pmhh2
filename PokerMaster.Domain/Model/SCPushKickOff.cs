using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class SCPushKickOff : DomainModel
	{
		

		public long LTime { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			LTime = parser.ParseField<long>(message, "lTime");
		}
	}
}
