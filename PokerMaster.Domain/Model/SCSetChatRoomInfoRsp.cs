using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class SCSetChatRoomInfoRsp : DomainModel
	{
		

		public ERROR_CODE_TYPE EErrCode { get; set; }
		public ChatRoomBaseInfo StChatRoomBaseInfo { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			EErrCode = parser.ParseEnum<ERROR_CODE_TYPE>(message, "eErrCode");
			StChatRoomBaseInfo = parser.ParseMessage<ChatRoomBaseInfo>(message, "stChatRoomBaseInfo");
		}
	}
}
