using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class CSCreatorInsuranceSetting : DomainModel
	{
		

		public long Uuid { get; set; }
		public long LGameRoomID { get; set; }
		public long LSettedUuid { get; set; }
		public int BCanInsurance { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			Uuid = parser.ParseField<long>(message, "uuid");
			LGameRoomID = parser.ParseField<long>(message, "lGameRoomID");
			LSettedUuid = parser.ParseField<long>(message, "lSettedUuid");
			BCanInsurance = parser.ParseField<int>(message, "bCanInsurance");
		}
	}
}
