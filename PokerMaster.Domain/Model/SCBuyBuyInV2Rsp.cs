using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class SCBuyBuyInV2Rsp : DomainModel
	{
		

		public ERROR_CODE_TYPE EErrCode { get; set; }
		public GameRoomInfo StGameRoomInfo { get; set; }
		public UserBaseInfoNet StUserBaseInfoNet { get; set; }
		public int BWaitingBuyinConfirmation { get; set; }
		public long LBuyinTime { get; set; }
		public long LGameRoomID { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			EErrCode = parser.ParseEnum<ERROR_CODE_TYPE>(message, "eErrCode");
			StGameRoomInfo = parser.ParseMessage<GameRoomInfo>(message, "stGameRoomInfo");
			StUserBaseInfoNet = parser.ParseMessage<UserBaseInfoNet>(message, "stUserBaseInfoNet");
			BWaitingBuyinConfirmation = parser.ParseField<int>(message, "bWaitingBuyinConfirmation");
			LBuyinTime = parser.ParseField<long>(message, "lBuyinTime");
			LGameRoomID = parser.ParseField<long>(message, "lGameRoomID");
		}
	}
}
