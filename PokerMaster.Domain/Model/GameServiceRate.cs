using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class GameServiceRate : DomainModel
	{
		

		public long LSmallBlinds { get; set; }
		public int IFeeRate { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			LSmallBlinds = parser.ParseField<long>(message, "lSmallBlinds");
			IFeeRate = parser.ParseField<int>(message, "iFeeRate");
		}
	}
}
