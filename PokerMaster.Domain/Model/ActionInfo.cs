using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class ActionInfo : DomainModel
	{
		

		public string SLocation { get; set; }
		public string SAction { get; set; }
		public long LBet { get; set; }
		public long LChips { get; set; }
		public long LPot { get; set; }
		public int IHighlight { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			SLocation = parser.ParseField<string>(message, "sLocation");
			SAction = parser.ParseField<string>(message, "sAction");
			LBet = parser.ParseField<long>(message, "lBet");
			LChips = parser.ParseField<long>(message, "lChips");
			LPot = parser.ParseField<long>(message, "lPot");
			IHighlight = parser.ParseField<int>(message, "iHighlight");
		}
	}
}
