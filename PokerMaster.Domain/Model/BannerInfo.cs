using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class BannerInfo : DomainModel
	{
		

		public string SImageUrl { get; set; }
		public string STargetUrl { get; set; }
		public long LBannerID { get; set; }
		public string STitle { get; set; }
		public string SSummary { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			SImageUrl = parser.ParseField<string>(message, "sImageUrl");
			STargetUrl = parser.ParseField<string>(message, "sTargetUrl");
			LBannerID = parser.ParseField<long>(message, "lBannerID");
			STitle = parser.ParseField<string>(message, "sTitle");
			SSummary = parser.ParseField<string>(message, "sSummary");
		}
	}
}
