using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class CSUploadPhoneNumber : DomainModel
	{
		

		public UserBaseInfoNet StUserBaseInfo { get; set; }
		public string SPhoneNumber { get; set; }
		public string SCountryCode { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			StUserBaseInfo = parser.ParseMessage<UserBaseInfoNet>(message, "stUserBaseInfo");
			SPhoneNumber = parser.ParseField<string>(message, "sPhoneNumber");
			SCountryCode = parser.ParseField<string>(message, "sCountryCode");
		}
	}
}
