using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class CSGetMTTBillInfoByRoomID : DomainModel
	{
		

		public long Uuid { get; set; }
		public long LRoomID { get; set; }
		public int IOffset { get; set; }
		public int INum { get; set; }
		public int BCashed { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			Uuid = parser.ParseField<long>(message, "uuid");
			LRoomID = parser.ParseField<long>(message, "lRoomID");
			IOffset = parser.ParseField<int>(message, "iOffset");
			INum = parser.ParseField<int>(message, "iNum");
			BCashed = parser.ParseField<int>(message, "bCashed");
		}
	}
}
