using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class CSUseItem : DomainModel
	{
		

		public long Uuid { get; set; }
		public int LItemID { get; set; }
		public long Touuid { get; set; }
		public long LRoomID { get; set; }
		public long LMsgID { get; set; }
		public string SExtra { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			Uuid = parser.ParseField<long>(message, "uuid");
			LItemID = parser.ParseField<int>(message, "lItemID");
			Touuid = parser.ParseField<long>(message, "touuid");
			LRoomID = parser.ParseField<long>(message, "lRoomID");
			LMsgID = parser.ParseField<long>(message, "lMsgID");
			SExtra = parser.ParseField<string>(message, "sExtra");
		}
	}
}
