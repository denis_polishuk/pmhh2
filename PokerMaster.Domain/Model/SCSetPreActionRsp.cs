using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class SCSetPreActionRsp : DomainModel
	{
		

		public ERROR_CODE_TYPE EErrCode { get; set; }
		public long LGameRoomID { get; set; }
		public long LGameHandID { get; set; }
		public USER_GAME_STATE EPreUserGameState { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			EErrCode = parser.ParseEnum<ERROR_CODE_TYPE>(message, "eErrCode");
			LGameRoomID = parser.ParseField<long>(message, "lGameRoomID");
			LGameHandID = parser.ParseField<long>(message, "lGameHandID");
			EPreUserGameState = parser.ParseEnum<USER_GAME_STATE>(message, "ePreUserGameState");
		}
	}
}
