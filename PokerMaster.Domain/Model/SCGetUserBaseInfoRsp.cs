using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class SCGetUserBaseInfoRsp : DomainModel
	{
		

		public ERROR_CODE_TYPE EErrCode { get; set; }
		public UserBaseInfoNet StUserBaseInfoNet { get; set; }
		public int BRemark { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			EErrCode = parser.ParseEnum<ERROR_CODE_TYPE>(message, "eErrCode");
			StUserBaseInfoNet = parser.ParseMessage<UserBaseInfoNet>(message, "stUserBaseInfoNet");
			BRemark = parser.ParseField<int>(message, "bRemark");
		}
	}
}
