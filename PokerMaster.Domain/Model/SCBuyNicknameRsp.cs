using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class SCBuyNicknameRsp : DomainModel
	{
		

		public ERROR_CODE_TYPE EErrCode { get; set; }
		public string SNickname { get; set; }
		public long LCoin { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			EErrCode = parser.ParseEnum<ERROR_CODE_TYPE>(message, "eErrCode");
			SNickname = parser.ParseField<string>(message, "sNickname");
			LCoin = parser.ParseField<long>(message, "lCoin");
		}
	}
}
