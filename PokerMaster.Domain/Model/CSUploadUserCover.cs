using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class CSUploadUserCover : DomainModel
	{
		

		public long Uuid { get; set; }
		public byte[] VIconDatas { get; set; }
		public int IPos { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			Uuid = parser.ParseField<long>(message, "uuid");
			VIconDatas = parser.ParseByteArray(message, "vIconDatas");
			IPos = parser.ParseField<int>(message, "iPos");
		}
	}
}
