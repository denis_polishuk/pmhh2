using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class LeagueGameFee : DomainModel
	{
		

		public long LSmallBlinds { get; set; }
		public int IFee { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			LSmallBlinds = parser.ParseField<long>(message, "lSmallBlinds");
			IFee = parser.ParseField<int>(message, "iFee");
		}
	}
}
