namespace PokerMaster.Domain.Model
{
	public enum CHAT_MESSAGE_TYPE
	{
		CHAT_MESSAGE_TYPE_TEXT = 1,
		CHAT_MESSAGE_TYPE_VOICE = 2,
		CHAT_MESSAGE_TYPE_BILL = 3,
		CHAT_MESSAGE_TYPE_POKER_PROCESS = 4,
		CHAT_MESSAGE_TYPE_GAME_CREATE = 5,
		CHAT_MESSAGE_TYPE_PIC = 6,
		CHAT_MESSAGE_TYPE_ENTER_GAME = 7,
		CHAT_MESSAGE_TYPE_OTHER = 8,
		CHAT_MESSAGE_TYPE_WEBPAGE = 9,
		CHAT_MESSAGE_TYPE_SYSTEM = 10,
		CHAT_MESSAGE_TYPE_GAME_SYSTEM = 11,
		CHAT_MESSAGE_TYPE_POKER_HISTORY = 12,
		CHAT_MESSAGE_TYPE_MTT_BILL = 13,
		CHAT_MESSAGE_TYPE_SHARE_REWARDING = 14,
		CHAT_MESSAGE_TYPE_OFFER_REWARDING = 15
	}
}
