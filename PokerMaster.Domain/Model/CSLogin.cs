using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class CSLogin : DomainModel
	{
		

		public string StrID { get; set; }
		public string StrPWD { get; set; }
		public string StrIosToken { get; set; }
		public string StrSSOToken { get; set; }
		public string StrSysVersion { get; set; }
		public string StrPhoneType { get; set; }
		public string StrScreenSize { get; set; }
		public DEVICE_TYPE EDeviceType { get; set; }
		public LANGUAGE_TYPE ELanguageType { get; set; }
		public string SCountryCode { get; set; }
		public string SExtra { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			StrID = parser.ParseField<string>(message, "strID");
			StrPWD = parser.ParseField<string>(message, "strPWD");
			StrIosToken = parser.ParseField<string>(message, "strIosToken");
			StrSSOToken = parser.ParseField<string>(message, "strSSOToken");
			StrSysVersion = parser.ParseField<string>(message, "strSysVersion");
			StrPhoneType = parser.ParseField<string>(message, "strPhoneType");
			StrScreenSize = parser.ParseField<string>(message, "strScreenSize");
			EDeviceType = parser.ParseEnum<DEVICE_TYPE>(message, "eDeviceType");
			ELanguageType = parser.ParseEnum<LANGUAGE_TYPE>(message, "eLanguageType");
			SCountryCode = parser.ParseField<string>(message, "sCountryCode");
			SExtra = parser.ParseField<string>(message, "sExtra");
		}
	}
}
