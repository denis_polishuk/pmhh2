using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class GameRoomInfo : DomainModel
	{
		

		public GAME_ROOM_TYPE EGameRoomType { get; set; }
		public GameRoomBaseInfo StGameRoomBaseInfo { get; set; }
		public SNGGameRoomBaseInfo StSNGGameRoomBaseInfo { get; set; }
		public GAME_ROOM_GAME_STATE EGameState { get; set; }
		public int[] VCurrentCards { get; set; }
		public long[] VCurrentPots { get; set; }
		public UserGameInfoNet[] VUserGameInfos { get; set; }
		public GameResultInfo StGameResultInfo { get; set; }
		public int BStateChange { get; set; }
		public int BAllAllin { get; set; }
		public long LGameHandID { get; set; }
		public long LEffectiveRaise { get; set; }
		public INSURANCE_STATUS EInsuranceStatus { get; set; }
		public InsuranceInfo VInsuranceInfos { get; set; }
		public PotInfo[] VPotInfos { get; set; }
		public long[] VKickUserIDs { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			EGameRoomType = parser.ParseEnum<GAME_ROOM_TYPE>(message, "eGameRoomType");
			StGameRoomBaseInfo = parser.ParseMessage<GameRoomBaseInfo>(message, "stGameRoomBaseInfo");
			StSNGGameRoomBaseInfo = parser.ParseMessage<SNGGameRoomBaseInfo>(message, "stSNGGameRoomBaseInfo");
			EGameState = parser.ParseEnum<GAME_ROOM_GAME_STATE>(message, "eGameState");
			VCurrentCards = parser.ParseFieldRepeated<int>(message, "vCurrentCards");
			VCurrentPots = parser.ParseFieldRepeated<long>(message, "vCurrentPots");
			VUserGameInfos = parser.ParseMessageRepeated<UserGameInfoNet>(message, "vUserGameInfos");
			StGameResultInfo = parser.ParseMessage<GameResultInfo>(message, "stGameResultInfo");
			BStateChange = parser.ParseField<int>(message, "bStateChange");
			BAllAllin = parser.ParseField<int>(message, "bAllAllin");
			LGameHandID = parser.ParseField<long>(message, "lGameHandID");
			LEffectiveRaise = parser.ParseField<long>(message, "lEffectiveRaise");
			EInsuranceStatus = parser.ParseEnum<INSURANCE_STATUS>(message, "eInsuranceStatus");
			VInsuranceInfos = parser.ParseMessage<InsuranceInfo>(message, "vInsuranceInfos");
			VPotInfos = parser.ParseMessageRepeated<PotInfo>(message, "vPotInfos");
			VKickUserIDs = parser.ParseFieldRepeated<long>(message, "vKickUserIDs");
		}
	}
}
