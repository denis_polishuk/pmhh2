using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class SCKickUserGameRoomRsp : DomainModel
	{
		

		public ERROR_CODE_TYPE EErrCode { get; set; }
		public long LRoomID { get; set; }
		public long LKickOffUuid { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			EErrCode = parser.ParseEnum<ERROR_CODE_TYPE>(message, "eErrCode");
			LRoomID = parser.ParseField<long>(message, "lRoomID");
			LKickOffUuid = parser.ParseField<long>(message, "lKickOffUuid");
		}
	}
}
