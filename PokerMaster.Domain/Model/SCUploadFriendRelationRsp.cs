using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class SCUploadFriendRelationRsp : DomainModel
	{
		

		public ERROR_CODE_TYPE EErrCode { get; set; }
		public UPLOAD_FRIEND_RELATION_TYPE EUploadType { get; set; }
		public UserFriendInfoNet[] VFriendsInfo { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			EErrCode = parser.ParseEnum<ERROR_CODE_TYPE>(message, "eErrCode");
			EUploadType = parser.ParseEnum<UPLOAD_FRIEND_RELATION_TYPE>(message, "eUploadType");
			VFriendsInfo = parser.ParseMessageRepeated<UserFriendInfoNet>(message, "vFriendsInfo");
		}
	}
}
