using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class ShowPlayer : DomainModel
	{
		

		public int[] VHoldCards { get; set; }
		public string SLocation { get; set; }
		public string SRank { get; set; }
		public long LWin { get; set; }
		public long LInsresult { get; set; }
		public int IHighlight { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			VHoldCards = parser.ParseFieldRepeated<int>(message, "vHoldCards");
			SLocation = parser.ParseField<string>(message, "sLocation");
			SRank = parser.ParseField<string>(message, "sRank");
			LWin = parser.ParseField<long>(message, "lWin");
			LInsresult = parser.ParseField<long>(message, "lInsresult");
			IHighlight = parser.ParseField<int>(message, "iHighlight");
		}
	}
}
