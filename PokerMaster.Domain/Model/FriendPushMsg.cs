using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class FriendPushMsg : DomainModel
	{
		

		public long LMsgID { get; set; }
		public FRIEND_MESSAGE_TYPE EFriendMessageType { get; set; }
		public UserBaseInfoNet StFromUser { get; set; }
		public UserBaseInfoNet StToUser { get; set; }
		public string SMsg { get; set; }
		public long LCreateTime { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			LMsgID = parser.ParseField<long>(message, "lMsgID");
			EFriendMessageType = parser.ParseEnum<FRIEND_MESSAGE_TYPE>(message, "eFriendMessageType");
			StFromUser = parser.ParseMessage<UserBaseInfoNet>(message, "stFromUser");
			StToUser = parser.ParseMessage<UserBaseInfoNet>(message, "stToUser");
			SMsg = parser.ParseField<string>(message, "sMsg");
			LCreateTime = parser.ParseField<long>(message, "lCreateTime");
		}
	}
}
