using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class QuickGameRoomPushMsg : DomainModel
	{
		

		public long LMsgID { get; set; }
		public QUICK_GAME_ROOM_MESSAGE_TYPE EMessageType { get; set; }
		public string SMsg { get; set; }
		public long LCreateTime { get; set; }
		public long LChatRoomID { get; set; }
		public long LGameRoomID { get; set; }
		public UserBaseInfoNet StFromUser { get; set; }
		public UserBaseInfoNet[] VUsers { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			LMsgID = parser.ParseField<long>(message, "lMsgID");
			EMessageType = parser.ParseEnum<QUICK_GAME_ROOM_MESSAGE_TYPE>(message, "eMessageType");
			SMsg = parser.ParseField<string>(message, "sMsg");
			LCreateTime = parser.ParseField<long>(message, "lCreateTime");
			LChatRoomID = parser.ParseField<long>(message, "lChatRoomID");
			LGameRoomID = parser.ParseField<long>(message, "lGameRoomID");
			StFromUser = parser.ParseMessage<UserBaseInfoNet>(message, "stFromUser");
			VUsers = parser.ParseMessageRepeated<UserBaseInfoNet>(message, "vUsers");
		}
	}
}
