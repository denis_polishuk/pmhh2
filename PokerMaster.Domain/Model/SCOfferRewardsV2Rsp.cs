using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class SCOfferRewardsV2Rsp : DomainModel
	{
		

		public ERROR_CODE_TYPE EErrCode { get; set; }
		public long LTalkwith { get; set; }
		public CHAT_TYPE ETalkType { get; set; }
		public long LMsgId { get; set; }
		public long LBalanceAfterReward { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			EErrCode = parser.ParseEnum<ERROR_CODE_TYPE>(message, "eErrCode");
			LTalkwith = parser.ParseField<long>(message, "lTalkwith");
			ETalkType = parser.ParseEnum<CHAT_TYPE>(message, "eTalkType");
			LMsgId = parser.ParseField<long>(message, "lMsgId");
			LBalanceAfterReward = parser.ParseField<long>(message, "lBalanceAfterReward");
		}
	}
}
