using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class SCGetGameRoomInfoRsp : DomainModel
	{
		

		public ERROR_CODE_TYPE EErrCode { get; set; }
		public GameRoomInfo[] StGameRoomInfo { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			EErrCode = parser.ParseEnum<ERROR_CODE_TYPE>(message, "eErrCode");
			StGameRoomInfo = parser.ParseMessageRepeated<GameRoomInfo>(message, "stGameRoomInfo");
		}
	}
}
