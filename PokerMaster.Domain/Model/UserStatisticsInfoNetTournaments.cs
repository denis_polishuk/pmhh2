using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class UserStatisticsInfoNetTournaments : DomainModel
	{
		

		public GAME_ROOM_TYPE EGameRoomType { get; set; }
		public long LTournamentsCount { get; set; }
		public long LTopsCount { get; set; }
		public long LSecondsCount { get; set; }
		public long LThirdsCount { get; set; }
		public long LTopThreesCount { get; set; }
		public long LProfitedCount { get; set; }
		public long LHandsCount { get; set; }
		public long LDealtHands { get; set; }
		public long LPreflopRaises { get; set; }
		public long LAllinsCount { get; set; }
		public long LWonAllins { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			EGameRoomType = parser.ParseEnum<GAME_ROOM_TYPE>(message, "eGameRoomType");
			LTournamentsCount = parser.ParseField<long>(message, "lTournamentsCount");
			LTopsCount = parser.ParseField<long>(message, "lTopsCount");
			LSecondsCount = parser.ParseField<long>(message, "lSecondsCount");
			LThirdsCount = parser.ParseField<long>(message, "lThirdsCount");
			LTopThreesCount = parser.ParseField<long>(message, "lTopThreesCount");
			LProfitedCount = parser.ParseField<long>(message, "lProfitedCount");
			LHandsCount = parser.ParseField<long>(message, "lHandsCount");
			LDealtHands = parser.ParseField<long>(message, "lDealtHands");
			LPreflopRaises = parser.ParseField<long>(message, "lPreflopRaises");
			LAllinsCount = parser.ParseField<long>(message, "lAllinsCount");
			LWonAllins = parser.ParseField<long>(message, "lWonAllins");
		}
	}
}
