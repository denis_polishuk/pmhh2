using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class CSOfferRewardsV2 : DomainModel
	{
		

		public long Uuid { get; set; }
		public long LTalkwith { get; set; }
		public CHAT_TYPE ETalkType { get; set; }
		public long LMsgId { get; set; }
		public long LRewardUnit { get; set; }
		public string SFavoriteName { get; set; }
		public long LRoomID { get; set; }
		public long LHandID { get; set; }
		public long LSharerUuid { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			Uuid = parser.ParseField<long>(message, "uuid");
			LTalkwith = parser.ParseField<long>(message, "lTalkwith");
			ETalkType = parser.ParseEnum<CHAT_TYPE>(message, "eTalkType");
			LMsgId = parser.ParseField<long>(message, "lMsgId");
			LRewardUnit = parser.ParseField<long>(message, "lRewardUnit");
			SFavoriteName = parser.ParseField<string>(message, "sFavoriteName");
			LRoomID = parser.ParseField<long>(message, "lRoomID");
			LHandID = parser.ParseField<long>(message, "lHandID");
			LSharerUuid = parser.ParseField<long>(message, "lSharerUuid");
		}
	}
}
