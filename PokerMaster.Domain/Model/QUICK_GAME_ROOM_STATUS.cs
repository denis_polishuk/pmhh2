namespace PokerMaster.Domain.Model
{
	public enum QUICK_GAME_ROOM_STATUS
	{
		QUICK_GAME_ROOM_NOTSTART = 1,
		QUICK_GAME_ROOM_START = 2,
		QUICK_GAME_ROOM_FINISH = 3
	}
}
