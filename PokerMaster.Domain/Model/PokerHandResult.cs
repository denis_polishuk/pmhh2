using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class PokerHandResult : DomainModel
	{
		

		public long LRoomID { get; set; }
		public long LHandID { get; set; }
		public int[] VCommunityCards { get; set; }
		public PokerResultUserInfo[] VPokerResultUserInfovs { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			LRoomID = parser.ParseField<long>(message, "lRoomID");
			LHandID = parser.ParseField<long>(message, "lHandID");
			VCommunityCards = parser.ParseFieldRepeated<int>(message, "vCommunityCards");
			VPokerResultUserInfovs = parser.ParseMessageRepeated<PokerResultUserInfo>(message, "vPokerResultUserInfovs");
		}
	}
}
