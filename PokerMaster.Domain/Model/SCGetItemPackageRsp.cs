using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class SCGetItemPackageRsp : DomainModel
	{
		

		public ERROR_CODE_TYPE EErrCode { get; set; }
		public ItemPackage StItemPackage { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			EErrCode = parser.ParseEnum<ERROR_CODE_TYPE>(message, "eErrCode");
			StItemPackage = parser.ParseMessage<ItemPackage>(message, "stItemPackage");
		}
	}
}
