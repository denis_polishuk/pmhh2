using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class SCLeaveGameRoomRsp : DomainModel
	{
		

		public ERROR_CODE_TYPE EErrCode { get; set; }
		public long LRoomID { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			EErrCode = parser.ParseEnum<ERROR_CODE_TYPE>(message, "eErrCode");
			LRoomID = parser.ParseField<long>(message, "lRoomID");
		}
	}
}
