using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class RecommendUserInfo : DomainModel
	{
		

		public UserBaseInfoNet StUserBaseInfoNet { get; set; }
		public RECOMMEND_USER_TYPE ERecommendUserType { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			StUserBaseInfoNet = parser.ParseMessage<UserBaseInfoNet>(message, "stUserBaseInfoNet");
			ERecommendUserType = parser.ParseEnum<RECOMMEND_USER_TYPE>(message, "eRecommendUserType");
		}
	}
}
