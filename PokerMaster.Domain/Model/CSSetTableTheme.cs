using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class CSSetTableTheme : DomainModel
	{
		

		public long Uuid { get; set; }
		public int IOrdinal { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			Uuid = parser.ParseField<long>(message, "uuid");
			IOrdinal = parser.ParseField<int>(message, "iOrdinal");
		}
	}
}
