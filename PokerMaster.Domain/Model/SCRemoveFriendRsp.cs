using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class SCRemoveFriendRsp : DomainModel
	{
		

		public ERROR_CODE_TYPE EErrCode { get; set; }
		public long FriendUuid { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			EErrCode = parser.ParseEnum<ERROR_CODE_TYPE>(message, "eErrCode");
			FriendUuid = parser.ParseField<long>(message, "friendUuid");
		}
	}
}
