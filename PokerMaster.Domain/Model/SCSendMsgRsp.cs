using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class SCSendMsgRsp : DomainModel
	{
		

		public ERROR_CODE_TYPE EErrCode { get; set; }
		public long LTalkwith { get; set; }
		public CHAT_TYPE ETalkType { get; set; }
		public long LMsgId { get; set; }
		public long LPicMsgId { get; set; }
		public int LCurrentSeq { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			EErrCode = parser.ParseEnum<ERROR_CODE_TYPE>(message, "eErrCode");
			LTalkwith = parser.ParseField<long>(message, "lTalkwith");
			ETalkType = parser.ParseEnum<CHAT_TYPE>(message, "eTalkType");
			LMsgId = parser.ParseField<long>(message, "lMsgId");
			LPicMsgId = parser.ParseField<long>(message, "lPicMsgId");
			LCurrentSeq = parser.ParseField<int>(message, "lCurrentSeq");
		}
	}
}
