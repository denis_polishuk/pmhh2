using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class SCUseItemRsp : DomainModel
	{
		

		public ERROR_CODE_TYPE EErrCode { get; set; }
		public int LItemID { get; set; }
		public long Touuid { get; set; }
		public long LRoomID { get; set; }
		public long LPopularity { get; set; }
		public long LCoin { get; set; }
		public long LMsgID { get; set; }
		public string SExtra { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			EErrCode = parser.ParseEnum<ERROR_CODE_TYPE>(message, "eErrCode");
			LItemID = parser.ParseField<int>(message, "lItemID");
			Touuid = parser.ParseField<long>(message, "touuid");
			LRoomID = parser.ParseField<long>(message, "lRoomID");
			LPopularity = parser.ParseField<long>(message, "lPopularity");
			LCoin = parser.ParseField<long>(message, "lCoin");
			LMsgID = parser.ParseField<long>(message, "lMsgID");
			SExtra = parser.ParseField<string>(message, "sExtra");
		}
	}
}
