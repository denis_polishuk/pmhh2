using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class RaiseShortcutsOptions : DomainModel
	{
		

		public int BFixedRatio { get; set; }
		public int IOptionIndex { get; set; }
		public int IRatioUp { get; set; }
		public int IRatioDown { get; set; }
		public long ICustomized { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			BFixedRatio = parser.ParseField<int>(message, "bFixedRatio");
			IOptionIndex = parser.ParseField<int>(message, "iOptionIndex");
			IRatioUp = parser.ParseField<int>(message, "iRatioUp");
			IRatioDown = parser.ParseField<int>(message, "iRatioDown");
			ICustomized = parser.ParseField<long>(message, "iCustomized");
		}
	}
}
