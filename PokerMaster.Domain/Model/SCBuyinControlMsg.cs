using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class SCBuyinControlMsg : DomainModel
	{
		

		public BuyinControlMsg StBuyinControlMsg { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			StBuyinControlMsg = parser.ParseMessage<BuyinControlMsg>(message, "stBuyinControlMsg");
		}
	}
}
