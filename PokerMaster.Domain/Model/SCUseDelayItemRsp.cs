using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class SCUseDelayItemRsp : DomainModel
	{
		

		public ERROR_CODE_TYPE EErrCode { get; set; }
		public UserBaseInfoNet StUserBaseInfoNet { get; set; }
		public long LBettingID { get; set; }
		public long LGameRoomID { get; set; }
		public int IDelayTotalTimes { get; set; }
		public long LActTime { get; set; }
		public long IDelayLong { get; set; }
		public int IDelayCost { get; set; }
		public int IDelaySingleTime { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			EErrCode = parser.ParseEnum<ERROR_CODE_TYPE>(message, "eErrCode");
			StUserBaseInfoNet = parser.ParseMessage<UserBaseInfoNet>(message, "stUserBaseInfoNet");
			LBettingID = parser.ParseField<long>(message, "lBettingID");
			LGameRoomID = parser.ParseField<long>(message, "lGameRoomID");
			IDelayTotalTimes = parser.ParseField<int>(message, "iDelayTotalTimes");
			LActTime = parser.ParseField<long>(message, "lActTime");
			IDelayLong = parser.ParseField<long>(message, "iDelayLong");
			IDelayCost = parser.ParseField<int>(message, "iDelayCost");
			IDelaySingleTime = parser.ParseField<int>(message, "iDelaySingleTime");
		}
	}
}
