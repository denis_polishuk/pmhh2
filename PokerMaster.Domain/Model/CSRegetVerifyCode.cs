using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class CSRegetVerifyCode : DomainModel
	{
		

		public string SPhoneNumber { get; set; }
		public string SCountryCode { get; set; }
		public LANGUAGE_TYPE ELanguageType { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			SPhoneNumber = parser.ParseField<string>(message, "sPhoneNumber");
			SCountryCode = parser.ParseField<string>(message, "sCountryCode");
			ELanguageType = parser.ParseEnum<LANGUAGE_TYPE>(message, "eLanguageType");
		}
	}
}
