using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class UseItemPushMsg : DomainModel
	{
		

		public long LMsgID { get; set; }
		public long LFromUuid { get; set; }
		public long LToUuid { get; set; }
		public long LRoomID { get; set; }
		public int LItemID { get; set; }
		public string SExtra { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			LMsgID = parser.ParseField<long>(message, "lMsgID");
			LFromUuid = parser.ParseField<long>(message, "lFromUuid");
			LToUuid = parser.ParseField<long>(message, "lToUuid");
			LRoomID = parser.ParseField<long>(message, "lRoomID");
			LItemID = parser.ParseField<int>(message, "lItemID");
			SExtra = parser.ParseField<string>(message, "sExtra");
		}
	}
}
