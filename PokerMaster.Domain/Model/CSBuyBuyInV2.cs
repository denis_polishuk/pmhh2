using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class CSBuyBuyInV2 : DomainModel
	{
		

		public long Uuid { get; set; }
		public long LRoomID { get; set; }
		public long LStacks { get; set; }
		public long LClubID { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			Uuid = parser.ParseField<long>(message, "uuid");
			LRoomID = parser.ParseField<long>(message, "lRoomID");
			LStacks = parser.ParseField<long>(message, "lStacks");
			LClubID = parser.ParseField<long>(message, "lClubID");
		}
	}
}
