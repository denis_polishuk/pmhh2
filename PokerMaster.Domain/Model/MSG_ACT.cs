namespace PokerMaster.Domain.Model
{
	public enum MSG_ACT
	{
		ACT_ACCEPT_INVITE = 1,
		ACT_DENY_INVITE = 2,
		ACT_LAUNCH_INVITE = 3
	}
}
