namespace PokerMaster.Domain.Model
{
	public enum MTT_SIGNUP_FEE_TYPE
	{
		MTT_SIGNUP_FEE_POPULARITY = 1,
		MTT_SIGNUP_FEE_TICKET = 2
	}
}
