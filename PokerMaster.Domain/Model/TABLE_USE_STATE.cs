namespace PokerMaster.Domain.Model
{
	public enum TABLE_USE_STATE
	{
		TABLE_STATE_Play = 1,
		TABLE_STATE_Ready = 2,
		TABLE_STATE_OVer = 3,
		TABLE_STATE_Empty = 4
	}
}
