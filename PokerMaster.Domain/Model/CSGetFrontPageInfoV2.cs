using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class CSGetFrontPageInfoV2 : DomainModel
	{
		

		public long Uuid { get; set; }
		public int IOffset { get; set; }
		public int INum { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			Uuid = parser.ParseField<long>(message, "uuid");
			IOffset = parser.ParseField<int>(message, "iOffset");
			INum = parser.ParseField<int>(message, "iNum");
		}
	}
}
