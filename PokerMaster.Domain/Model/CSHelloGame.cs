using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class CSHelloGame : DomainModel
	{
		

		public long Uuid { get; set; }
		public long LGameRoomID { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			Uuid = parser.ParseField<long>(message, "uuid");
			LGameRoomID = parser.ParseField<long>(message, "lGameRoomID");
		}
	}
}
