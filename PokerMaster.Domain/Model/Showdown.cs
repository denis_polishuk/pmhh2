using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class Showdown : DomainModel
	{
		

		public int IPlayerNum { get; set; }
		public long LPot { get; set; }
		public int[] VShowdownPubCards { get; set; }
		public ShowPlayer[] VShowPlayers { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			IPlayerNum = parser.ParseField<int>(message, "iPlayerNum");
			LPot = parser.ParseField<long>(message, "lPot");
			VShowdownPubCards = parser.ParseFieldRepeated<int>(message, "vShowdownPubCards");
			VShowPlayers = parser.ParseMessageRepeated<ShowPlayer>(message, "vShowPlayers");
		}
	}
}
