using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class SCResetPwdVerifyCodeRsp : DomainModel
	{
		

		public ERROR_CODE_TYPE EErrCode { get; set; }
		public string SNewPassword { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			EErrCode = parser.ParseEnum<ERROR_CODE_TYPE>(message, "eErrCode");
			SNewPassword = parser.ParseField<string>(message, "sNewPassword");
		}
	}
}
