using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class CSHello : DomainModel
	{
		

		public long Uuid { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			Uuid = parser.ParseField<long>(message, "uuid");
		}
	}
}
