using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class SCReconnectRsp : DomainModel
	{
		

		public ERROR_CODE_TYPE EErrCode { get; set; }
		public long LCurrentSystemTime { get; set; }
		public int IUploadLog { get; set; }
		public string SAccessToken { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			EErrCode = parser.ParseEnum<ERROR_CODE_TYPE>(message, "eErrCode");
			LCurrentSystemTime = parser.ParseField<long>(message, "lCurrentSystemTime");
			IUploadLog = parser.ParseField<int>(message, "iUploadLog");
			SAccessToken = parser.ParseField<string>(message, "sAccessToken");
		}
	}
}
