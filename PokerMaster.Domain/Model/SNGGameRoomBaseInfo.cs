using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class SNGGameRoomBaseInfo : DomainModel
	{
		

		public long LGameRoomId { get; set; }
		public string StrRoomName { get; set; }
		public UserBaseInfoNet StCreateUserBaseInfo { get; set; }
		public int BPrivateRoom { get; set; }
		public long LSmallBlinds { get; set; }
		public long LBigBlinds { get; set; }
		public int ICurrentLevel { get; set; }
		public int IMaxiLevel { get; set; }
		public int BLevelChanged { get; set; }
		public long[] VWinLadder { get; set; }
		public long LOriginalStacks { get; set; }
		public int IBlindInterval { get; set; }
		public CREATE_ROOM_TYPE ECreateRoomType { get; set; }
		public long LFromRoomID { get; set; }
		public int IGameRoomUserMaxNums { get; set; }
		public string SFromMsg { get; set; }
		public int BStarted { get; set; }
		public long LCreateTime { get; set; }
		public long LStartTime { get; set; }
		public SNG_ROOM_TYPE ESNGRoomtype { get; set; }
		public int IGameRoomUserNums { get; set; }
		public int BInGame { get; set; }
		public int BSignupInviting { get; set; }
		public int BBuyinControl { get; set; }
		public int IAnte { get; set; }
		public long LPrepareTime { get; set; }
		public int LCancelWaitInterval { get; set; }
		public int BIPLimit { get; set; }
		public int BGPSLimit { get; set; }
		public long LLeagueID { get; set; }
		public string SLeagueName { get; set; }
		public int IThinkingInterval { get; set; }
		public int BBuyin { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			LGameRoomId = parser.ParseField<long>(message, "lGameRoomId");
			StrRoomName = parser.ParseField<string>(message, "strRoomName");
			StCreateUserBaseInfo = parser.ParseMessage<UserBaseInfoNet>(message, "stCreateUserBaseInfo");
			BPrivateRoom = parser.ParseField<int>(message, "bPrivateRoom");
			LSmallBlinds = parser.ParseField<long>(message, "lSmallBlinds");
			LBigBlinds = parser.ParseField<long>(message, "lBigBlinds");
			ICurrentLevel = parser.ParseField<int>(message, "iCurrentLevel");
			IMaxiLevel = parser.ParseField<int>(message, "iMaxiLevel");
			BLevelChanged = parser.ParseField<int>(message, "bLevelChanged");
			VWinLadder = parser.ParseFieldRepeated<long>(message, "vWinLadder");
			LOriginalStacks = parser.ParseField<long>(message, "lOriginalStacks");
			IBlindInterval = parser.ParseField<int>(message, "iBlindInterval");
			ECreateRoomType = parser.ParseEnum<CREATE_ROOM_TYPE>(message, "eCreateRoomType");
			LFromRoomID = parser.ParseField<long>(message, "lFromRoomID");
			IGameRoomUserMaxNums = parser.ParseField<int>(message, "iGameRoomUserMaxNums");
			SFromMsg = parser.ParseField<string>(message, "sFromMsg");
			BStarted = parser.ParseField<int>(message, "bStarted");
			LCreateTime = parser.ParseField<long>(message, "lCreateTime");
			LStartTime = parser.ParseField<long>(message, "lStartTime");
			ESNGRoomtype = parser.ParseEnum<SNG_ROOM_TYPE>(message, "eSNGRoomtype");
			IGameRoomUserNums = parser.ParseField<int>(message, "iGameRoomUserNums");
			BInGame = parser.ParseField<int>(message, "bInGame");
			BSignupInviting = parser.ParseField<int>(message, "bSignupInviting");
			BBuyinControl = parser.ParseField<int>(message, "bBuyinControl");
			IAnte = parser.ParseField<int>(message, "iAnte");
			LPrepareTime = parser.ParseField<long>(message, "lPrepareTime");
			LCancelWaitInterval = parser.ParseField<int>(message, "lCancelWaitInterval");
			BIPLimit = parser.ParseField<int>(message, "bIPLimit");
			BGPSLimit = parser.ParseField<int>(message, "bGPSLimit");
			LLeagueID = parser.ParseField<long>(message, "lLeagueID");
			SLeagueName = parser.ParseField<string>(message, "sLeagueName");
			IThinkingInterval = parser.ParseField<int>(message, "iThinkingInterval");
			BBuyin = parser.ParseField<int>(message, "bBuyin");
		}
	}
}
