using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class OddsStructure : DomainModel
	{
		

		public int IOuts { get; set; }
		public int IOdds { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			IOuts = parser.ParseField<int>(message, "iOuts");
			IOdds = parser.ParseField<int>(message, "iOdds");
		}
	}
}
