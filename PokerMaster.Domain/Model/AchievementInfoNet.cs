using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
    public class AchievementInfoNet : DomainModel
    {


        public int IAchievementID { get; set; }
        public string SAchievementName { get; set; }
        public string SAchievementIcon { get; set; }
        public string SAchievementDescription { get; set; }

        public override void Parse(DynamicMessage message, ProtoParser parser)
        {
            IAchievementID = parser.ParseField<int>(message, "iAchievementID");
            SAchievementName = parser.ParseField<string>(message, "sAchievementName");
            SAchievementIcon = parser.ParseField<string>(message, "sAchievementIcon");
            SAchievementDescription = parser.ParseField<string>(message, "sAchievementDescription");
        }
    }
}
