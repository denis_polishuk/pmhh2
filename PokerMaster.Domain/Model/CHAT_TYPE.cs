namespace PokerMaster.Domain.Model
{
	public enum CHAT_TYPE
	{
		CHAT_TYPE_SINGLE = 1,
		CHAT_TYPE_GROUP = 2,
		CHAT_TYPE_CLUB = 3
	}
}
