using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class CmdResult : DomainModel
	{
		

		public int ICmdId { get; set; }
		public ERROR_CODE_TYPE EErrCode { get; set; }
		public string StrErrDesc { get; set; }
		public int ISubErrCode { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			ICmdId = parser.ParseField<int>(message, "iCmdId");
			EErrCode = parser.ParseEnum<ERROR_CODE_TYPE>(message, "eErrCode");
			StrErrDesc = parser.ParseField<string>(message, "strErrDesc");
			ISubErrCode = parser.ParseField<int>(message, "iSubErrCode");
		}
	}
}
