using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class SCUseItemPushMsg : DomainModel
	{
		

		public UseItemPushMsg[] VMsgs { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			VMsgs = parser.ParseMessageRepeated<UseItemPushMsg>(message, "vMsgs");
		}
	}
}
