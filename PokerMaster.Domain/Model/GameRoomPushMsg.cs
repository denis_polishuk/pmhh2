using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class GameRoomPushMsg : DomainModel
	{
		

		public long LMsgID { get; set; }
		public GAME_ROOM_MESSAGE_TYPE EGameMessageType { get; set; }
		public UserGameInfoNet StFromUser { get; set; }
		public string SMsg { get; set; }
		public long LCreateTime { get; set; }
		public GameRoomInfo StGameRoomInfo { get; set; }
		public int IPos { get; set; }
		public int IThinkingInterval { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			LMsgID = parser.ParseField<long>(message, "lMsgID");
			EGameMessageType = parser.ParseEnum<GAME_ROOM_MESSAGE_TYPE>(message, "eGameMessageType");
			StFromUser = parser.ParseMessage<UserGameInfoNet>(message, "stFromUser");
			SMsg = parser.ParseField<string>(message, "sMsg");
			LCreateTime = parser.ParseField<long>(message, "lCreateTime");
			StGameRoomInfo = parser.ParseMessage<GameRoomInfo>(message, "stGameRoomInfo");
			IPos = parser.ParseField<int>(message, "iPos");
			IThinkingInterval = parser.ParseField<int>(message, "iThinkingInterval");
		}
	}
}
