namespace PokerMaster.Domain.Model
{
	public enum UNIT_OF_DAYS_TYPE
	{
		UNIT_OF_DAYS_TYPE_DAILY = 1,
		UNIT_OF_DAYS_TYPE_WEEKLY = 2,
		UNIT_OF_DAYS_TYPE_MONTHLY = 3
	}
}
