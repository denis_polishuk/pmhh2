using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class SCGetBillInfosV3Rsp : DomainModel
	{
		

		public ERROR_CODE_TYPE EErrCode { get; set; }
		public BillInfoNet[] StBillInfoNet { get; set; }
		public int IUnReadNum { get; set; }
		public long LNewestTime { get; set; }
		public long LClubID { get; set; }
		public GAME_ROOM_TYPE EGameRoomType { get; set; }
		public UserStatisticsInfoNetNormals[] StUserStatisticsInfoNetNormals { get; set; }
		public UserStatisticsInfoNetTournaments StUserStatisticsInfoNetTournaments { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			EErrCode = parser.ParseEnum<ERROR_CODE_TYPE>(message, "eErrCode");
			StBillInfoNet = parser.ParseMessageRepeated<BillInfoNet>(message, "stBillInfoNet");
			IUnReadNum = parser.ParseField<int>(message, "iUnReadNum");
			LNewestTime = parser.ParseField<long>(message, "lNewestTime");
			LClubID = parser.ParseField<long>(message, "lClubID");
			EGameRoomType = parser.ParseEnum<GAME_ROOM_TYPE>(message, "eGameRoomType");
			StUserStatisticsInfoNetNormals = parser.ParseMessageRepeated<UserStatisticsInfoNetNormals>(message, "stUserStatisticsInfoNetNormals");
			StUserStatisticsInfoNetTournaments = parser.ParseMessage<UserStatisticsInfoNetTournaments>(message, "stUserStatisticsInfoNetTournaments");
		}
	}
}
