using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class QuickGameRoomInfo : DomainModel
	{
		

		public ChatRoomInfo StChatRoomInfo { get; set; }
		public string SCryptCode { get; set; }
		public QUICK_GAME_ROOM_STATUS EQuickGameRoomStatus { get; set; }
		public GAME_ROOM_TYPE EGameRoomType { get; set; }
		public GameRoomBaseInfo StGameRoomBaseInfo { get; set; }
		public SNGGameRoomBaseInfo StSNGGameRoomBaseInfo { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			StChatRoomInfo = parser.ParseMessage<ChatRoomInfo>(message, "stChatRoomInfo");
			SCryptCode = parser.ParseField<string>(message, "sCryptCode");
			EQuickGameRoomStatus = parser.ParseEnum<QUICK_GAME_ROOM_STATUS>(message, "eQuickGameRoomStatus");
			EGameRoomType = parser.ParseEnum<GAME_ROOM_TYPE>(message, "eGameRoomType");
			StGameRoomBaseInfo = parser.ParseMessage<GameRoomBaseInfo>(message, "stGameRoomBaseInfo");
			StSNGGameRoomBaseInfo = parser.ParseMessage<SNGGameRoomBaseInfo>(message, "stSNGGameRoomBaseInfo");
		}
	}
}
