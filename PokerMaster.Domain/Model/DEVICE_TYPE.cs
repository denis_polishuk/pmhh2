namespace PokerMaster.Domain.Model
{
	public enum DEVICE_TYPE
	{
		DEVICE_TYPE_All = 0,
		DEVICE_TYPE_IOS = 1,
		DEVICE_TYPE_ANDROID = 2
	}
}
