using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class CSGiveMoney : DomainModel
	{
		

		public long Uuid { get; set; }
		public long LRoomID { get; set; }
		public long LPopularity { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			Uuid = parser.ParseField<long>(message, "uuid");
			LRoomID = parser.ParseField<long>(message, "lRoomID");
			LPopularity = parser.ParseField<long>(message, "lPopularity");
		}
	}
}
