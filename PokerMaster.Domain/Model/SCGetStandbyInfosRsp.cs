using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class SCGetStandbyInfosRsp : DomainModel
	{
		

		public ERROR_CODE_TYPE EErrCode { get; set; }
		public long LRoomID { get; set; }
		public UserBaseInfoNet[] VPlays { get; set; }
		public UserBaseInfoNet[] VStandBysOnline { get; set; }
		public UserBaseInfoNet[] VStandBysOffline { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			EErrCode = parser.ParseEnum<ERROR_CODE_TYPE>(message, "eErrCode");
			LRoomID = parser.ParseField<long>(message, "lRoomID");
			VPlays = parser.ParseMessageRepeated<UserBaseInfoNet>(message, "vPlays");
			VStandBysOnline = parser.ParseMessageRepeated<UserBaseInfoNet>(message, "vStandBysOnline");
			VStandBysOffline = parser.ParseMessageRepeated<UserBaseInfoNet>(message, "vStandBysOffline");
		}
	}
}
