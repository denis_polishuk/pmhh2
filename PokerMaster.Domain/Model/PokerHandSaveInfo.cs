using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class PokerHandSaveInfo : DomainModel
	{
		

		public PokerHistoryInfo BaseInfo { get; set; }
		public PokerHandResult HandResult { get; set; }
		public string[] Logs { get; set; }
		public PokerHandAliasInfo[] Aliases { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			BaseInfo = parser.ParseMessage<PokerHistoryInfo>(message, "baseInfo");
			HandResult = parser.ParseMessage<PokerHandResult>(message, "handResult");
			Logs = parser.ParseFieldRepeated<string>(message, "logs");
			Aliases = parser.ParseMessageRepeated<PokerHandAliasInfo>(message, "aliases");
		}
	}
}
