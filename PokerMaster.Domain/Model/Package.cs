using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class Package : DomainModel
	{
		

		public long Uuid { get; set; }
		public byte[] Head { get; set; }
		public long LMisSystemTime { get; set; }
		public byte[] Body { get; set; }
		public long LCurrentSystemTime { get; set; }
		public long ISeqNo { get; set; }
		public TEXAS_CMD ECmd { get; set; }
		public int CEncodeType { get; set; }
		public int Version { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			Uuid = parser.ParseField<long>(message, "uuid");
			Head = parser.ParseByteArray(message, "head");
			LMisSystemTime = parser.ParseField<long>(message, "lMisSystemTime");
			Body = parser.ParseByteArray(message, "body");
			LCurrentSystemTime = parser.ParseField<long>(message, "lCurrentSystemTime");
			ISeqNo = parser.ParseField<long>(message, "iSeqNo");
			ECmd = parser.ParseEnum<TEXAS_CMD>(message, "eCmd");
			CEncodeType = parser.ParseField<int>(message, "cEncodeType");
			Version = parser.ParseField<int>(message, "version");
		}
	}
}
