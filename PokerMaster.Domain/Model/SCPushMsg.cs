using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class SCPushMsg : DomainModel
	{
		

		public PushMsg[] VMsgs { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			VMsgs = parser.ParseMessageRepeated<PushMsg>(message, "vMsgs");
		}
	}
}
