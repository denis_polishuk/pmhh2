using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class UserInfoNetV4 : DomainModel
	{
		

		public UserBaseInfoNet StBaseInfo { get; set; }
		public UserStatisticsInfoNetNormals[] StUserStatisticsInfoNetNormals { get; set; }
		public UserStatisticsInfoNetTournaments[] StUserStatisticsInfoNetTournaments { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			StBaseInfo = parser.ParseMessage<UserBaseInfoNet>(message, "stBaseInfo");
			StUserStatisticsInfoNetNormals = parser.ParseMessageRepeated<UserStatisticsInfoNetNormals>(message, "stUserStatisticsInfoNetNormals");
			StUserStatisticsInfoNetTournaments = parser.ParseMessageRepeated<UserStatisticsInfoNetTournaments>(message, "stUserStatisticsInfoNetTournaments");
		}
	}
}
