using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class BillInfoNet : DomainModel
	{
		

		public GAME_ROOM_TYPE EGameRoomType { get; set; }
		public NormalBillInfoNet StNormalBillInfoNet { get; set; }
		public SNGBillInfoNet StSNGBillInfoNet { get; set; }
		public int BClubManager { get; set; }
		public string SFromClubName { get; set; }
		public string SFromClubUrl { get; set; }
		public string SFromClubCreatorName { get; set; }
		public long LFromClubCreatorNameUuid { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			EGameRoomType = parser.ParseEnum<GAME_ROOM_TYPE>(message, "eGameRoomType");
			StNormalBillInfoNet = parser.ParseMessage<NormalBillInfoNet>(message, "stNormalBillInfoNet");
			StSNGBillInfoNet = parser.ParseMessage<SNGBillInfoNet>(message, "stSNGBillInfoNet");
			BClubManager = parser.ParseField<int>(message, "bClubManager");
			SFromClubName = parser.ParseField<string>(message, "sFromClubName");
			SFromClubUrl = parser.ParseField<string>(message, "sFromClubUrl");
			SFromClubCreatorName = parser.ParseField<string>(message, "sFromClubCreatorName");
			LFromClubCreatorNameUuid = parser.ParseField<long>(message, "lFromClubCreatorNameUuid");
		}
	}
}
