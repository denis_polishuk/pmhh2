namespace PokerMaster.Domain.Model
{
	public enum INSURANCE_STATUS
	{
		INSURANCE_STATUS_NO = 1,
		INSURANCE_STATUS_BETTING = 2,
		INSURANCE_STATUS_RESULT = 3
	}
}
