using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class CSStopSNGGame : DomainModel
	{
		

		public long Uuid { get; set; }
		public long BStop { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			Uuid = parser.ParseField<long>(message, "uuid");
			BStop = parser.ParseField<long>(message, "bStop");
		}
	}
}
