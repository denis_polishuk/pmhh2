using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class FavoritePokerHistoryInfo : DomainModel
	{
		

		public long LFavoriteUuid { get; set; }
		public string SFavoriteName { get; set; }
		public PokerHistoryInfo StPokerHistoryInfo { get; set; }
		public long LFavoriteSaveTime { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			LFavoriteUuid = parser.ParseField<long>(message, "lFavoriteUuid");
			SFavoriteName = parser.ParseField<string>(message, "sFavoriteName");
			StPokerHistoryInfo = parser.ParseMessage<PokerHistoryInfo>(message, "stPokerHistoryInfo");
			LFavoriteSaveTime = parser.ParseField<long>(message, "lFavoriteSaveTime");
		}
	}
}
