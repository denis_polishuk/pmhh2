using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class CSBuyNickname : DomainModel
	{
		

		public long Uuid { get; set; }
		public string SNickname { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			Uuid = parser.ParseField<long>(message, "uuid");
			SNickname = parser.ParseField<string>(message, "sNickname");
		}
	}
}
