using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class SCGetQuickGameRoomRsp : DomainModel
	{
		

		public ERROR_CODE_TYPE EErrCode { get; set; }
		public QuickGameRoomInfo[] VQuickGameRoomInfos { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			EErrCode = parser.ParseEnum<ERROR_CODE_TYPE>(message, "eErrCode");
			VQuickGameRoomInfos = parser.ParseMessageRepeated<QuickGameRoomInfo>(message, "vQuickGameRoomInfos");
		}
	}
}
