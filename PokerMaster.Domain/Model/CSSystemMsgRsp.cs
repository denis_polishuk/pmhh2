using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class CSSystemMsgRsp : DomainModel
	{
		

		public long Uuid { get; set; }
		public long[] VMsgIds { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			Uuid = parser.ParseField<long>(message, "uuid");
			VMsgIds = parser.ParseFieldRepeated<long>(message, "vMsgIds");
		}
	}
}
