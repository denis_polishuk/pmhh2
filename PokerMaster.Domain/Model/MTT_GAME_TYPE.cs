namespace PokerMaster.Domain.Model
{
	public enum MTT_GAME_TYPE
	{
		MTT_GAME_COIN = 1,
		MTT_GAME_OBJECTS = 2,
		MTT_GAME_POPULARITY = 3
	}
}
