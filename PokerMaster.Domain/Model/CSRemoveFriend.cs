using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class CSRemoveFriend : DomainModel
	{
		

		public long Uuid { get; set; }
		public long FriendUuid { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			Uuid = parser.ParseField<long>(message, "uuid");
			FriendUuid = parser.ParseField<long>(message, "friendUuid");
		}
	}
}
