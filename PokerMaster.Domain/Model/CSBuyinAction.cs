using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class CSBuyinAction : DomainModel
	{
		

		public long Uuid { get; set; }
		public long LBuyinActionID { get; set; }
		public long LGameRoomID { get; set; }
		public long LActionUuid { get; set; }
		public BUYIN_CONTROL_TYPE EBuyinControlMessageType { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			Uuid = parser.ParseField<long>(message, "uuid");
			LBuyinActionID = parser.ParseField<long>(message, "lBuyinActionID");
			LGameRoomID = parser.ParseField<long>(message, "lGameRoomID");
			LActionUuid = parser.ParseField<long>(message, "lActionUuid");
			EBuyinControlMessageType = parser.ParseEnum<BUYIN_CONTROL_TYPE>(message, "eBuyinControlMessageType");
		}
	}
}
