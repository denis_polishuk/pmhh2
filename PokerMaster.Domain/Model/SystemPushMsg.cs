using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class SystemPushMsg : DomainModel
	{
		

		public long LMsgID { get; set; }
		public SYSTEM_MESSAGE_TYPE ESystemMessageType { get; set; }
		public string STitle { get; set; }
		public string SUrl { get; set; }
		public string SContent { get; set; }
		public long LTimeoutTime { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			LMsgID = parser.ParseField<long>(message, "lMsgID");
			ESystemMessageType = parser.ParseEnum<SYSTEM_MESSAGE_TYPE>(message, "eSystemMessageType");
			STitle = parser.ParseField<string>(message, "sTitle");
			SUrl = parser.ParseField<string>(message, "sUrl");
			SContent = parser.ParseField<string>(message, "sContent");
			LTimeoutTime = parser.ParseField<long>(message, "lTimeoutTime");
		}
	}
}
