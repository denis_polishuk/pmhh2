namespace PokerMaster.Domain.Model
{
	public enum SEARCH_TYPE
	{
		NICKNAME_SEARCH_TYPE = 1,
		UUID_SEARCH_TYPE = 2,
		ALL_SEARCH_TYPE = 3
	}
}
