using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class SCGameActionRsp : DomainModel
	{
		

		public ERROR_CODE_TYPE EErrCode { get; set; }
		public long LRoomID { get; set; }
		public GAME_ACT_TYPE EGameActType { get; set; }
		public long LStacks { get; set; }
		public long LRemainStacks { get; set; }
		public long LBettingID { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			EErrCode = parser.ParseEnum<ERROR_CODE_TYPE>(message, "eErrCode");
			LRoomID = parser.ParseField<long>(message, "lRoomID");
			EGameActType = parser.ParseEnum<GAME_ACT_TYPE>(message, "eGameActType");
			LStacks = parser.ParseField<long>(message, "lStacks");
			LRemainStacks = parser.ParseField<long>(message, "lRemainStacks");
			LBettingID = parser.ParseField<long>(message, "lBettingID");
		}
	}
}
