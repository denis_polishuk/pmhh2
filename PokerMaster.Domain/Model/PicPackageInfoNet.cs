using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class PicPackageInfoNet : DomainModel
	{
		

		public int LCurrentSeq { get; set; }
		public int LMaxSeq { get; set; }
		public byte[] VVoiceDatas { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			LCurrentSeq = parser.ParseField<int>(message, "lCurrentSeq");
			LMaxSeq = parser.ParseField<int>(message, "lMaxSeq");
			VVoiceDatas = parser.ParseByteArray(message, "vVoiceDatas");
		}
	}
}
