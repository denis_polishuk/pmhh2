using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class SCSystemClubMsg : DomainModel
	{
		

		public ClubPushMsg[] VMsgs { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			VMsgs = parser.ParseMessageRepeated<ClubPushMsg>(message, "vMsgs");
		}
	}
}
