using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class SCGetUserInfoV4Rsp : DomainModel
	{
		

		public ERROR_CODE_TYPE EErrCode { get; set; }
		public UserInfoNetV4 StUserInfoNetV4 { get; set; }
		public int BRemark { get; set; }
		public long LGameRoomID { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			EErrCode = parser.ParseEnum<ERROR_CODE_TYPE>(message, "eErrCode");
			StUserInfoNetV4 = parser.ParseMessage<UserInfoNetV4>(message, "stUserInfoNetV4");
			BRemark = parser.ParseField<int>(message, "bRemark");
			LGameRoomID = parser.ParseField<long>(message, "lGameRoomID");
		}
	}
}
