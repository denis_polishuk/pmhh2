namespace PokerMaster.Domain.Model
{
	public enum SNG_ROOM_TYPE
	{
		QUICK_SNG = 1,
		NORMAL_SNG = 2,
		LONG_SNG = 3,
		DEEP_SNG = 4
	}
}
