using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class BuyinControlMsg : DomainModel
	{
		

		public long LBuyinActionID { get; set; }
		public UserBaseInfoNet StFromUser { get; set; }
		public long LBuyStacks { get; set; }
		public GAME_ROOM_TYPE EGameRoomType { get; set; }
		public GameRoomBaseInfo StGameRoomBaseInfo { get; set; }
		public SNGGameRoomBaseInfo StSNGGameRoomBaseInfo { get; set; }
		public BUYIN_CONTROL_MESSAGE_TYPE EBuyinControlMessageTypeDeny { get; set; }
		public long LActionUuid { get; set; }
		public string SActionNickname { get; set; }
		public long LMsgID { get; set; }
		public long LClubID { get; set; }
		public string SClubName { get; set; }
		public long LBuyinTime { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			LBuyinActionID = parser.ParseField<long>(message, "lBuyinActionID");
			StFromUser = parser.ParseMessage<UserBaseInfoNet>(message, "stFromUser");
			LBuyStacks = parser.ParseField<long>(message, "lBuyStacks");
			EGameRoomType = parser.ParseEnum<GAME_ROOM_TYPE>(message, "eGameRoomType");
			StGameRoomBaseInfo = parser.ParseMessage<GameRoomBaseInfo>(message, "stGameRoomBaseInfo");
			StSNGGameRoomBaseInfo = parser.ParseMessage<SNGGameRoomBaseInfo>(message, "stSNGGameRoomBaseInfo");
			EBuyinControlMessageTypeDeny = parser.ParseEnum<BUYIN_CONTROL_MESSAGE_TYPE>(message, "eBuyinControlMessageTypeDeny");
			LActionUuid = parser.ParseField<long>(message, "lActionUuid");
			SActionNickname = parser.ParseField<string>(message, "sActionNickname");
			LMsgID = parser.ParseField<long>(message, "lMsgID");
			LClubID = parser.ParseField<long>(message, "lClubID");
			SClubName = parser.ParseField<string>(message, "sClubName");
			LBuyinTime = parser.ParseField<long>(message, "lBuyinTime");
		}
	}
}
