using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class UserGameInfoNet : DomainModel
	{
		

		public UserBaseInfoNet StUserInfo { get; set; }
		public USER_GAME_STATE EGameState { get; set; }
		public USER_GAME_ROLE EGameRole { get; set; }
		public long LRoomId { get; set; }
		public long LRemainStacks { get; set; }
		public int BBuyin { get; set; }
		public long LBetStacks { get; set; }
		public int[] VCurrentHands { get; set; }
		public int BGameDealer { get; set; }
		public int IDelayTotalTimes { get; set; }
		public long LActTime { get; set; }
		public long IDelayLong { get; set; }
		public int IDelayCost { get; set; }
		public int IDelaySingleTime { get; set; }
		public int BShowCardOne { get; set; }
		public int BShowCardTwo { get; set; }
		public long LBettingID { get; set; }
		public int LBuyinStacks { get; set; }
		public int BWaitingBuyinConfirmation { get; set; }
		public long LBuyinTime { get; set; }
		public int ISNGRank { get; set; }
		public int BDelegate { get; set; }
		public int BWaitBB { get; set; }
		public int BJustSit { get; set; }
		public int BShowCardThree { get; set; }
		public int BShowCardFour { get; set; }
		public int BKickedInGame { get; set; }
		public long LBuyinClubID { get; set; }
		public int[] VPayTableCards { get; set; }
		public USER_GAME_STATE EPreAction { get; set; }
		public int IUserHandID { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			StUserInfo = parser.ParseMessage<UserBaseInfoNet>(message, "stUserInfo");
			EGameState = parser.ParseEnum<USER_GAME_STATE>(message, "eGameState");
			EGameRole = parser.ParseEnum<USER_GAME_ROLE>(message, "eGameRole");
			LRoomId = parser.ParseField<long>(message, "lRoomId");
			LRemainStacks = parser.ParseField<long>(message, "lRemainStacks");
			BBuyin = parser.ParseField<int>(message, "bBuyin");
			LBetStacks = parser.ParseField<long>(message, "lBetStacks");
			VCurrentHands = parser.ParseFieldRepeated<int>(message, "vCurrentHands");
			BGameDealer = parser.ParseField<int>(message, "bGameDealer");
			IDelayTotalTimes = parser.ParseField<int>(message, "iDelayTotalTimes");
			LActTime = parser.ParseField<long>(message, "lActTime");
			IDelayLong = parser.ParseField<long>(message, "iDelayLong");
			IDelayCost = parser.ParseField<int>(message, "iDelayCost");
			IDelaySingleTime = parser.ParseField<int>(message, "iDelaySingleTime");
			BShowCardOne = parser.ParseField<int>(message, "bShowCardOne");
			BShowCardTwo = parser.ParseField<int>(message, "bShowCardTwo");
			LBettingID = parser.ParseField<long>(message, "lBettingID");
			LBuyinStacks = parser.ParseField<int>(message, "lBuyinStacks");
			BWaitingBuyinConfirmation = parser.ParseField<int>(message, "bWaitingBuyinConfirmation");
			LBuyinTime = parser.ParseField<long>(message, "lBuyinTime");
			ISNGRank = parser.ParseField<int>(message, "iSNGRank");
			BDelegate = parser.ParseField<int>(message, "bDelegate");
			BWaitBB = parser.ParseField<int>(message, "bWaitBB");
			BJustSit = parser.ParseField<int>(message, "bJustSit");
			BShowCardThree = parser.ParseField<int>(message, "bShowCardThree");
			BShowCardFour = parser.ParseField<int>(message, "bShowCardFour");
			BKickedInGame = parser.ParseField<int>(message, "bKickedInGame");
			LBuyinClubID = parser.ParseField<long>(message, "lBuyinClubID");
			VPayTableCards = parser.ParseFieldRepeated<int>(message, "vPayTableCards");
			EPreAction = parser.ParseEnum<USER_GAME_STATE>(message, "ePreAction");
			IUserHandID = parser.ParseField<int>(message, "iUserHandID");
		}
	}
}
