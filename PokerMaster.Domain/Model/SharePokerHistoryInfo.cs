using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class SharePokerHistoryInfo : DomainModel
	{
		

		public long LShareUuid { get; set; }
		public FavoritePokerHistoryInfo StFavoritePokerHistoryInfo { get; set; }
		public long LRewardUnit { get; set; }
		public string SComments { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			LShareUuid = parser.ParseField<long>(message, "lShareUuid");
			StFavoritePokerHistoryInfo = parser.ParseMessage<FavoritePokerHistoryInfo>(message, "stFavoritePokerHistoryInfo");
			LRewardUnit = parser.ParseField<long>(message, "lRewardUnit");
			SComments = parser.ParseField<string>(message, "sComments");
		}
	}
}
