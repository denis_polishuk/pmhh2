using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class CSEnterQuickGameRoom : DomainModel
	{
		

		public long Uuid { get; set; }
		public string SCryptCode { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			Uuid = parser.ParseField<long>(message, "uuid");
			SCryptCode = parser.ParseField<string>(message, "sCryptCode");
		}
	}
}
