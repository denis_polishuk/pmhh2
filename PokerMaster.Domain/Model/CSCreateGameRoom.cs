using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class CSCreateGameRoom : DomainModel
	{
		

		public long Uuid { get; set; }
		public string SRoomName { get; set; }
		public long LSmallBlinds { get; set; }
		public long LBigBlinds { get; set; }
		public long LBuyIn { get; set; }
		public int BBuyIn { get; set; }
		public long LDuration { get; set; }
		public long LFromRoomID { get; set; }
		public int BPrivateRoom { get; set; }
		public UserBaseInfoNet[] VUserBaseInfos { get; set; }
		public GAME_ROOM_TYPE EGameRoomType { get; set; }
		public CREATE_ROOM_TYPE ECreateRoomType { get; set; }
		public GAME_ROOM_SENIOR_TYPE EGameRoomSeniorType { get; set; }
		public int IMaxBuyinRatio { get; set; }
		public int IMinBuyinRatio { get; set; }
		public int BBuyinControl { get; set; }
		public int IGameRoomUserMaxNums { get; set; }
		public int IAnte { get; set; }
		public int BInsurance { get; set; }
		public int BIPLimit { get; set; }
		public int BGPSLimit { get; set; }
		public long LLeagueID { get; set; }
		public int BStraddle { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			Uuid = parser.ParseField<long>(message, "uuid");
			SRoomName = parser.ParseField<string>(message, "sRoomName");
			LSmallBlinds = parser.ParseField<long>(message, "lSmallBlinds");
			LBigBlinds = parser.ParseField<long>(message, "lBigBlinds");
			LBuyIn = parser.ParseField<long>(message, "lBuyIn");
			BBuyIn = parser.ParseField<int>(message, "bBuyIn");
			LDuration = parser.ParseField<long>(message, "lDuration");
			LFromRoomID = parser.ParseField<long>(message, "lFromRoomID");
			BPrivateRoom = parser.ParseField<int>(message, "bPrivateRoom");
			VUserBaseInfos = parser.ParseMessageRepeated<UserBaseInfoNet>(message, "vUserBaseInfos");
			EGameRoomType = parser.ParseEnum<GAME_ROOM_TYPE>(message, "eGameRoomType");
			ECreateRoomType = parser.ParseEnum<CREATE_ROOM_TYPE>(message, "eCreateRoomType");
			EGameRoomSeniorType = parser.ParseEnum<GAME_ROOM_SENIOR_TYPE>(message, "eGameRoomSeniorType");
			IMaxBuyinRatio = parser.ParseField<int>(message, "iMaxBuyinRatio");
			IMinBuyinRatio = parser.ParseField<int>(message, "iMinBuyinRatio");
			BBuyinControl = parser.ParseField<int>(message, "bBuyinControl");
			IGameRoomUserMaxNums = parser.ParseField<int>(message, "iGameRoomUserMaxNums");
			IAnte = parser.ParseField<int>(message, "iAnte");
			BInsurance = parser.ParseField<int>(message, "bInsurance");
			BIPLimit = parser.ParseField<int>(message, "bIPLimit");
			BGPSLimit = parser.ParseField<int>(message, "bGPSLimit");
			LLeagueID = parser.ParseField<long>(message, "lLeagueID");
			BStraddle = parser.ParseField<int>(message, "bStraddle");
		}
	}
}
