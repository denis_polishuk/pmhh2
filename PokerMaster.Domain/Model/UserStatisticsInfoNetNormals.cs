using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class UserStatisticsInfoNetNormals : DomainModel
	{
		

		public GAME_ROOM_TYPE EGameRoomType { get; set; }
		public UNIT_OF_DAYS_TYPE EUnitOfDaysType { get; set; }
		public long LGamesCount { get; set; }
		public long LWonGames { get; set; }
		public long LHandsCount { get; set; }
		public long LDealtHands { get; set; }
		public long LAllinsCount { get; set; }
		public long LWonAllins { get; set; }
		public long LProfitTotally { get; set; }
		public long LPreflopRaises { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			EGameRoomType = parser.ParseEnum<GAME_ROOM_TYPE>(message, "eGameRoomType");
			EUnitOfDaysType = parser.ParseEnum<UNIT_OF_DAYS_TYPE>(message, "eUnitOfDaysType");
			LGamesCount = parser.ParseField<long>(message, "lGamesCount");
			LWonGames = parser.ParseField<long>(message, "lWonGames");
			LHandsCount = parser.ParseField<long>(message, "lHandsCount");
			LDealtHands = parser.ParseField<long>(message, "lDealtHands");
			LAllinsCount = parser.ParseField<long>(message, "lAllinsCount");
			LWonAllins = parser.ParseField<long>(message, "lWonAllins");
			LProfitTotally = parser.ParseField<long>(message, "lProfitTotally");
			LPreflopRaises = parser.ParseField<long>(message, "lPreflopRaises");
		}
	}
}
