using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class SCGetBillInfoByBillIDV3Rsp : DomainModel
	{
		

		public ERROR_CODE_TYPE EErrCode { get; set; }
		public BillInfoNet StBillInfoNet { get; set; }
		public int IShareFlag { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			EErrCode = parser.ParseEnum<ERROR_CODE_TYPE>(message, "eErrCode");
			StBillInfoNet = parser.ParseMessage<BillInfoNet>(message, "stBillInfoNet");
			IShareFlag = parser.ParseField<int>(message, "iShareFlag");
		}
	}
}
