using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class SCUserDelegateRsp : DomainModel
	{
		

		public ERROR_CODE_TYPE EErrCode { get; set; }
		public long LGameRoomID { get; set; }
		public int BDelegate { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			EErrCode = parser.ParseEnum<ERROR_CODE_TYPE>(message, "eErrCode");
			LGameRoomID = parser.ParseField<long>(message, "lGameRoomID");
			BDelegate = parser.ParseField<int>(message, "bDelegate");
		}
	}
}
