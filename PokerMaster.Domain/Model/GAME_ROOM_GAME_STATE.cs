namespace PokerMaster.Domain.Model
{
	public enum GAME_ROOM_GAME_STATE
	{
		ROOM_GAME_STATE_GameWait = 1,
		ROOM_GAME_STATE_GameStart = 2,
		ROOM_GAME_STATE_PreFlop = 3,
		ROOM_GAME_STATE_Flop = 4,
		ROOM_GAME_STATE_Turn = 5,
		ROOM_GAME_STATE_River = 6,
		ROOM_GAME_STATE_Result = 7,
		ROOM_GAME_STATE_SHOWCARD = 8,
		ROOM_GAME_STATE_Over = 9,
		ROOM_GAME_STATE_GamePrepare = 10,
		ROOM_GAME_STATE_Ante = 11,
		ROOM_GAME_STATE_Flop_One = 12,
		ROOM_GAME_STATE_Flop_Two = 13,
		ROOM_GAME_STATE_Flop_Three = 14
	}
}
