using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class GameUserStatisticsInfoNet : DomainModel
	{
		

		public long LHandsCount { get; set; }
		public long LDealtHands { get; set; }
		public long LWonPots { get; set; }
		public long LAllinsCount { get; set; }
		public long LWonAllins { get; set; }
		public long LSurvivedFlops { get; set; }
		public long LFlopWins { get; set; }
		public long LSurvivedTurns { get; set; }
		public long LTurnWins { get; set; }
		public long LSurvivedRivers { get; set; }
		public long LRiverWins { get; set; }
		public long LSurvivedShowdowns { get; set; }
		public long LShowdownWins { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			LHandsCount = parser.ParseField<long>(message, "lHandsCount");
			LDealtHands = parser.ParseField<long>(message, "lDealtHands");
			LWonPots = parser.ParseField<long>(message, "lWonPots");
			LAllinsCount = parser.ParseField<long>(message, "lAllinsCount");
			LWonAllins = parser.ParseField<long>(message, "lWonAllins");
			LSurvivedFlops = parser.ParseField<long>(message, "lSurvivedFlops");
			LFlopWins = parser.ParseField<long>(message, "lFlopWins");
			LSurvivedTurns = parser.ParseField<long>(message, "lSurvivedTurns");
			LTurnWins = parser.ParseField<long>(message, "lTurnWins");
			LSurvivedRivers = parser.ParseField<long>(message, "lSurvivedRivers");
			LRiverWins = parser.ParseField<long>(message, "lRiverWins");
			LSurvivedShowdowns = parser.ParseField<long>(message, "lSurvivedShowdowns");
			LShowdownWins = parser.ParseField<long>(message, "lShowdownWins");
		}
	}
}
