using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class SCSystemChatRoomMsg : DomainModel
	{
		

		public ChatRoomPushMsg[] VMsgs { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			VMsgs = parser.ParseMessageRepeated<ChatRoomPushMsg>(message, "vMsgs");
		}
	}
}
