using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class CSCreateQuickSNGGameRoom : DomainModel
	{
		

		public long Uuid { get; set; }
		public string StrRoomName { get; set; }
		public int BPrivateRoom { get; set; }
		public SNG_ROOM_TYPE ESNGRoomType { get; set; }
		public CREATE_ROOM_TYPE ECreateRoomType { get; set; }
		public long LFromRoomID { get; set; }
		public int IGameRoomUserMaxNums { get; set; }
		public int BBuyinControl { get; set; }
		public int BIPLimit { get; set; }
		public int BGPSLimit { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			Uuid = parser.ParseField<long>(message, "uuid");
			StrRoomName = parser.ParseField<string>(message, "strRoomName");
			BPrivateRoom = parser.ParseField<int>(message, "bPrivateRoom");
			ESNGRoomType = parser.ParseEnum<SNG_ROOM_TYPE>(message, "eSNGRoomType");
			ECreateRoomType = parser.ParseEnum<CREATE_ROOM_TYPE>(message, "eCreateRoomType");
			LFromRoomID = parser.ParseField<long>(message, "lFromRoomID");
			IGameRoomUserMaxNums = parser.ParseField<int>(message, "iGameRoomUserMaxNums");
			BBuyinControl = parser.ParseField<int>(message, "bBuyinControl");
			BIPLimit = parser.ParseField<int>(message, "bIPLimit");
			BGPSLimit = parser.ParseField<int>(message, "bGPSLimit");
		}
	}
}
