using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class SCGiveMoneyPushMsg : DomainModel
	{
		

		public GiveMoneyPushMsg[] VMsgs { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			VMsgs = parser.ParseMessageRepeated<GiveMoneyPushMsg>(message, "vMsgs");
		}
	}
}
