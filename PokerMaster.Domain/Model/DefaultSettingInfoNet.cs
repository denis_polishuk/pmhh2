using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class DefaultSettingInfoNet : DomainModel
	{
		

		public int IMaxInGameUserInfo { get; set; }
		public int ISimultaneousPlays { get; set; }
		public int IKickOff { get; set; }
		public int IFavoriteNum { get; set; }
		public int[] VSBs { get; set; }
		public long[] VPlayDuration { get; set; }
		public int IMaxLevel { get; set; }
		public int IMaxClubNums { get; set; }
		public int ISeniorGamrRoomCost { get; set; }
		public SNGBlindsStructure[] VSNGBlindsStructures { get; set; }
		public OddsStructure[] VOddsStructure { get; set; }
		public int BInsurance { get; set; }
		public ShowCardItemFee[] VShowCardItemFees { get; set; }
		public LeagueGameFeeLadders[] VLeagueGameFeeLadders { get; set; }
		public GameServiceRate[] VGameServiceRates { get; set; }
		public int IEnableFourColorDeck { get; set; }
		public int IEnableGamingMute { get; set; }
		public int IEnableChattingMute { get; set; }
		public int IEnableRaiseAssistant { get; set; }
		public RaiseShortcutsOptions[] VRaiseShortcutsOptions { get; set; }
		public RaiseShortcutsOptions[] VRaiseShortcutsOptionsPotLimit { get; set; }
		public int ITableThemeOrdinal { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			IMaxInGameUserInfo = parser.ParseField<int>(message, "iMaxInGameUserInfo");
			ISimultaneousPlays = parser.ParseField<int>(message, "iSimultaneousPlays");
			IKickOff = parser.ParseField<int>(message, "iKickOff");
			IFavoriteNum = parser.ParseField<int>(message, "iFavoriteNum");
			VSBs = parser.ParseFieldRepeated<int>(message, "vSBs");
			VPlayDuration = parser.ParseFieldRepeated<long>(message, "vPlayDuration");
			IMaxLevel = parser.ParseField<int>(message, "iMaxLevel");
			IMaxClubNums = parser.ParseField<int>(message, "iMaxClubNums");
			ISeniorGamrRoomCost = parser.ParseField<int>(message, "iSeniorGamrRoomCost");
			VSNGBlindsStructures = parser.ParseMessageRepeated<SNGBlindsStructure>(message, "vSNGBlindsStructures");
			VOddsStructure = parser.ParseMessageRepeated<OddsStructure>(message, "vOddsStructure");
			BInsurance = parser.ParseField<int>(message, "bInsurance");
			VShowCardItemFees = parser.ParseMessageRepeated<ShowCardItemFee>(message, "vShowCardItemFees");
			VLeagueGameFeeLadders = parser.ParseMessageRepeated<LeagueGameFeeLadders>(message, "vLeagueGameFeeLadders");
			VGameServiceRates = parser.ParseMessageRepeated<GameServiceRate>(message, "vGameServiceRates");
			IEnableFourColorDeck = parser.ParseField<int>(message, "iEnableFourColorDeck");
			IEnableGamingMute = parser.ParseField<int>(message, "iEnableGamingMute");
			IEnableChattingMute = parser.ParseField<int>(message, "iEnableChattingMute");
			IEnableRaiseAssistant = parser.ParseField<int>(message, "iEnableRaiseAssistant");
			VRaiseShortcutsOptions = parser.ParseMessageRepeated<RaiseShortcutsOptions>(message, "vRaiseShortcutsOptions");
			VRaiseShortcutsOptionsPotLimit = parser.ParseMessageRepeated<RaiseShortcutsOptions>(message, "vRaiseShortcutsOptionsPotLimit");
			ITableThemeOrdinal = parser.ParseField<int>(message, "iTableThemeOrdinal");
		}
	}
}
