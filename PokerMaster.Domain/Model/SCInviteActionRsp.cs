using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class SCInviteActionRsp : DomainModel
	{
		

		public ERROR_CODE_TYPE EErrCode { get; set; }
		public UserFriendInfoNet StInviteUser { get; set; }
		public USER_FRIEND_STATE BState { get; set; }
		public string StrMessage { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			EErrCode = parser.ParseEnum<ERROR_CODE_TYPE>(message, "eErrCode");
			StInviteUser = parser.ParseMessage<UserFriendInfoNet>(message, "stInviteUser");
			BState = parser.ParseEnum<USER_FRIEND_STATE>(message, "bState");
			StrMessage = parser.ParseField<string>(message, "strMessage");
		}
	}
}
