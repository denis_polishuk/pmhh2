using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class InsurancePotInfo : DomainModel
	{
		

		public long LPotID { get; set; }
		public long LPotStacks { get; set; }
		public int[] VOuts { get; set; }
		public int IOdds { get; set; }
		public long LTotalPotStacks { get; set; }
		public long LNoLosses { get; set; }
		public long LMinStacks { get; set; }
		public long LMaxStacks { get; set; }
		public long LBetStacks { get; set; }
		public long LGetStacks { get; set; }
		public int IPotIndex { get; set; }
		public long LSysStacks { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			LPotID = parser.ParseField<long>(message, "lPotID");
			LPotStacks = parser.ParseField<long>(message, "lPotStacks");
			VOuts = parser.ParseFieldRepeated<int>(message, "vOuts");
			IOdds = parser.ParseField<int>(message, "iOdds");
			LTotalPotStacks = parser.ParseField<long>(message, "lTotalPotStacks");
			LNoLosses = parser.ParseField<long>(message, "lNoLosses");
			LMinStacks = parser.ParseField<long>(message, "lMinStacks");
			LMaxStacks = parser.ParseField<long>(message, "lMaxStacks");
			LBetStacks = parser.ParseField<long>(message, "lBetStacks");
			LGetStacks = parser.ParseField<long>(message, "lGetStacks");
			IPotIndex = parser.ParseField<int>(message, "iPotIndex");
			LSysStacks = parser.ParseField<long>(message, "lSysStacks");
		}
	}
}
