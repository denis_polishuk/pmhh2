using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class NickNameLocation : DomainModel
	{
		

		public string SLocation { get; set; }
		public string SNickName { get; set; }
		public string SAnonymous { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			SLocation = parser.ParseField<string>(message, "sLocation");
			SNickName = parser.ParseField<string>(message, "sNickName");
			SAnonymous = parser.ParseField<string>(message, "sAnonymous");
		}
	}
}
