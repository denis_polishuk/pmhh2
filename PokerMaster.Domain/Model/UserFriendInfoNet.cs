using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class UserFriendInfoNet : DomainModel
	{
		

		public UserBaseInfoNet StUserBaseInfo { get; set; }
		public USER_STATE EUserState { get; set; }
		public USER_FRIEND_STATE EFriendState { get; set; }
		public long LRoomId { get; set; }
		public int IMute { get; set; }
		public int IVibrate { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			StUserBaseInfo = parser.ParseMessage<UserBaseInfoNet>(message, "stUserBaseInfo");
			EUserState = parser.ParseEnum<USER_STATE>(message, "eUserState");
			EFriendState = parser.ParseEnum<USER_FRIEND_STATE>(message, "eFriendState");
			LRoomId = parser.ParseField<long>(message, "lRoomId");
			IMute = parser.ParseField<int>(message, "iMute");
			IVibrate = parser.ParseField<int>(message, "iVibrate");
		}
	}
}
