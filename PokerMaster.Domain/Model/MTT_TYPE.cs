namespace PokerMaster.Domain.Model
{
	public enum MTT_TYPE
	{
		MTT_TYPE_NORMAL = 1,
		MTT_TYPE_TURBO = 2,
		MTT_TYPE_HYPERTURBO = 3
	}
}
