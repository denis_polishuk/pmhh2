using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class MttPokerHandInfo : DomainModel
	{
		

		public long LRoomID { get; set; }
		public long LHandID { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			LRoomID = parser.ParseField<long>(message, "lRoomID");
			LHandID = parser.ParseField<long>(message, "lHandID");
		}
	}
}
