namespace PokerMaster.Domain.Model
{
	public enum GAME_ACT_TYPE
	{
		GAME_ACT_TYPE_FOLD = 1,
		GAME_ACT_TYPE_CALL = 2,
		GAME_ACT_TYPE_BET = 3,
		GAME_ACT_TYPE_CHECK = 4,
		GAME_ACT_TYPE_RAISE = 5,
		GAME_ACT_TYPE_ALLIN = 6
	}
}
