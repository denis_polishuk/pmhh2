using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class ChatRoomPushMsg : DomainModel
	{
		

		public long LMsgID { get; set; }
		public CHAT_ROOM_MESSAGE_TYPE EGameMessageType { get; set; }
		public string SMsg { get; set; }
		public long LCreateTime { get; set; }
		public long LRoomID { get; set; }
		public UserBaseInfoNet StFromUser { get; set; }
		public UserBaseInfoNet[] VUsers { get; set; }
		public ChatRoomBaseInfo StChatRoomBaseInfo { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			LMsgID = parser.ParseField<long>(message, "lMsgID");
			EGameMessageType = parser.ParseEnum<CHAT_ROOM_MESSAGE_TYPE>(message, "eGameMessageType");
			SMsg = parser.ParseField<string>(message, "sMsg");
			LCreateTime = parser.ParseField<long>(message, "lCreateTime");
			LRoomID = parser.ParseField<long>(message, "lRoomID");
			StFromUser = parser.ParseMessage<UserBaseInfoNet>(message, "stFromUser");
			VUsers = parser.ParseMessageRepeated<UserBaseInfoNet>(message, "vUsers");
			StChatRoomBaseInfo = parser.ParseMessage<ChatRoomBaseInfo>(message, "stChatRoomBaseInfo");
		}
	}
}
