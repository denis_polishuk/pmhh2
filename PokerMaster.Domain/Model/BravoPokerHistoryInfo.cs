using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class BravoPokerHistoryInfo : DomainModel
	{
		

		public long LRoomID { get; set; }
		public long LHandID { get; set; }
		public int IType { get; set; }
		public long LWinUuid { get; set; }
		public string SNickName { get; set; }
		public string SSmallIcon { get; set; }
		public string SDesc { get; set; }
		public long LMaxPot { get; set; }
		public string SPokerHistoryUrl { get; set; }
		public string SPokerHistoryShareUrl { get; set; }
		public int[] VHandCards { get; set; }
		public string SPokerHistoryName { get; set; }
		public string SPokerDesc { get; set; }
		public long LSaveUuid { get; set; }
		public string SPokerAnonymousShareUrl { get; set; }
		public string SPokerHistoryAnonymousName { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			LRoomID = parser.ParseField<long>(message, "lRoomID");
			LHandID = parser.ParseField<long>(message, "lHandID");
			IType = parser.ParseField<int>(message, "iType");
			LWinUuid = parser.ParseField<long>(message, "lWinUuid");
			SNickName = parser.ParseField<string>(message, "sNickName");
			SSmallIcon = parser.ParseField<string>(message, "sSmallIcon");
			SDesc = parser.ParseField<string>(message, "sDesc");
			LMaxPot = parser.ParseField<long>(message, "lMaxPot");
			SPokerHistoryUrl = parser.ParseField<string>(message, "sPokerHistoryUrl");
			SPokerHistoryShareUrl = parser.ParseField<string>(message, "sPokerHistoryShareUrl");
			VHandCards = parser.ParseFieldRepeated<int>(message, "vHandCards");
			SPokerHistoryName = parser.ParseField<string>(message, "sPokerHistoryName");
			SPokerDesc = parser.ParseField<string>(message, "sPokerDesc");
			LSaveUuid = parser.ParseField<long>(message, "lSaveUuid");
			SPokerAnonymousShareUrl = parser.ParseField<string>(message, "sPokerAnonymousShareUrl");
			SPokerHistoryAnonymousName = parser.ParseField<string>(message, "sPokerHistoryAnonymousName");
		}
	}
}
