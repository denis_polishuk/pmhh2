using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class CSForceStandup : DomainModel
	{
		

		public long Uuid { get; set; }
		public long LGameRoomID { get; set; }
		public int LForceStandUpUuid { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			Uuid = parser.ParseField<long>(message, "uuid");
			LGameRoomID = parser.ParseField<long>(message, "lGameRoomID");
			LForceStandUpUuid = parser.ParseField<int>(message, "lForceStandUpUuid");
		}
	}
}
