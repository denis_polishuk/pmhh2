using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class UserBillInfoNet : DomainModel
	{
		

		public long Uuid { get; set; }
		public long LTotalBuyStacks { get; set; }
		public long LRemainBuyStacks { get; set; }
		public long ISngRank { get; set; }
		public long LInsuranceBuyin { get; set; }
		public long LInsuranceGetStacks { get; set; }
		public long LClubID { get; set; }
		public string SClubName { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			Uuid = parser.ParseField<long>(message, "uuid");
			LTotalBuyStacks = parser.ParseField<long>(message, "lTotalBuyStacks");
			LRemainBuyStacks = parser.ParseField<long>(message, "lRemainBuyStacks");
			ISngRank = parser.ParseField<long>(message, "iSngRank");
			LInsuranceBuyin = parser.ParseField<long>(message, "lInsuranceBuyin");
			LInsuranceGetStacks = parser.ParseField<long>(message, "lInsuranceGetStacks");
			LClubID = parser.ParseField<long>(message, "lClubID");
			SClubName = parser.ParseField<string>(message, "sClubName");
		}
	}
}
