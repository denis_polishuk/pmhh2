using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class SCGetBillInfoByRoomIDRsp : DomainModel
	{
		

		public ERROR_CODE_TYPE EErrCode { get; set; }
		public BillInfoNet StBillInfoNet { get; set; }
		public UserBaseInfoNet[] VPlays { get; set; }
		public UserBaseInfoNet[] VStandBysOnline { get; set; }
		public UserBaseInfoNet[] VStandBysOffline { get; set; }
		public long LGameRoomID { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			EErrCode = parser.ParseEnum<ERROR_CODE_TYPE>(message, "eErrCode");
			StBillInfoNet = parser.ParseMessage<BillInfoNet>(message, "stBillInfoNet");
			VPlays = parser.ParseMessageRepeated<UserBaseInfoNet>(message, "vPlays");
			VStandBysOnline = parser.ParseMessageRepeated<UserBaseInfoNet>(message, "vStandBysOnline");
			VStandBysOffline = parser.ParseMessageRepeated<UserBaseInfoNet>(message, "vStandBysOffline");
			LGameRoomID = parser.ParseField<long>(message, "lGameRoomID");
		}
	}
}
