using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class CSLoginThirdParty : DomainModel
	{
		

		public string StrID { get; set; }
		public ID_TYPE EIDType { get; set; }
		public DEVICE_TYPE EDeviceType { get; set; }
		public string StrIosToken { get; set; }
		public string StrSSOToken { get; set; }
		public string StrSysVersion { get; set; }
		public string StrPhoneType { get; set; }
		public string StrScreenSize { get; set; }
		public string StrCover { get; set; }
		public string StrNick { get; set; }
		public int IGender { get; set; }
		public LANGUAGE_TYPE ELanguageType { get; set; }
		public string SExtra { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			StrID = parser.ParseField<string>(message, "strID");
			EIDType = parser.ParseEnum<ID_TYPE>(message, "eIDType");
			EDeviceType = parser.ParseEnum<DEVICE_TYPE>(message, "eDeviceType");
			StrIosToken = parser.ParseField<string>(message, "strIosToken");
			StrSSOToken = parser.ParseField<string>(message, "strSSOToken");
			StrSysVersion = parser.ParseField<string>(message, "strSysVersion");
			StrPhoneType = parser.ParseField<string>(message, "strPhoneType");
			StrScreenSize = parser.ParseField<string>(message, "strScreenSize");
			StrCover = parser.ParseField<string>(message, "strCover");
			StrNick = parser.ParseField<string>(message, "strNick");
			IGender = parser.ParseField<int>(message, "iGender");
			ELanguageType = parser.ParseEnum<LANGUAGE_TYPE>(message, "eLanguageType");
			SExtra = parser.ParseField<string>(message, "sExtra");
		}
	}
}
