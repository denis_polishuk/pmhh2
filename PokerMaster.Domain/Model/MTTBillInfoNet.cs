using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class MTTBillInfoNet : DomainModel
	{
		

		public long LBillID { get; set; }
		public MTTUserBillInfoNet StSelfMTTUserBillInfo { get; set; }
		public MTTUserBillInfoNet[] VMTTUserBillInfos { get; set; }
		public GameUserStatisticsInfoNet StGameUserStatisticsInfoNet { get; set; }
		public int ITotalNum { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			LBillID = parser.ParseField<long>(message, "lBillID");
			StSelfMTTUserBillInfo = parser.ParseMessage<MTTUserBillInfoNet>(message, "stSelfMTTUserBillInfo");
			VMTTUserBillInfos = parser.ParseMessageRepeated<MTTUserBillInfoNet>(message, "vMTTUserBillInfos");
			StGameUserStatisticsInfoNet = parser.ParseMessage<GameUserStatisticsInfoNet>(message, "stGameUserStatisticsInfoNet");
			ITotalNum = parser.ParseField<int>(message, "iTotalNum");
		}
	}
}
