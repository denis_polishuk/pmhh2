using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class SCGetFrontPageInfoV2Rsp : DomainModel
	{
		

		public ERROR_CODE_TYPE EErrCode { get; set; }
		public BannerInfo[] VBannerInfos { get; set; }
		public RecommendUserInfo[] VRecommendUserInfos { get; set; }
		public ClubBaseInfo[] VRecommendClubBaseInfos { get; set; }
		public UserBaseInfoNet[] VUserBaseInfos { get; set; }
		public int IOffset { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			EErrCode = parser.ParseEnum<ERROR_CODE_TYPE>(message, "eErrCode");
			VBannerInfos = parser.ParseMessageRepeated<BannerInfo>(message, "vBannerInfos");
			VRecommendUserInfos = parser.ParseMessageRepeated<RecommendUserInfo>(message, "vRecommendUserInfos");
			VRecommendClubBaseInfos = parser.ParseMessageRepeated<ClubBaseInfo>(message, "vRecommendClubBaseInfos");
			VUserBaseInfos = parser.ParseMessageRepeated<UserBaseInfoNet>(message, "vUserBaseInfos");
			IOffset = parser.ParseField<int>(message, "iOffset");
		}
	}
}
