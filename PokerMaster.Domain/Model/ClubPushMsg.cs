using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class ClubPushMsg : DomainModel
	{
		

		public long LMsgID { get; set; }
		public CLUB_MESSAGE_TYPE EClubMessageType { get; set; }
		public UserBaseInfoNet StFromUser { get; set; }
		public UserBaseInfoNet StToUser { get; set; }
		public ClubBaseInfo StClubBaseInfo { get; set; }
		public string SMsg { get; set; }
		public long LCreateTime { get; set; }
		public long LFund { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			LMsgID = parser.ParseField<long>(message, "lMsgID");
			EClubMessageType = parser.ParseEnum<CLUB_MESSAGE_TYPE>(message, "eClubMessageType");
			StFromUser = parser.ParseMessage<UserBaseInfoNet>(message, "stFromUser");
			StToUser = parser.ParseMessage<UserBaseInfoNet>(message, "stToUser");
			StClubBaseInfo = parser.ParseMessage<ClubBaseInfo>(message, "stClubBaseInfo");
			SMsg = parser.ParseField<string>(message, "sMsg");
			LCreateTime = parser.ParseField<long>(message, "lCreateTime");
			LFund = parser.ParseField<long>(message, "lFund");
		}
	}
}
