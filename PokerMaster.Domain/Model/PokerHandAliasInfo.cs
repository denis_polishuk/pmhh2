using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class PokerHandAliasInfo : DomainModel
	{
		

		public long LUuid { get; set; }
		public string Alias { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			LUuid = parser.ParseField<long>(message, "lUuid");
			Alias = parser.ParseField<string>(message, "alias");
		}
	}
}
