using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class NormalBillInfoNet : DomainModel
	{
		

		public long LBillID { get; set; }
		public int BOwnerDeleted { get; set; }
		public string SPlayName { get; set; }
		public UserBaseInfoNet StCreateUserBaseInfo { get; set; }
		public long LCreateTime { get; set; }
		public long LDuration { get; set; }
		public long LSmallBlind { get; set; }
		public long LBigBlind { get; set; }
		public long LBuyin { get; set; }
		public long LTotalBuyin { get; set; }
		public long LTotalGameNum { get; set; }
		public long LMaxPot { get; set; }
		public UserBillInfoNet[] VUserBillInfos { get; set; }
		public string SFromText { get; set; }
		public CREATE_ROOM_TYPE ECreateRoomType { get; set; }
		public long LFromRoomID { get; set; }
		public int IGameRoomUserMaxNums { get; set; }
		public GameUserStatisticsInfoNet StGameUserStatisticsInfoNet { get; set; }
		public int IAnte { get; set; }
		public int BInsurance { get; set; }
		public long LLeagueID { get; set; }
		public string SLeagueName { get; set; }
		public long LProfitStacks { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			LBillID = parser.ParseField<long>(message, "lBillID");
			BOwnerDeleted = parser.ParseField<int>(message, "bOwnerDeleted");
			SPlayName = parser.ParseField<string>(message, "sPlayName");
			StCreateUserBaseInfo = parser.ParseMessage<UserBaseInfoNet>(message, "stCreateUserBaseInfo");
			LCreateTime = parser.ParseField<long>(message, "lCreateTime");
			LDuration = parser.ParseField<long>(message, "lDuration");
			LSmallBlind = parser.ParseField<long>(message, "lSmallBlind");
			LBigBlind = parser.ParseField<long>(message, "lBigBlind");
			LBuyin = parser.ParseField<long>(message, "lBuyin");
			LTotalBuyin = parser.ParseField<long>(message, "lTotalBuyin");
			LTotalGameNum = parser.ParseField<long>(message, "lTotalGameNum");
			LMaxPot = parser.ParseField<long>(message, "lMaxPot");
			VUserBillInfos = parser.ParseMessageRepeated<UserBillInfoNet>(message, "vUserBillInfos");
			SFromText = parser.ParseField<string>(message, "sFromText");
			ECreateRoomType = parser.ParseEnum<CREATE_ROOM_TYPE>(message, "eCreateRoomType");
			LFromRoomID = parser.ParseField<long>(message, "lFromRoomID");
			IGameRoomUserMaxNums = parser.ParseField<int>(message, "iGameRoomUserMaxNums");
			StGameUserStatisticsInfoNet = parser.ParseMessage<GameUserStatisticsInfoNet>(message, "stGameUserStatisticsInfoNet");
			IAnte = parser.ParseField<int>(message, "iAnte");
			BInsurance = parser.ParseField<int>(message, "bInsurance");
			LLeagueID = parser.ParseField<long>(message, "lLeagueID");
			SLeagueName = parser.ParseField<string>(message, "sLeagueName");
			LProfitStacks = parser.ParseField<long>(message, "lProfitStacks");
		}
	}
}
