using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class SCUploadChatRoomIconRsp : DomainModel
	{
		

		public ERROR_CODE_TYPE EErrCode { get; set; }
		public string SIconUrl { get; set; }
		public long LRoomID { get; set; }
		public string SSmallIconUrl { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			EErrCode = parser.ParseEnum<ERROR_CODE_TYPE>(message, "eErrCode");
			SIconUrl = parser.ParseField<string>(message, "sIconUrl");
			LRoomID = parser.ParseField<long>(message, "lRoomID");
			SSmallIconUrl = parser.ParseField<string>(message, "sSmallIconUrl");
		}
	}
}
