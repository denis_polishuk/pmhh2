using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class TicketInfo : DomainModel
	{
		

		public string STicketID { get; set; }
		public string STicketTypeName { get; set; }
		public int ITicketTypeID { get; set; }
		public string STicketName { get; set; }
		public string STicketLogo { get; set; }
		public long LCreateTime { get; set; }
		public long LExpirationTime { get; set; }
		public string SRedeemCode { get; set; }
		public int ICirculationNums { get; set; }
		public int ICirculationUsedNums { get; set; }
		public long LBelongUuid { get; set; }
		public string STicketDesc { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			STicketID = parser.ParseField<string>(message, "sTicketID");
			STicketTypeName = parser.ParseField<string>(message, "sTicketTypeName");
			ITicketTypeID = parser.ParseField<int>(message, "iTicketTypeID");
			STicketName = parser.ParseField<string>(message, "sTicketName");
			STicketLogo = parser.ParseField<string>(message, "sTicketLogo");
			LCreateTime = parser.ParseField<long>(message, "lCreateTime");
			LExpirationTime = parser.ParseField<long>(message, "lExpirationTime");
			SRedeemCode = parser.ParseField<string>(message, "sRedeemCode");
			ICirculationNums = parser.ParseField<int>(message, "iCirculationNums");
			ICirculationUsedNums = parser.ParseField<int>(message, "iCirculationUsedNums");
			LBelongUuid = parser.ParseField<long>(message, "lBelongUuid");
			STicketDesc = parser.ParseField<string>(message, "sTicketDesc");
		}
	}
}
