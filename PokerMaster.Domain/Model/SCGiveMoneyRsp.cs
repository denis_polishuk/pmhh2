using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class SCGiveMoneyRsp : DomainModel
	{
		

		public ERROR_CODE_TYPE EErrCode { get; set; }
		public long LPopularity { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			EErrCode = parser.ParseEnum<ERROR_CODE_TYPE>(message, "eErrCode");
			LPopularity = parser.ParseField<long>(message, "lPopularity");
		}
	}
}
