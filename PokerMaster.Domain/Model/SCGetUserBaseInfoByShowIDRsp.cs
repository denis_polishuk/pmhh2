using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class SCGetUserBaseInfoByShowIDRsp : DomainModel
	{
		

		public ERROR_CODE_TYPE EErrCode { get; set; }
		public string SShowID { get; set; }
		public string StrCover { get; set; }
		public string StrNick { get; set; }
		public int IGender { get; set; }
		public long Uuid { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			EErrCode = parser.ParseEnum<ERROR_CODE_TYPE>(message, "eErrCode");
			SShowID = parser.ParseField<string>(message, "sShowID");
			StrCover = parser.ParseField<string>(message, "strCover");
			StrNick = parser.ParseField<string>(message, "strNick");
			IGender = parser.ParseField<int>(message, "iGender");
			Uuid = parser.ParseField<long>(message, "uuid");
		}
	}
}
