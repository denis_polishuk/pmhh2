using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class SCGetLoginRewardRsp : DomainModel
	{
		

		public ERROR_CODE_TYPE EErrCode { get; set; }
		public long LPopularity { get; set; }
		public long LCoin { get; set; }
		public long LAddPopularity { get; set; }
		public long LAddCoin { get; set; }
		public int[] VCards { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			EErrCode = parser.ParseEnum<ERROR_CODE_TYPE>(message, "eErrCode");
			LPopularity = parser.ParseField<long>(message, "lPopularity");
			LCoin = parser.ParseField<long>(message, "lCoin");
			LAddPopularity = parser.ParseField<long>(message, "lAddPopularity");
			LAddCoin = parser.ParseField<long>(message, "lAddCoin");
			VCards = parser.ParseFieldRepeated<int>(message, "vCards");
		}
	}
}
