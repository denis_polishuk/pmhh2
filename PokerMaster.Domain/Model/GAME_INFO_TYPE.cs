namespace PokerMaster.Domain.Model
{
	public enum GAME_INFO_TYPE
	{
		ALL = 1,
		MY_GAME = 2,
		FRIENDS_GAME = 3,
		BILLS = 4
	}
}
