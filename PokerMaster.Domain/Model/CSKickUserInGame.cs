using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class CSKickUserInGame : DomainModel
	{
		

		public long Uuid { get; set; }
		public long LGameRoomID { get; set; }
		public int LKickUserUuid { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			Uuid = parser.ParseField<long>(message, "uuid");
			LGameRoomID = parser.ParseField<long>(message, "lGameRoomID");
			LKickUserUuid = parser.ParseField<int>(message, "lKickUserUuid");
		}
	}
}
