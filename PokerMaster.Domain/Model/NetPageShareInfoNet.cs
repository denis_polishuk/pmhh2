using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class NetPageShareInfoNet : DomainModel
	{
		

		public long LPageID { get; set; }
		public string SPageTitle { get; set; }
		public string SCoverPicUrl { get; set; }
		public string SPageDesc { get; set; }
		public string SUrl { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			LPageID = parser.ParseField<long>(message, "lPageID");
			SPageTitle = parser.ParseField<string>(message, "sPageTitle");
			SCoverPicUrl = parser.ParseField<string>(message, "sCoverPicUrl");
			SPageDesc = parser.ParseField<string>(message, "sPageDesc");
			SUrl = parser.ParseField<string>(message, "sUrl");
		}
	}
}
