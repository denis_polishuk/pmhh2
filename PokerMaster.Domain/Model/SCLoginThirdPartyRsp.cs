using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class SCLoginThirdPartyRsp : DomainModel
	{
		

		public ERROR_CODE_TYPE EErrCode { get; set; }
		public UserBaseInfoNet StUserBaseInfo { get; set; }
		public DefaultSettingInfoNet StDefaultSettingInfo { get; set; }
		public int BNewUser { get; set; }
		public string SAccessToken { get; set; }
		public string SEncryptKey { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			EErrCode = parser.ParseEnum<ERROR_CODE_TYPE>(message, "eErrCode");
			StUserBaseInfo = parser.ParseMessage<UserBaseInfoNet>(message, "stUserBaseInfo");
			StDefaultSettingInfo = parser.ParseMessage<DefaultSettingInfoNet>(message, "stDefaultSettingInfo");
			BNewUser = parser.ParseField<int>(message, "bNewUser");
			SAccessToken = parser.ParseField<string>(message, "sAccessToken");
			SEncryptKey = parser.ParseField<string>(message, "sEncryptKey");
		}
	}
}
