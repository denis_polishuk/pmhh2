using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class GlobalDefaultSettingInfoNet : DomainModel
	{
		

		public int IFavoriteNum { get; set; }
		public int BInsurance { get; set; }
		public int BMTT { get; set; }
		public int IBillNumsPerpage { get; set; }
		public int BHasSG { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			IFavoriteNum = parser.ParseField<int>(message, "iFavoriteNum");
			BInsurance = parser.ParseField<int>(message, "bInsurance");
			BMTT = parser.ParseField<int>(message, "bMTT");
			IBillNumsPerpage = parser.ParseField<int>(message, "iBillNumsPerpage");
			BHasSG = parser.ParseField<int>(message, "bHasSG");
		}
	}
}
