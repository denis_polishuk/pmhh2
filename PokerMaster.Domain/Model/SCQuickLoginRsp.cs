using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class SCQuickLoginRsp : DomainModel
	{
		

		public ERROR_CODE_TYPE EErrCode { get; set; }
		public UserBaseInfoNet StUserBaseInfoNet { get; set; }
		public DefaultSettingInfoNet StDefaultSettingInfo { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			EErrCode = parser.ParseEnum<ERROR_CODE_TYPE>(message, "eErrCode");
			StUserBaseInfoNet = parser.ParseMessage<UserBaseInfoNet>(message, "stUserBaseInfoNet");
			StDefaultSettingInfo = parser.ParseMessage<DefaultSettingInfoNet>(message, "stDefaultSettingInfo");
		}
	}
}
