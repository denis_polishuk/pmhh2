using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class SCCreateSNGGameRoomRsp : DomainModel
	{
		

		public ERROR_CODE_TYPE EErrCode { get; set; }
		public long LGameRoomId { get; set; }
		public SNGGameRoomBaseInfo StSNGGameRoomBaseInfo { get; set; }
		public UserBaseInfoNet StUserBaseInfoNet { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			EErrCode = parser.ParseEnum<ERROR_CODE_TYPE>(message, "eErrCode");
			LGameRoomId = parser.ParseField<long>(message, "lGameRoomId");
			StSNGGameRoomBaseInfo = parser.ParseMessage<SNGGameRoomBaseInfo>(message, "stSNGGameRoomBaseInfo");
			StUserBaseInfoNet = parser.ParseMessage<UserBaseInfoNet>(message, "stUserBaseInfoNet");
		}
	}
}
