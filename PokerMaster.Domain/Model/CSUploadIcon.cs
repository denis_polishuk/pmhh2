using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class CSUploadIcon : DomainModel
	{
		

		public long Uuid { get; set; }
		public byte[] VIconDatas { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			Uuid = parser.ParseField<long>(message, "uuid");
			VIconDatas = parser.ParseByteArray(message, "vIconDatas");
		}
	}
}
