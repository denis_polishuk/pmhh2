namespace PokerMaster.Domain.Model
{
	public enum MTT_GAME_BONUS_TYPE
	{
		MTT_GAME_BONUS_STATIC = 1,
		MTT_GAME_BONUS_DYNAMICS = 2
	}
}
