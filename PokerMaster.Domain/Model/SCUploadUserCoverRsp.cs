using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class SCUploadUserCoverRsp : DomainModel
	{
		

		public ERROR_CODE_TYPE EErrCode { get; set; }
		public string[] SSmallAlbums { get; set; }
		public string[] SBigAlbums { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			EErrCode = parser.ParseEnum<ERROR_CODE_TYPE>(message, "eErrCode");
			SSmallAlbums = parser.ParseFieldRepeated<string>(message, "sSmallAlbums");
			SBigAlbums = parser.ParseFieldRepeated<string>(message, "sBigAlbums");
		}
	}
}
