using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class SCSystemQuickGameRoomMsg : DomainModel
	{
		

		public QuickGameRoomPushMsg[] VMsgs { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			VMsgs = parser.ParseMessageRepeated<QuickGameRoomPushMsg>(message, "vMsgs");
		}
	}
}
