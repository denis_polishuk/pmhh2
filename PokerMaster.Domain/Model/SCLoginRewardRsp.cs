using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class SCLoginRewardRsp : DomainModel
	{
		

		public ERROR_CODE_TYPE EErrCode { get; set; }
		public int BLoginReward { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			EErrCode = parser.ParseEnum<ERROR_CODE_TYPE>(message, "eErrCode");
			BLoginReward = parser.ParseField<int>(message, "bLoginReward");
		}
	}
}
