using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class PotInfo : DomainModel
	{
		

		public long LPotStacks { get; set; }
		public long[] VUuids { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			LPotStacks = parser.ParseField<long>(message, "lPotStacks");
			VUuids = parser.ParseFieldRepeated<long>(message, "vUuids");
		}
	}
}
