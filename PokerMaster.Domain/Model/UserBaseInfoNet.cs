using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class UserBaseInfoNet : DomainModel
	{
		

		public long Uuid { get; set; }
		public ID_TYPE EType { get; set; }
		public string StrID { get; set; }
		public string StrCover { get; set; }
		public string StrNick { get; set; }
		public string StrPhoneNumber { get; set; }
		public VIP_TYPE EVipType { get; set; }
		public int IGender { get; set; }
		public long LLoginTime { get; set; }
		public string StrDesc { get; set; }
		public long LCoin { get; set; }
		public long LPopularity { get; set; }
		public int ILevel { get; set; }
		public int IExperience { get; set; }
		public long LVIPLimitTime { get; set; }
		public int IMute { get; set; }
		public int IVibrate { get; set; }
		public string StrSmallCover { get; set; }
		public string[] SSmallAlbums { get; set; }
		public string[] SBigAlbums { get; set; }
		public long LAndroidCoin { get; set; }
		public long LAndroidPopularity { get; set; }
		public DEVICE_TYPE EDeviceType { get; set; }
		public LANGUAGE_TYPE ELanguageType { get; set; }
		public string SCountryCode { get; set; }
		public string SRemark { get; set; }
		public int IFriendInviteMute { get; set; }
		public string SShowID { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			Uuid = parser.ParseField<long>(message, "uuid");
			EType = parser.ParseEnum<ID_TYPE>(message, "eType");
			StrID = parser.ParseField<string>(message, "strID");
			StrCover = parser.ParseField<string>(message, "strCover");
			StrNick = parser.ParseField<string>(message, "strNick");
			StrPhoneNumber = parser.ParseField<string>(message, "strPhoneNumber");
			EVipType = parser.ParseEnum<VIP_TYPE>(message, "eVipType");
			IGender = parser.ParseField<int>(message, "iGender");
			LLoginTime = parser.ParseField<long>(message, "lLoginTime");
			StrDesc = parser.ParseField<string>(message, "strDesc");
			LCoin = parser.ParseField<long>(message, "lCoin");
			LPopularity = parser.ParseField<long>(message, "lPopularity");
			ILevel = parser.ParseField<int>(message, "iLevel");
			IExperience = parser.ParseField<int>(message, "iExperience");
			LVIPLimitTime = parser.ParseField<long>(message, "lVIPLimitTime");
			IMute = parser.ParseField<int>(message, "iMute");
			IVibrate = parser.ParseField<int>(message, "iVibrate");
			StrSmallCover = parser.ParseField<string>(message, "strSmallCover");
			SSmallAlbums = parser.ParseFieldRepeated<string>(message, "sSmallAlbums");
			SBigAlbums = parser.ParseFieldRepeated<string>(message, "sBigAlbums");
			LAndroidCoin = parser.ParseField<long>(message, "lAndroidCoin");
			LAndroidPopularity = parser.ParseField<long>(message, "lAndroidPopularity");
			EDeviceType = parser.ParseEnum<DEVICE_TYPE>(message, "eDeviceType");
			ELanguageType = parser.ParseEnum<LANGUAGE_TYPE>(message, "eLanguageType");
			SCountryCode = parser.ParseField<string>(message, "sCountryCode");
			SRemark = parser.ParseField<string>(message, "sRemark");
			IFriendInviteMute = parser.ParseField<int>(message, "iFriendInviteMute");
			SShowID = parser.ParseField<string>(message, "sShowID");
		}
	}
}
