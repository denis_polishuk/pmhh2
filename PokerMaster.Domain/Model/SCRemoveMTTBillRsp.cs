using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class SCRemoveMTTBillRsp : DomainModel
	{
		

		public ERROR_CODE_TYPE EErrCode { get; set; }
		public long LBillID { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			EErrCode = parser.ParseEnum<ERROR_CODE_TYPE>(message, "eErrCode");
			LBillID = parser.ParseField<long>(message, "lBillID");
		}
	}
}
