using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class CSGameAction : DomainModel
	{
		

		public long Uuid { get; set; }
		public long LRoomID { get; set; }
		public GAME_ACT_TYPE EGameActType { get; set; }
		public long LStacks { get; set; }
		public long LBettingID { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			Uuid = parser.ParseField<long>(message, "uuid");
			LRoomID = parser.ParseField<long>(message, "lRoomID");
			EGameActType = parser.ParseEnum<GAME_ACT_TYPE>(message, "eGameActType");
			LStacks = parser.ParseField<long>(message, "lStacks");
			LBettingID = parser.ParseField<long>(message, "lBettingID");
		}
	}
}
