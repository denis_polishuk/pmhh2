using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class CSKickUserGameRoom : DomainModel
	{
		

		public long Uuid { get; set; }
		public long LRoomID { get; set; }
		public long LKickOffUuid { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			Uuid = parser.ParseField<long>(message, "uuid");
			LRoomID = parser.ParseField<long>(message, "lRoomID");
			LKickOffUuid = parser.ParseField<long>(message, "lKickOffUuid");
		}
	}
}
