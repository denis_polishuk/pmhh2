using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class CSSetRaiseShortcuts : DomainModel
	{
		

		public long Uuid { get; set; }
		public RaiseShortcutsOptions[] VRaiseShortcutsOptions { get; set; }
		public GAME_ROOM_TYPE EGameRoomType { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			Uuid = parser.ParseField<long>(message, "uuid");
			VRaiseShortcutsOptions = parser.ParseMessageRepeated<RaiseShortcutsOptions>(message, "vRaiseShortcutsOptions");
			EGameRoomType = parser.ParseEnum<GAME_ROOM_TYPE>(message, "eGameRoomType");
		}
	}
}
