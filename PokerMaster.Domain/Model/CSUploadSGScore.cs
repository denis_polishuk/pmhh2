using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class CSUploadSGScore : DomainModel
	{
		

		public long Uuid { get; set; }
		public long LPopularity { get; set; }
		public int IBetTimes { get; set; }
		public int IWinTimes { get; set; }
		public int IDoubleWinTimes { get; set; }
		public long LMsg { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			Uuid = parser.ParseField<long>(message, "uuid");
			LPopularity = parser.ParseField<long>(message, "lPopularity");
			IBetTimes = parser.ParseField<int>(message, "iBetTimes");
			IWinTimes = parser.ParseField<int>(message, "iWinTimes");
			IDoubleWinTimes = parser.ParseField<int>(message, "iDoubleWinTimes");
			LMsg = parser.ParseField<long>(message, "lMsg");
		}
	}
}
