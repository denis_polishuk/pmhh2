using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class CSShowTableCards : DomainModel
	{
		

		public long Uuid { get; set; }
		public long LGameRoomID { get; set; }
		public long LGameHandID { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			Uuid = parser.ParseField<long>(message, "uuid");
			LGameRoomID = parser.ParseField<long>(message, "lGameRoomID");
			LGameHandID = parser.ParseField<long>(message, "lGameHandID");
		}
	}
}
