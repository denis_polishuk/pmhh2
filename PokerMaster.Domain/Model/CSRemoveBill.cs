using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class CSRemoveBill : DomainModel
	{
		

		public long LBillID { get; set; }
		public long Uuid { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			LBillID = parser.ParseField<long>(message, "lBillID");
			Uuid = parser.ParseField<long>(message, "uuid");
		}
	}
}
