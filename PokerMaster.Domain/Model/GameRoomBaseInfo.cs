using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class GameRoomBaseInfo : DomainModel
	{
		

		public long LGameRoomId { get; set; }
		public string StrRoomName { get; set; }
		public UserBaseInfoNet StCreateUserBaseInfo { get; set; }
		public int BPrivateRoom { get; set; }
		public long LSmallBlinds { get; set; }
		public long LBigBlinds { get; set; }
		public long LBuyIn { get; set; }
		public int BBuyIn { get; set; }
		public long LCreateTime { get; set; }
		public long LDuration { get; set; }
		public long LChatRoomID { get; set; }
		public int BStarted { get; set; }
		public int IGameRoomUserNums { get; set; }
		public string SFromMsg { get; set; }
		public CREATE_ROOM_TYPE ECreateRoomType { get; set; }
		public GAME_ROOM_SENIOR_TYPE EGameRoomSeniorType { get; set; }
		public int IMaxBuyinRatio { get; set; }
		public int IMinBuyinRatio { get; set; }
		public int BBuyinControl { get; set; }
		public int BGamePause { get; set; }
		public long LGamePauseTime { get; set; }
		public int IGameRoomUserMaxNums { get; set; }
		public int IAnte { get; set; }
		public int BInsurance { get; set; }
		public int BInGame { get; set; }
		public GAME_ROOM_TYPE EGameRoomType { get; set; }
		public long LStartTime { get; set; }
		public int BIPLimit { get; set; }
		public int BGPSLimit { get; set; }
		public long LLeagueID { get; set; }
		public string SLeagueName { get; set; }
		public string SFromClubName { get; set; }
		public string SFromClubUrl { get; set; }
		public int IThinkingInterval { get; set; }
		public int BStraddle { get; set; }
		public string SFromClubCreatorName { get; set; }
		public int ITableIndex { get; set; }
		public long LFromClubCreatorNameUuid { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			LGameRoomId = parser.ParseField<long>(message, "lGameRoomId");
			StrRoomName = parser.ParseField<string>(message, "strRoomName");
			StCreateUserBaseInfo = parser.ParseMessage<UserBaseInfoNet>(message, "stCreateUserBaseInfo");
			BPrivateRoom = parser.ParseField<int>(message, "bPrivateRoom");
			LSmallBlinds = parser.ParseField<long>(message, "lSmallBlinds");
			LBigBlinds = parser.ParseField<long>(message, "lBigBlinds");
			LBuyIn = parser.ParseField<long>(message, "lBuyIn");
			BBuyIn = parser.ParseField<int>(message, "bBuyIn");
			LCreateTime = parser.ParseField<long>(message, "lCreateTime");
			LDuration = parser.ParseField<long>(message, "lDuration");
			LChatRoomID = parser.ParseField<long>(message, "lChatRoomID");
			BStarted = parser.ParseField<int>(message, "bStarted");
			IGameRoomUserNums = parser.ParseField<int>(message, "iGameRoomUserNums");
			SFromMsg = parser.ParseField<string>(message, "sFromMsg");
			ECreateRoomType = parser.ParseEnum<CREATE_ROOM_TYPE>(message, "eCreateRoomType");
			EGameRoomSeniorType = parser.ParseEnum<GAME_ROOM_SENIOR_TYPE>(message, "eGameRoomSeniorType");
			IMaxBuyinRatio = parser.ParseField<int>(message, "iMaxBuyinRatio");
			IMinBuyinRatio = parser.ParseField<int>(message, "iMinBuyinRatio");
			BBuyinControl = parser.ParseField<int>(message, "bBuyinControl");
			BGamePause = parser.ParseField<int>(message, "bGamePause");
			LGamePauseTime = parser.ParseField<long>(message, "lGamePauseTime");
			IGameRoomUserMaxNums = parser.ParseField<int>(message, "iGameRoomUserMaxNums");
			IAnte = parser.ParseField<int>(message, "iAnte");
			BInsurance = parser.ParseField<int>(message, "bInsurance");
			BInGame = parser.ParseField<int>(message, "bInGame");
			EGameRoomType = parser.ParseEnum<GAME_ROOM_TYPE>(message, "eGameRoomType");
			LStartTime = parser.ParseField<long>(message, "lStartTime");
			BIPLimit = parser.ParseField<int>(message, "bIPLimit");
			BGPSLimit = parser.ParseField<int>(message, "bGPSLimit");
			LLeagueID = parser.ParseField<long>(message, "lLeagueID");
			SLeagueName = parser.ParseField<string>(message, "sLeagueName");
			SFromClubName = parser.ParseField<string>(message, "sFromClubName");
			SFromClubUrl = parser.ParseField<string>(message, "sFromClubUrl");
			IThinkingInterval = parser.ParseField<int>(message, "iThinkingInterval");
			BStraddle = parser.ParseField<int>(message, "bStraddle");
			SFromClubCreatorName = parser.ParseField<string>(message, "sFromClubCreatorName");
			ITableIndex = parser.ParseField<int>(message, "iTableIndex");
			LFromClubCreatorNameUuid = parser.ParseField<long>(message, "lFromClubCreatorNameUuid");
		}
	}
}
