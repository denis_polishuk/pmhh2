using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class ItemPackage : DomainModel
	{
		

		public PackageItem[] VTicketInfos { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			VTicketInfos = parser.ParseMessageRepeated<PackageItem>(message, "vTicketInfos");
		}
	}
}
