using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class Header : DomainModel
	{
		

		public int ShVer { get; set; }
		public long LCurrTime { get; set; }
		public UserBaseInfoNet StUserInfo { get; set; }
		public CmdResult StResult { get; set; }
		public string SAccessToken { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			ShVer = parser.ParseField<int>(message, "shVer");
			LCurrTime = parser.ParseField<long>(message, "lCurrTime");
			StUserInfo = parser.ParseMessage<UserBaseInfoNet>(message, "stUserInfo");
			StResult = parser.ParseMessage<CmdResult>(message, "stResult");
			SAccessToken = parser.ParseField<string>(message, "sAccessToken");
		}
	}
}
