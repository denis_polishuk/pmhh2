using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class CSInviteAction : DomainModel
	{
		

		public long Uuid { get; set; }
		public long Frienduuid { get; set; }
		public MSG_ACT EPartyAct { get; set; }
		public string SHint { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			Uuid = parser.ParseField<long>(message, "uuid");
			Frienduuid = parser.ParseField<long>(message, "frienduuid");
			EPartyAct = parser.ParseEnum<MSG_ACT>(message, "ePartyAct");
			SHint = parser.ParseField<string>(message, "sHint");
		}
	}
}
