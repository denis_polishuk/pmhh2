using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class CSSearchFriend : DomainModel
	{
		

		public long Uuid { get; set; }
		public SEARCH_TYPE ESearchType { get; set; }
		public string SSearchWord { get; set; }
		public long LSearchUuid { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			Uuid = parser.ParseField<long>(message, "uuid");
			ESearchType = parser.ParseEnum<SEARCH_TYPE>(message, "eSearchType");
			SSearchWord = parser.ParseField<string>(message, "sSearchWord");
			LSearchUuid = parser.ParseField<long>(message, "lSearchUuid");
		}
	}
}
