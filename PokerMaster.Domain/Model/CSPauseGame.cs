using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class CSPauseGame : DomainModel
	{
		

		public long Uuid { get; set; }
		public long LGameRoomID { get; set; }
		public int BPauseGame { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			Uuid = parser.ParseField<long>(message, "uuid");
			LGameRoomID = parser.ParseField<long>(message, "lGameRoomID");
			BPauseGame = parser.ParseField<int>(message, "bPauseGame");
		}
	}
}
