using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class MTTBaseBillInfoNet : DomainModel
	{
		

		public long LBillID { get; set; }
		public int BOwnerDeleted { get; set; }
		public string SMTTGameName { get; set; }
		public long LCreateTime { get; set; }
		public string STotalBounds { get; set; }
		public int ITotalPlayerNums { get; set; }
		public int IBounsLimit { get; set; }
		public long LMTTSignupFee { get; set; }
		public long LFee { get; set; }
		public MTT_TYPE EMTTType { get; set; }
		public int BRebuy { get; set; }
		public string SBillUrl { get; set; }
		public MTTUserBillInfoNet StSelfMTTUserBillInfo { get; set; }
		public MTTUserBillInfoNet[] VTopThreeUserBillInfos { get; set; }
		public int BSatellite { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			LBillID = parser.ParseField<long>(message, "lBillID");
			BOwnerDeleted = parser.ParseField<int>(message, "bOwnerDeleted");
			SMTTGameName = parser.ParseField<string>(message, "sMTTGameName");
			LCreateTime = parser.ParseField<long>(message, "lCreateTime");
			STotalBounds = parser.ParseField<string>(message, "sTotalBounds");
			ITotalPlayerNums = parser.ParseField<int>(message, "iTotalPlayerNums");
			IBounsLimit = parser.ParseField<int>(message, "iBounsLimit");
			LMTTSignupFee = parser.ParseField<long>(message, "lMTTSignupFee");
			LFee = parser.ParseField<long>(message, "lFee");
			EMTTType = parser.ParseEnum<MTT_TYPE>(message, "eMTTType");
			BRebuy = parser.ParseField<int>(message, "bRebuy");
			SBillUrl = parser.ParseField<string>(message, "sBillUrl");
			StSelfMTTUserBillInfo = parser.ParseMessage<MTTUserBillInfoNet>(message, "stSelfMTTUserBillInfo");
			VTopThreeUserBillInfos = parser.ParseMessageRepeated<MTTUserBillInfoNet>(message, "vTopThreeUserBillInfos");
			BSatellite = parser.ParseField<int>(message, "bSatellite");
		}
	}
}
