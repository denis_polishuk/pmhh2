using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class CSResetBuyinRatio : DomainModel
	{
		

		public long Uuid { get; set; }
		public long LRoomID { get; set; }
		public int IMaxBuyinRatio { get; set; }
		public int IMinBuyinRatio { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			Uuid = parser.ParseField<long>(message, "uuid");
			LRoomID = parser.ParseField<long>(message, "lRoomID");
			IMaxBuyinRatio = parser.ParseField<int>(message, "iMaxBuyinRatio");
			IMinBuyinRatio = parser.ParseField<int>(message, "iMinBuyinRatio");
		}
	}
}
