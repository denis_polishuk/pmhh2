using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class SCSetUserInfoRsp : DomainModel
	{
		

		public ERROR_CODE_TYPE EErrCode { get; set; }
		public string SNickname { get; set; }
		public int IGender { get; set; }
		public string SIconUrl { get; set; }
		public string SPhhoneNumber { get; set; }
		public string SDesc { get; set; }
		public int BMute { get; set; }
		public int BVibrate { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			EErrCode = parser.ParseEnum<ERROR_CODE_TYPE>(message, "eErrCode");
			SNickname = parser.ParseField<string>(message, "sNickname");
			IGender = parser.ParseField<int>(message, "iGender");
			SIconUrl = parser.ParseField<string>(message, "sIconUrl");
			SPhhoneNumber = parser.ParseField<string>(message, "sPhhoneNumber");
			SDesc = parser.ParseField<string>(message, "sDesc");
			BMute = parser.ParseField<int>(message, "bMute");
			BVibrate = parser.ParseField<int>(message, "bVibrate");
		}
	}
}
