using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class ChatRoomInfo : DomainModel
	{
		

		public ChatRoomBaseInfo StChatRoomBaseInfo { get; set; }
		public UserBaseInfoNet[] VUserBaseInfos { get; set; }
		public GameRoomBaseInfo[] VGameRoomBaseInfos { get; set; }
		public SNGGameRoomBaseInfo[] VGSNGameRoomBaseInfos { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			StChatRoomBaseInfo = parser.ParseMessage<ChatRoomBaseInfo>(message, "stChatRoomBaseInfo");
			VUserBaseInfos = parser.ParseMessageRepeated<UserBaseInfoNet>(message, "vUserBaseInfos");
			VGameRoomBaseInfos = parser.ParseMessageRepeated<GameRoomBaseInfo>(message, "vGameRoomBaseInfos");
			VGSNGameRoomBaseInfos = parser.ParseMessageRepeated<SNGGameRoomBaseInfo>(message, "vGSNGameRoomBaseInfos");
		}
	}
}
