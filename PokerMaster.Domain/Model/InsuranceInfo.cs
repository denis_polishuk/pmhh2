using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class InsuranceInfo : DomainModel
	{
		

		public InsuranceUserInfo[] VInsuranceUserInfos { get; set; }
		public int IFirstInsurance { get; set; }
		public long[] VNoOutsUuids { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			VInsuranceUserInfos = parser.ParseMessageRepeated<InsuranceUserInfo>(message, "vInsuranceUserInfos");
			IFirstInsurance = parser.ParseField<int>(message, "iFirstInsurance");
			VNoOutsUuids = parser.ParseFieldRepeated<long>(message, "vNoOutsUuids");
		}
	}
}
