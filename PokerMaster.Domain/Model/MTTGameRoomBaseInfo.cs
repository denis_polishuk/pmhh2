using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class MTTGameRoomBaseInfo : DomainModel
	{
		

		public long LMTTGameRoomID { get; set; }
		public string SMTTTitle { get; set; }
		public string EMTTBGImgUrl { get; set; }
		public long LCreateTime { get; set; }
		public long LStartTime { get; set; }
		public long LRegistrationFee { get; set; }
		public long LServiceFee { get; set; }
		public int BReentry { get; set; }
		public int BSatellite { get; set; }
		public int ICurrentSigupNums { get; set; }
		public int IPlayingNums { get; set; }
		public int IMinSigupNums { get; set; }
		public int IMaxSigupNums { get; set; }
		public int BInGame { get; set; }
		public MTT_GAME_STATUS EMTTGameStatus { get; set; }
		public int IDelayLevel { get; set; }
		public int ICurrentBlindLevel { get; set; }
		public MTT_GAME_TYPE EMTTGameType { get; set; }
		public MTT_SIGNUP_FEE_TYPE ESignUpFeeType { get; set; }
		public MTT_TYPE EMTTType { get; set; }
		public int IReentryableNum { get; set; }
		public int IReentryMinLevel { get; set; }
		public int IReentryMaxLevel { get; set; }
		public int ICurrentReentryNum { get; set; }
		public long LDelayTime { get; set; }
		public int IEliminated { get; set; }
		public int BSyncShuffle { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			LMTTGameRoomID = parser.ParseField<long>(message, "lMTTGameRoomID");
			SMTTTitle = parser.ParseField<string>(message, "sMTTTitle");
			EMTTBGImgUrl = parser.ParseField<string>(message, "eMTTBGImgUrl");
			LCreateTime = parser.ParseField<long>(message, "lCreateTime");
			LStartTime = parser.ParseField<long>(message, "lStartTime");
			LRegistrationFee = parser.ParseField<long>(message, "lRegistrationFee");
			LServiceFee = parser.ParseField<long>(message, "lServiceFee");
			BReentry = parser.ParseField<int>(message, "bReentry");
			BSatellite = parser.ParseField<int>(message, "bSatellite");
			ICurrentSigupNums = parser.ParseField<int>(message, "iCurrentSigupNums");
			IPlayingNums = parser.ParseField<int>(message, "iPlayingNums");
			IMinSigupNums = parser.ParseField<int>(message, "iMinSigupNums");
			IMaxSigupNums = parser.ParseField<int>(message, "iMaxSigupNums");
			BInGame = parser.ParseField<int>(message, "bInGame");
			EMTTGameStatus = parser.ParseEnum<MTT_GAME_STATUS>(message, "eMTTGameStatus");
			IDelayLevel = parser.ParseField<int>(message, "iDelayLevel");
			ICurrentBlindLevel = parser.ParseField<int>(message, "iCurrentBlindLevel");
			EMTTGameType = parser.ParseEnum<MTT_GAME_TYPE>(message, "eMTTGameType");
			ESignUpFeeType = parser.ParseEnum<MTT_SIGNUP_FEE_TYPE>(message, "eSignUpFeeType");
			EMTTType = parser.ParseEnum<MTT_TYPE>(message, "eMTTType");
			IReentryableNum = parser.ParseField<int>(message, "iReentryableNum");
			IReentryMinLevel = parser.ParseField<int>(message, "iReentryMinLevel");
			IReentryMaxLevel = parser.ParseField<int>(message, "iReentryMaxLevel");
			ICurrentReentryNum = parser.ParseField<int>(message, "iCurrentReentryNum");
			LDelayTime = parser.ParseField<long>(message, "lDelayTime");
			IEliminated = parser.ParseField<int>(message, "iEliminated");
			BSyncShuffle = parser.ParseField<int>(message, "bSyncShuffle");
		}
	}
}
