using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class GameResultInfo : DomainModel
	{
		

		public GamePotResultInfo[] StGamePotResultInfo { get; set; }
		public long[] VlOriginalUserStacks { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			StGamePotResultInfo = parser.ParseMessageRepeated<GamePotResultInfo>(message, "stGamePotResultInfo");
			VlOriginalUserStacks = parser.ParseFieldRepeated<long>(message, "vlOriginalUserStacks");
		}
	}
}
