using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class GameItemInfo : DomainModel
	{
		

		public int IItemID { get; set; }
		public string SItemName { get; set; }
		public string SItemIcon { get; set; }
		public int IItemType { get; set; }
		public int IItemNum { get; set; }
		public string SItemDescription { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			IItemID = parser.ParseField<int>(message, "iItemID");
			SItemName = parser.ParseField<string>(message, "sItemName");
			SItemIcon = parser.ParseField<string>(message, "sItemIcon");
			IItemType = parser.ParseField<int>(message, "iItemType");
			IItemNum = parser.ParseField<int>(message, "iItemNum");
			SItemDescription = parser.ParseField<string>(message, "sItemDescription");
		}
	}
}
