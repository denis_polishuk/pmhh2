using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class ShareMTTBillInfoNet : DomainModel
	{
		

		public MTTBaseBillInfoNet StMTTBaseBillInfoNet { get; set; }
		public long LShareUuid { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			StMTTBaseBillInfoNet = parser.ParseMessage<MTTBaseBillInfoNet>(message, "stMTTBaseBillInfoNet");
			LShareUuid = parser.ParseField<long>(message, "lShareUuid");
		}
	}
}
