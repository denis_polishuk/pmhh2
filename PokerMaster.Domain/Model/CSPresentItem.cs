using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class CSPresentItem : DomainModel
	{
		

		public long LFromUuid { get; set; }
		public string LToUuidStr { get; set; }
		public PackageItem StPackageItem { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			LFromUuid = parser.ParseField<long>(message, "lFromUuid");
			LToUuidStr = parser.ParseField<string>(message, "lToUuidStr");
			StPackageItem = parser.ParseMessage<PackageItem>(message, "stPackageItem");
		}
	}
}
