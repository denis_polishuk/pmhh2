using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class CSGetBillInfoByBillIDV3 : DomainModel
	{
		

		public long Uuid { get; set; }
		public long LBillID { get; set; }
		public int IShareFlag { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			Uuid = parser.ParseField<long>(message, "uuid");
			LBillID = parser.ParseField<long>(message, "lBillID");
			IShareFlag = parser.ParseField<int>(message, "iShareFlag");
		}
	}
}
