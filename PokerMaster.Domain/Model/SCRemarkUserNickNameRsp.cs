using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class SCRemarkUserNickNameRsp : DomainModel
	{
		

		public ERROR_CODE_TYPE EErrCode { get; set; }
		public long RemarkUuid { get; set; }
		public string SRemarContent { get; set; }
		public long LGameRoomID { get; set; }
		public string SAlias { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			EErrCode = parser.ParseEnum<ERROR_CODE_TYPE>(message, "eErrCode");
			RemarkUuid = parser.ParseField<long>(message, "remarkUuid");
			SRemarContent = parser.ParseField<string>(message, "sRemarContent");
			LGameRoomID = parser.ParseField<long>(message, "lGameRoomID");
			SAlias = parser.ParseField<string>(message, "sAlias");
		}
	}
}
