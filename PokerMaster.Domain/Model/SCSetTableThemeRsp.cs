using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class SCSetTableThemeRsp : DomainModel
	{
		

		public ERROR_CODE_TYPE EErrCode { get; set; }
		public long Uuid { get; set; }
		public int IOrdinal { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			EErrCode = parser.ParseEnum<ERROR_CODE_TYPE>(message, "eErrCode");
			Uuid = parser.ParseField<long>(message, "uuid");
			IOrdinal = parser.ParseField<int>(message, "iOrdinal");
		}
	}
}
