using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class PokerSection : DomainModel
	{
		

		public int[] VSectionPubCards { get; set; }
		public int IPlayerNum { get; set; }
		public ActionInfo[] VActionInfos { get; set; }
		public long LPot { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			VSectionPubCards = parser.ParseFieldRepeated<int>(message, "vSectionPubCards");
			IPlayerNum = parser.ParseField<int>(message, "iPlayerNum");
			VActionInfos = parser.ParseMessageRepeated<ActionInfo>(message, "vActionInfos");
			LPot = parser.ParseField<long>(message, "lPot");
		}
	}
}
