using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class BuyinControl : DomainModel
	{
		

		public UserBaseInfoNet StUserBaseInfo { get; set; }
		public long LStacks { get; set; }
		public long LBuyinCreateTime { get; set; }
		public long LGameRoomID { get; set; }
		public string SGameRoomName { get; set; }
		public GAME_ROOM_TYPE EGameRoomType { get; set; }
		public long LClubID { get; set; }
		public string SClubName { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			StUserBaseInfo = parser.ParseMessage<UserBaseInfoNet>(message, "stUserBaseInfo");
			LStacks = parser.ParseField<long>(message, "lStacks");
			LBuyinCreateTime = parser.ParseField<long>(message, "lBuyinCreateTime");
			LGameRoomID = parser.ParseField<long>(message, "lGameRoomID");
			SGameRoomName = parser.ParseField<string>(message, "sGameRoomName");
			EGameRoomType = parser.ParseEnum<GAME_ROOM_TYPE>(message, "eGameRoomType");
			LClubID = parser.ParseField<long>(message, "lClubID");
			SClubName = parser.ParseField<string>(message, "sClubName");
		}
	}
}
