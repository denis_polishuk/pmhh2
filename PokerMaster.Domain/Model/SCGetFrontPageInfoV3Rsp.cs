using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class SCGetFrontPageInfoV3Rsp : DomainModel
	{
		

		public ERROR_CODE_TYPE EErrCode { get; set; }
		public GameRoomBaseInfo[] VGameRoomBaseInfos { get; set; }
		public SNGGameRoomBaseInfo[] VSNGGameRoomBaseInfos { get; set; }
		public QuickGameRoomInfo[] VQuickGameRoomInfos { get; set; }
		public int IOffset { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			EErrCode = parser.ParseEnum<ERROR_CODE_TYPE>(message, "eErrCode");
			VGameRoomBaseInfos = parser.ParseMessageRepeated<GameRoomBaseInfo>(message, "vGameRoomBaseInfos");
			VSNGGameRoomBaseInfos = parser.ParseMessageRepeated<SNGGameRoomBaseInfo>(message, "vSNGGameRoomBaseInfos");
			VQuickGameRoomInfos = parser.ParseMessageRepeated<QuickGameRoomInfo>(message, "vQuickGameRoomInfos");
			IOffset = parser.ParseField<int>(message, "iOffset");
		}
	}
}
