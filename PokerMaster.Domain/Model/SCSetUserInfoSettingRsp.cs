using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class SCSetUserInfoSettingRsp : DomainModel
	{
		

		public ERROR_CODE_TYPE EErrCode { get; set; }
		public long Uuid { get; set; }
		public int IMute { get; set; }
		public int IVibrate { get; set; }
		public int IFriendInviteMute { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			EErrCode = parser.ParseEnum<ERROR_CODE_TYPE>(message, "eErrCode");
			Uuid = parser.ParseField<long>(message, "uuid");
			IMute = parser.ParseField<int>(message, "iMute");
			IVibrate = parser.ParseField<int>(message, "iVibrate");
			IFriendInviteMute = parser.ParseField<int>(message, "iFriendInviteMute");
		}
	}
}
