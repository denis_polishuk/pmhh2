using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class CSUploadFriendRelation : DomainModel
	{
		

		public long Uuid { get; set; }
		public UPLOAD_FRIEND_RELATION_TYPE EUploadType { get; set; }
		public UploadUserInfoNet[] VFriendsInfo { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			Uuid = parser.ParseField<long>(message, "uuid");
			EUploadType = parser.ParseEnum<UPLOAD_FRIEND_RELATION_TYPE>(message, "eUploadType");
			VFriendsInfo = parser.ParseMessageRepeated<UploadUserInfoNet>(message, "vFriendsInfo");
		}
	}
}
