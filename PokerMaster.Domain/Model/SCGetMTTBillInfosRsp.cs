using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class SCGetMTTBillInfosRsp : DomainModel
	{
		

		public ERROR_CODE_TYPE EErrCode { get; set; }
		public MTTBaseBillInfoNet[] VMTTBaseBillInfoNets { get; set; }
		public int IUnReadNum { get; set; }
		public long LNewestTime { get; set; }
		public UserStatisticsInfoNetTournaments StUserStatisticsInfoNetTournaments { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			EErrCode = parser.ParseEnum<ERROR_CODE_TYPE>(message, "eErrCode");
			VMTTBaseBillInfoNets = parser.ParseMessageRepeated<MTTBaseBillInfoNet>(message, "vMTTBaseBillInfoNets");
			IUnReadNum = parser.ParseField<int>(message, "iUnReadNum");
			LNewestTime = parser.ParseField<long>(message, "lNewestTime");
			StUserStatisticsInfoNetTournaments = parser.ParseMessage<UserStatisticsInfoNetTournaments>(message, "stUserStatisticsInfoNetTournaments");
		}
	}
}
