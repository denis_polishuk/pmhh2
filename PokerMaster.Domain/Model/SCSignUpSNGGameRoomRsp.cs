using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class SCSignUpSNGGameRoomRsp : DomainModel
	{
		

		public ERROR_CODE_TYPE EErrCode { get; set; }
		public GameRoomInfo StGameRoomInfo { get; set; }
		public UserBaseInfoNet StUserBaseInfoNet { get; set; }
		public long LGameRoomID { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			EErrCode = parser.ParseEnum<ERROR_CODE_TYPE>(message, "eErrCode");
			StGameRoomInfo = parser.ParseMessage<GameRoomInfo>(message, "stGameRoomInfo");
			StUserBaseInfoNet = parser.ParseMessage<UserBaseInfoNet>(message, "stUserBaseInfoNet");
			LGameRoomID = parser.ParseField<long>(message, "lGameRoomID");
		}
	}
}
