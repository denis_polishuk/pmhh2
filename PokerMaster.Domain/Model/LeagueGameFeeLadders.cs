using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class LeagueGameFeeLadders : DomainModel
	{
		

		public int IGameRoomUserMaxNums { get; set; }
		public LeagueGameFee[] VLeagueGameFees { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			IGameRoomUserMaxNums = parser.ParseField<int>(message, "iGameRoomUserMaxNums");
			VLeagueGameFees = parser.ParseMessageRepeated<LeagueGameFee>(message, "vLeagueGameFees");
		}
	}
}
