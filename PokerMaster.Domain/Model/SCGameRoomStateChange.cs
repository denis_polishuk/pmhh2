using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class SCGameRoomStateChange : DomainModel
	{
		

		public GameRoomInfo StGameRoomInfo { get; set; }
		public int[] VGivenCards { get; set; }
		public int[] VGivenHands { get; set; }
		public long Uuid { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			StGameRoomInfo = parser.ParseMessage<GameRoomInfo>(message, "stGameRoomInfo");
			VGivenCards = parser.ParseFieldRepeated<int>(message, "vGivenCards");
			VGivenHands = parser.ParseFieldRepeated<int>(message, "vGivenHands");
			Uuid = parser.ParseField<long>(message, "uuid");
		}
	}
}
