using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class SCGetAllMyAliasDataRsp : DomainModel
	{
		

		public ERROR_CODE_TYPE EErrCode { get; set; }
		public MyAliasData[] VMyAliasData { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			EErrCode = parser.ParseEnum<ERROR_CODE_TYPE>(message, "eErrCode");
			VMyAliasData = parser.ParseMessageRepeated<MyAliasData>(message, "vMyAliasData");
		}
	}
}
