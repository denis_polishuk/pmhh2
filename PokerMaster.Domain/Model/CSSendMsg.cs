using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class CSSendMsg : DomainModel
	{
		

		public long Uuid { get; set; }
		public long LTalkwith { get; set; }
		public CHAT_TYPE ETalkType { get; set; }
		public CHAT_MESSAGE_TYPE EChatMessageType { get; set; }
		public string SMsg { get; set; }
		public byte[] VVoiceDatas { get; set; }
		public ShareBillInfoNet StShareBillInfo { get; set; }
		public PokerProcessInfo StPokerProcessBaseInfo { get; set; }
		public PicPackageInfoNet StPicPackageInfo { get; set; }
		public long LMsgId { get; set; }
		public long LDuration { get; set; }
		public NetPageShareInfoNet StNetPageShareInfoNet { get; set; }
		public SharePokerHistoryInfo StSharePokerHistoryInfo { get; set; }
		public string[] VExtras { get; set; }
		public ShareMTTBillInfoNet StShareMTTBillInfoNet { get; set; }
		public SharePokerHistoryInfo StShareRewardingInfo { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			Uuid = parser.ParseField<long>(message, "uuid");
			LTalkwith = parser.ParseField<long>(message, "lTalkwith");
			ETalkType = parser.ParseEnum<CHAT_TYPE>(message, "eTalkType");
			EChatMessageType = parser.ParseEnum<CHAT_MESSAGE_TYPE>(message, "eChatMessageType");
			SMsg = parser.ParseField<string>(message, "sMsg");
			VVoiceDatas = parser.ParseByteArray(message, "vVoiceDatas");
			StShareBillInfo = parser.ParseMessage<ShareBillInfoNet>(message, "stShareBillInfo");
			StPokerProcessBaseInfo = parser.ParseMessage<PokerProcessInfo>(message, "stPokerProcessBaseInfo");
			StPicPackageInfo = parser.ParseMessage<PicPackageInfoNet>(message, "stPicPackageInfo");
			LMsgId = parser.ParseField<long>(message, "lMsgId");
			LDuration = parser.ParseField<long>(message, "lDuration");
			StNetPageShareInfoNet = parser.ParseMessage<NetPageShareInfoNet>(message, "stNetPageShareInfoNet");
			StSharePokerHistoryInfo = parser.ParseMessage<SharePokerHistoryInfo>(message, "stSharePokerHistoryInfo");
			VExtras = parser.ParseFieldRepeated<string>(message, "vExtras");
			StShareMTTBillInfoNet = parser.ParseMessage<ShareMTTBillInfoNet>(message, "stShareMTTBillInfoNet");
			StShareRewardingInfo = parser.ParseMessage<SharePokerHistoryInfo>(message, "stShareRewardingInfo");
		}
	}
}
