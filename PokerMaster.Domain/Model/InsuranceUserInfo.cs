using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class InsuranceUserInfo : DomainModel
	{
		

		public long Uuid { get; set; }
		public INSURANCE_BET_STATUS EInsuranceBetStatus { get; set; }
		public InsurancePotInfo[] VInsurancePotInfos { get; set; }
		public long LTotalGetStacks { get; set; }
		public int BCanInsurance { get; set; }
		public int BDefaultInsurance { get; set; }
		public long LInsuranceStartTime { get; set; }
		public long LInsuranceActDuration { get; set; }
		public int IInsuranceDelayCost { get; set; }
		public int IInsuranceDelayLong { get; set; }
		public int BFirstInsurance { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			Uuid = parser.ParseField<long>(message, "uuid");
			EInsuranceBetStatus = parser.ParseEnum<INSURANCE_BET_STATUS>(message, "eInsuranceBetStatus");
			VInsurancePotInfos = parser.ParseMessageRepeated<InsurancePotInfo>(message, "vInsurancePotInfos");
			LTotalGetStacks = parser.ParseField<long>(message, "lTotalGetStacks");
			BCanInsurance = parser.ParseField<int>(message, "bCanInsurance");
			BDefaultInsurance = parser.ParseField<int>(message, "bDefaultInsurance");
			LInsuranceStartTime = parser.ParseField<long>(message, "lInsuranceStartTime");
			LInsuranceActDuration = parser.ParseField<long>(message, "lInsuranceActDuration");
			IInsuranceDelayCost = parser.ParseField<int>(message, "iInsuranceDelayCost");
			IInsuranceDelayLong = parser.ParseField<int>(message, "iInsuranceDelayLong");
			BFirstInsurance = parser.ParseField<int>(message, "bFirstInsurance");
		}
	}
}
