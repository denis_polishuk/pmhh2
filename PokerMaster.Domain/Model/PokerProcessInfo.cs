using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class PokerProcessInfo : DomainModel
	{
		

		public long LPokerProcessID { get; set; }
		public string StrPokerProcessUrl { get; set; }
		public UserBaseInfoNet StSaveUserBaseInfo { get; set; }
		public string StrRoomName { get; set; }
		public long LBigBlind { get; set; }
		public long LSmallBlind { get; set; }
		public long LWinStacks { get; set; }
		public UserBaseInfoNet StShareUser { get; set; }
		public int[] VHandCards { get; set; }
		public long LSaveTime { get; set; }
		public int IAnte { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			LPokerProcessID = parser.ParseField<long>(message, "lPokerProcessID");
			StrPokerProcessUrl = parser.ParseField<string>(message, "strPokerProcessUrl");
			StSaveUserBaseInfo = parser.ParseMessage<UserBaseInfoNet>(message, "stSaveUserBaseInfo");
			StrRoomName = parser.ParseField<string>(message, "strRoomName");
			LBigBlind = parser.ParseField<long>(message, "lBigBlind");
			LSmallBlind = parser.ParseField<long>(message, "lSmallBlind");
			LWinStacks = parser.ParseField<long>(message, "lWinStacks");
			StShareUser = parser.ParseMessage<UserBaseInfoNet>(message, "stShareUser");
			VHandCards = parser.ParseFieldRepeated<int>(message, "vHandCards");
			LSaveTime = parser.ParseField<long>(message, "lSaveTime");
			IAnte = parser.ParseField<int>(message, "iAnte");
		}
	}
}
