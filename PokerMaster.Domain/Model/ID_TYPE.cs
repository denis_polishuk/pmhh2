namespace PokerMaster.Domain.Model
{
	public enum ID_TYPE
	{
		ID_TYPE_SINAWEIBO = 1,
		ID_TYPE_QQWEIBO = 2,
		ID_TYPE_RENREN = 3,
		ID_TYPE_PHONE = 4,
		ID_TYPE_NA = 5,
		ID_TYPE_FACEBOOK = 6
	}
}
