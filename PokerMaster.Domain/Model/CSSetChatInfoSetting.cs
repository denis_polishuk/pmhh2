using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class CSSetChatInfoSetting : DomainModel
	{
		

		public long Uuid { get; set; }
		public CHAT_TYPE EChatType { get; set; }
		public long LRoomID { get; set; }
		public long ChatUuid { get; set; }
		public int IMute { get; set; }
		public int IVibrate { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			Uuid = parser.ParseField<long>(message, "uuid");
			EChatType = parser.ParseEnum<CHAT_TYPE>(message, "eChatType");
			LRoomID = parser.ParseField<long>(message, "lRoomID");
			ChatUuid = parser.ParseField<long>(message, "chatUuid");
			IMute = parser.ParseField<int>(message, "iMute");
			IVibrate = parser.ParseField<int>(message, "iVibrate");
		}
	}
}
