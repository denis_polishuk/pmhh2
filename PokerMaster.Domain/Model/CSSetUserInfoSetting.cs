using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class CSSetUserInfoSetting : DomainModel
	{
		

		public long Uuid { get; set; }
		public int IMute { get; set; }
		public int IVibrate { get; set; }
		public int IFriendInviteMute { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			Uuid = parser.ParseField<long>(message, "uuid");
			IMute = parser.ParseField<int>(message, "iMute");
			IVibrate = parser.ParseField<int>(message, "iVibrate");
			IFriendInviteMute = parser.ParseField<int>(message, "iFriendInviteMute");
		}
	}
}
