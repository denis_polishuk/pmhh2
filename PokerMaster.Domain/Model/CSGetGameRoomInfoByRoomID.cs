using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class CSGetGameRoomInfoByRoomID : DomainModel
	{
		

		public long LRoomID { get; set; }
		public long Uuid { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			LRoomID = parser.ParseField<long>(message, "lRoomID");
			Uuid = parser.ParseField<long>(message, "uuid");
		}
	}
}
