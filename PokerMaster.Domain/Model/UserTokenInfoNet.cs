using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class UserTokenInfoNet : DomainModel
	{
		

		public long Uuid { get; set; }
		public string StrAPNToken { get; set; }
		public string StrSSOToken { get; set; }
		public string StrAndroidXingeAPNToken { get; set; }
		public string StrAndroidFCMAPNToken { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			Uuid = parser.ParseField<long>(message, "uuid");
			StrAPNToken = parser.ParseField<string>(message, "strAPNToken");
			StrSSOToken = parser.ParseField<string>(message, "strSSOToken");
			StrAndroidXingeAPNToken = parser.ParseField<string>(message, "strAndroidXingeAPNToken");
			StrAndroidFCMAPNToken = parser.ParseField<string>(message, "strAndroidFCMAPNToken");
		}
	}
}
