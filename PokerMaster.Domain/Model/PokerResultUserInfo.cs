using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class PokerResultUserInfo : DomainModel
	{
		

		public long Uuid { get; set; }
		public long LWinStacks { get; set; }
		public int BFlod { get; set; }
		public int[] VGivenHands { get; set; }
		public int[] BShowCard { get; set; }
		public long LBuyInsuranceStacks { get; set; }
		public long LBuyGetStacks { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			Uuid = parser.ParseField<long>(message, "uuid");
			LWinStacks = parser.ParseField<long>(message, "lWinStacks");
			BFlod = parser.ParseField<int>(message, "bFlod");
			VGivenHands = parser.ParseFieldRepeated<int>(message, "vGivenHands");
			BShowCard = parser.ParseFieldRepeated<int>(message, "bShowCard");
			LBuyInsuranceStacks = parser.ParseField<long>(message, "lBuyInsuranceStacks");
			LBuyGetStacks = parser.ParseField<long>(message, "lBuyGetStacks");
		}
	}
}
