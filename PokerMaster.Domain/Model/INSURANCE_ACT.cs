namespace PokerMaster.Domain.Model
{
	public enum INSURANCE_ACT
	{
		INSURANCE_ACT_BET = 1,
		INSURANCE_ACT_FOLD = 2
	}
}
