using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class CSGetUserBaseInfo : DomainModel
	{
		

		public long Uuid { get; set; }
		public long LGetUuid { get; set; }
		public int BRemark { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			Uuid = parser.ParseField<long>(message, "uuid");
			LGetUuid = parser.ParseField<long>(message, "lGetUuid");
			BRemark = parser.ParseField<int>(message, "bRemark");
		}
	}
}
