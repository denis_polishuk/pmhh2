using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class SCSearchFriendRsp : DomainModel
	{
		

		public ERROR_CODE_TYPE EErrCode { get; set; }
		public UserFriendInfoNet[] VUserFriendIfos { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			EErrCode = parser.ParseEnum<ERROR_CODE_TYPE>(message, "eErrCode");
			VUserFriendIfos = parser.ParseMessageRepeated<UserFriendInfoNet>(message, "vUserFriendIfos");
		}
	}
}
