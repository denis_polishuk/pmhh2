using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class CSGetChatRoomInfoByUuid : DomainModel
	{
		

		public long Uuid { get; set; }
		public int LStartIndex { get; set; }
		public int LNum { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			Uuid = parser.ParseField<long>(message, "uuid");
			LStartIndex = parser.ParseField<int>(message, "lStartIndex");
			LNum = parser.ParseField<int>(message, "lNum");
		}
	}
}
