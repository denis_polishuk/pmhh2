using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class CSRegister : DomainModel
	{
		

		public UserBaseInfoNet StUserBaseInfo { get; set; }
		public string Pwd { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			StUserBaseInfo = parser.ParseMessage<UserBaseInfoNet>(message, "stUserBaseInfo");
			Pwd = parser.ParseField<string>(message, "pwd");
		}
	}
}
