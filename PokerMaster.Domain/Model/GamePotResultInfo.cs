using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class GamePotResultInfo : DomainModel
	{
		

		public UserBaseInfoNet[] StUserWiner { get; set; }
		public long[] LUserPots { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			StUserWiner = parser.ParseMessageRepeated<UserBaseInfoNet>(message, "stUserWiner");
			LUserPots = parser.ParseFieldRepeated<long>(message, "lUserPots");
		}
	}
}
