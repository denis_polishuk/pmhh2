using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class ClubBaseInfo : DomainModel
	{
		

		public long LClubID { get; set; }
		public UserBaseInfoNet StCreateUserBaseInfo { get; set; }
		public string SClubName { get; set; }
		public string SClubLocation { get; set; }
		public string[] SSmallAlbums { get; set; }
		public string[] SBigAlbums { get; set; }
		public long LFund { get; set; }
		public string SSmallIcon { get; set; }
		public string SBigIcon { get; set; }
		public string SDesc { get; set; }
		public int BClubMsgMute { get; set; }
		public int BClubMsgVibrate { get; set; }
		public int BVerify { get; set; }
		public int IClubLevel { get; set; }
		public int IClubExperience { get; set; }
		public long LCreateTime { get; set; }
		public int IMaxMembers { get; set; }
		public int ICurMembers { get; set; }
		public long LClubScore { get; set; }
		public int IManagerCreate { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			LClubID = parser.ParseField<long>(message, "lClubID");
			StCreateUserBaseInfo = parser.ParseMessage<UserBaseInfoNet>(message, "stCreateUserBaseInfo");
			SClubName = parser.ParseField<string>(message, "sClubName");
			SClubLocation = parser.ParseField<string>(message, "sClubLocation");
			SSmallAlbums = parser.ParseFieldRepeated<string>(message, "sSmallAlbums");
			SBigAlbums = parser.ParseFieldRepeated<string>(message, "sBigAlbums");
			LFund = parser.ParseField<long>(message, "lFund");
			SSmallIcon = parser.ParseField<string>(message, "sSmallIcon");
			SBigIcon = parser.ParseField<string>(message, "sBigIcon");
			SDesc = parser.ParseField<string>(message, "sDesc");
			BClubMsgMute = parser.ParseField<int>(message, "bClubMsgMute");
			BClubMsgVibrate = parser.ParseField<int>(message, "bClubMsgVibrate");
			BVerify = parser.ParseField<int>(message, "bVerify");
			IClubLevel = parser.ParseField<int>(message, "iClubLevel");
			IClubExperience = parser.ParseField<int>(message, "iClubExperience");
			LCreateTime = parser.ParseField<long>(message, "lCreateTime");
			IMaxMembers = parser.ParseField<int>(message, "iMaxMembers");
			ICurMembers = parser.ParseField<int>(message, "iCurMembers");
			LClubScore = parser.ParseField<long>(message, "lClubScore");
			IManagerCreate = parser.ParseField<int>(message, "iManagerCreate");
		}
	}
}
