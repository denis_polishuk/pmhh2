using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class SCBuyInsuranceDelayRsp : DomainModel
	{
		

		public ERROR_CODE_TYPE EErrCode { get; set; }
		public long LGameRoomID { get; set; }
		public InsuranceUserInfo StInsuranceUserInfo { get; set; }
		public long LCoin { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			EErrCode = parser.ParseEnum<ERROR_CODE_TYPE>(message, "eErrCode");
			LGameRoomID = parser.ParseField<long>(message, "lGameRoomID");
			StInsuranceUserInfo = parser.ParseMessage<InsuranceUserInfo>(message, "stInsuranceUserInfo");
			LCoin = parser.ParseField<long>(message, "lCoin");
		}
	}
}
