using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class CSGetFriendStatus : DomainModel
	{
		

		public long Uuid { get; set; }
		public USER_FRIEND_STATE EStatus { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			Uuid = parser.ParseField<long>(message, "uuid");
			EStatus = parser.ParseEnum<USER_FRIEND_STATE>(message, "eStatus");
		}
	}
}
