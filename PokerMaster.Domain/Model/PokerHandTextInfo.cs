using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class PokerHandTextInfo : DomainModel
	{
		

		public long LRoomId { get; set; }
		public long LHandId { get; set; }
		public string SRoomName { get; set; }
		public long LSb { get; set; }
		public long LBb { get; set; }
		public long LAnte { get; set; }
		public int IPlayerNum { get; set; }
		public int[] VHoldCards { get; set; }
		public string SLocation { get; set; }
		public int IIsIns { get; set; }
		public long LInsPot { get; set; }
		public PokerSection StPreflop { get; set; }
		public PokerSection StFlop { get; set; }
		public PokerSection StTurn { get; set; }
		public PokerSection StRiver { get; set; }
		public Showdown StShowdown { get; set; }
		public NickNameLocation[] VNickNameLocation { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			LRoomId = parser.ParseField<long>(message, "lRoomId");
			LHandId = parser.ParseField<long>(message, "lHandId");
			SRoomName = parser.ParseField<string>(message, "sRoomName");
			LSb = parser.ParseField<long>(message, "lSb");
			LBb = parser.ParseField<long>(message, "lBb");
			LAnte = parser.ParseField<long>(message, "lAnte");
			IPlayerNum = parser.ParseField<int>(message, "iPlayerNum");
			VHoldCards = parser.ParseFieldRepeated<int>(message, "vHoldCards");
			SLocation = parser.ParseField<string>(message, "sLocation");
			IIsIns = parser.ParseField<int>(message, "iIsIns");
			LInsPot = parser.ParseField<long>(message, "lInsPot");
			StPreflop = parser.ParseMessage<PokerSection>(message, "stPreflop");
			StFlop = parser.ParseMessage<PokerSection>(message, "stFlop");
			StTurn = parser.ParseMessage<PokerSection>(message, "stTurn");
			StRiver = parser.ParseMessage<PokerSection>(message, "stRiver");
			StShowdown = parser.ParseMessage<Showdown>(message, "stShowdown");
			VNickNameLocation = parser.ParseMessageRepeated<NickNameLocation>(message, "vNickNameLocation");
		}
	}
}
