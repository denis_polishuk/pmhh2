using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class BuyInsuranceInfo : DomainModel
	{
		

		public long LPotID { get; set; }
		public INSURANCE_ACT EInsuranceAct { get; set; }
		public long LBetStacks { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			LPotID = parser.ParseField<long>(message, "lPotID");
			EInsuranceAct = parser.ParseEnum<INSURANCE_ACT>(message, "eInsuranceAct");
			LBetStacks = parser.ParseField<long>(message, "lBetStacks");
		}
	}
}
