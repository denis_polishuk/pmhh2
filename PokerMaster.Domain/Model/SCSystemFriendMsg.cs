using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class SCSystemFriendMsg : DomainModel
	{
		

		public FriendPushMsg[] VMsgs { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			VMsgs = parser.ParseMessageRepeated<FriendPushMsg>(message, "vMsgs");
		}
	}
}
