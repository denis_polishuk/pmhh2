using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class CSGetBillInfosV3 : DomainModel
	{
		

		public long Uuid { get; set; }
		public long LNewestTime { get; set; }
		public int INum { get; set; }
		public long LClubID { get; set; }
		public GAME_ROOM_TYPE EGameRoomType { get; set; }
		public string STimeZoneId { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			Uuid = parser.ParseField<long>(message, "uuid");
			LNewestTime = parser.ParseField<long>(message, "lNewestTime");
			INum = parser.ParseField<int>(message, "iNum");
			LClubID = parser.ParseField<long>(message, "lClubID");
			EGameRoomType = parser.ParseEnum<GAME_ROOM_TYPE>(message, "eGameRoomType");
			STimeZoneId = parser.ParseField<string>(message, "sTimeZoneId");
		}
	}
}
