using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class CSUploadVerifyCode : DomainModel
	{
		

		public UserBaseInfoNet StUserBaseInfo { get; set; }
		public string SVeryifyCode { get; set; }
		public string StrIosToken { get; set; }
		public string StrSSOToken { get; set; }
		public DEVICE_TYPE EDeviceType { get; set; }
		public LANGUAGE_TYPE ELanguageType { get; set; }
		public int BRegister { get; set; }
		public string SExtra { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			StUserBaseInfo = parser.ParseMessage<UserBaseInfoNet>(message, "stUserBaseInfo");
			SVeryifyCode = parser.ParseField<string>(message, "sVeryifyCode");
			StrIosToken = parser.ParseField<string>(message, "strIosToken");
			StrSSOToken = parser.ParseField<string>(message, "strSSOToken");
			EDeviceType = parser.ParseEnum<DEVICE_TYPE>(message, "eDeviceType");
			ELanguageType = parser.ParseEnum<LANGUAGE_TYPE>(message, "eLanguageType");
			BRegister = parser.ParseField<int>(message, "bRegister");
			SExtra = parser.ParseField<string>(message, "sExtra");
		}
	}
}
