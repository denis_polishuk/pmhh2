namespace PokerMaster.Domain.Model
{
	public enum INSURANCE_BET_STATUS
	{
		INSURANCE_BET_BETTING = 1,
		INSURANCE_BET_FOLD = 2,
		INSURANCE_BET_BETED = 3
	}
}
