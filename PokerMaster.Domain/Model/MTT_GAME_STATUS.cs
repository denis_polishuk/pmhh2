namespace PokerMaster.Domain.Model
{
	public enum MTT_GAME_STATUS
	{
		MTT_GAME_NOT_START = 1,
		MTT_GAME_START = 2,
		MTT_GAME_FINISH = 3
	}
}
