using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class PokerHistoryInfo : DomainModel
	{
		

		public long LSaveUuid { get; set; }
		public long LRoomID { get; set; }
		public long LHandID { get; set; }
		public string SPokerHistoryName { get; set; }
		public long LTime { get; set; }
		public long LSmallBlind { get; set; }
		public long LBigBlind { get; set; }
		public int BInGame { get; set; }
		public long LGetPopularity { get; set; }
		public string SPokerHistoryUrl { get; set; }
		public string SPokerHistoryRoomName { get; set; }
		public int IGameType { get; set; }
		public long LGroupId { get; set; }
		public int[] VHandCards { get; set; }
		public string SPokerHistoryShareUrl { get; set; }
		public int IAnte { get; set; }
		public long LMaxPot { get; set; }
		public string SPokerDesc { get; set; }
		public int IType { get; set; }
		public long LWinUuid { get; set; }
		public string SPokerAnonymousShareUrl { get; set; }
		public string SPokerHistoryAnonymousName { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			LSaveUuid = parser.ParseField<long>(message, "lSaveUuid");
			LRoomID = parser.ParseField<long>(message, "lRoomID");
			LHandID = parser.ParseField<long>(message, "lHandID");
			SPokerHistoryName = parser.ParseField<string>(message, "sPokerHistoryName");
			LTime = parser.ParseField<long>(message, "lTime");
			LSmallBlind = parser.ParseField<long>(message, "lSmallBlind");
			LBigBlind = parser.ParseField<long>(message, "lBigBlind");
			BInGame = parser.ParseField<int>(message, "bInGame");
			LGetPopularity = parser.ParseField<long>(message, "lGetPopularity");
			SPokerHistoryUrl = parser.ParseField<string>(message, "sPokerHistoryUrl");
			SPokerHistoryRoomName = parser.ParseField<string>(message, "sPokerHistoryRoomName");
			IGameType = parser.ParseField<int>(message, "iGameType");
			LGroupId = parser.ParseField<long>(message, "lGroupId");
			VHandCards = parser.ParseFieldRepeated<int>(message, "vHandCards");
			SPokerHistoryShareUrl = parser.ParseField<string>(message, "sPokerHistoryShareUrl");
			IAnte = parser.ParseField<int>(message, "iAnte");
			LMaxPot = parser.ParseField<long>(message, "lMaxPot");
			SPokerDesc = parser.ParseField<string>(message, "sPokerDesc");
			IType = parser.ParseField<int>(message, "iType");
			LWinUuid = parser.ParseField<long>(message, "lWinUuid");
			SPokerAnonymousShareUrl = parser.ParseField<string>(message, "sPokerAnonymousShareUrl");
			SPokerHistoryAnonymousName = parser.ParseField<string>(message, "sPokerHistoryAnonymousName");
		}
	}
}
