using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class CSUploadChatRoomIcon : DomainModel
	{
		

		public long LRoomID { get; set; }
		public byte[] VIconDatas { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			LRoomID = parser.ParseField<long>(message, "lRoomID");
			VIconDatas = parser.ParseByteArray(message, "vIconDatas");
		}
	}
}
