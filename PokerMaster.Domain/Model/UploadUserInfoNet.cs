using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class UploadUserInfoNet : DomainModel
	{
		

		public string StrIDMD5 { get; set; }
		public ID_TYPE EType { get; set; }
		public UPLOAD_ACTION_TYPE EUploadActionType { get; set; }
		public string SNickname { get; set; }
		public string SIcon { get; set; }
		public int IGender { get; set; }
		public string SPhonenumber { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			StrIDMD5 = parser.ParseField<string>(message, "strIDMD5");
			EType = parser.ParseEnum<ID_TYPE>(message, "eType");
			EUploadActionType = parser.ParseEnum<UPLOAD_ACTION_TYPE>(message, "eUploadActionType");
			SNickname = parser.ParseField<string>(message, "sNickname");
			SIcon = parser.ParseField<string>(message, "sIcon");
			IGender = parser.ParseField<int>(message, "iGender");
			SPhonenumber = parser.ParseField<string>(message, "sPhonenumber");
		}
	}
}
