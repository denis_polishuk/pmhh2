﻿using Google.ProtocolBuffers;
using Google.ProtocolBuffers.Serialization;
using PokerMaster.Domain.Proto;
using System;
using System.Globalization;
using System.IO;
using System.Text;

namespace PokerMaster.Domain.Model
{
    public abstract class DomainModel
    {
        public abstract void Parse(DynamicMessage message, ProtoParser parser);

        public void Parse(byte[] bytes, ProtoParser parser)
        {
            var message = parser.ParseMessage(bytes, GetType().Name);
            Parse(message, parser);
            //DumpMessage(message, GetType().Name);
        }


        private void DumpMessage(DynamicMessage dm, string messageType)
        {
            using (FileStream stream = new FileStream("_message_dump.txt", FileMode.Append))
            {
                byte[] header =
                    Encoding.UTF8.GetBytes(
                        $"\r\n{{ \"packet\": {{ \"type\": \"{messageType}\", \"time\": \"{DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff", CultureInfo.InvariantCulture)}\", \"data\": \r\n");
                stream.Write(header, 0, header.Length);

                JsonFormatWriter writer = JsonFormatWriter.CreateInstance(stream);
                writer.Formatted();
                writer.WriteMessage(dm);

                byte[] footer = Encoding.UTF8.GetBytes("\r\n}},\r\n");
                stream.Write(footer, 0, footer.Length);

                stream.Flush();
            }
        }


    }
}
