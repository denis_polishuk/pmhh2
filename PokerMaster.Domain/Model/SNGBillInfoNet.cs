using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class SNGBillInfoNet : DomainModel
	{
		

		public long LBillID { get; set; }
		public int BOwnerDeleted { get; set; }
		public string SPlayName { get; set; }
		public UserBaseInfoNet StCreateUserBaseInfo { get; set; }
		public long LCreateTime { get; set; }
		public string SFromText { get; set; }
		public CREATE_ROOM_TYPE ECreateRoomType { get; set; }
		public long LFromRoomID { get; set; }
		public UserBillInfoNet[] VUserBillInfos { get; set; }
		public int BPrivateRoom { get; set; }
		public long LOriginalStacks { get; set; }
		public int IBlindInterval { get; set; }
		public int IGameRoomUserMaxNums { get; set; }
		public SNG_ROOM_TYPE ESNGRoomType { get; set; }
		public long LMaxPot { get; set; }
		public long LTotalGameNum { get; set; }
		public long LLeagueID { get; set; }
		public string SLeagueName { get; set; }
		public GameUserStatisticsInfoNet StGameUserStatisticsInfoNet { get; set; }
		public long LProfitStacks { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			LBillID = parser.ParseField<long>(message, "lBillID");
			BOwnerDeleted = parser.ParseField<int>(message, "bOwnerDeleted");
			SPlayName = parser.ParseField<string>(message, "sPlayName");
			StCreateUserBaseInfo = parser.ParseMessage<UserBaseInfoNet>(message, "stCreateUserBaseInfo");
			LCreateTime = parser.ParseField<long>(message, "lCreateTime");
			SFromText = parser.ParseField<string>(message, "sFromText");
			ECreateRoomType = parser.ParseEnum<CREATE_ROOM_TYPE>(message, "eCreateRoomType");
			LFromRoomID = parser.ParseField<long>(message, "lFromRoomID");
			VUserBillInfos = parser.ParseMessageRepeated<UserBillInfoNet>(message, "vUserBillInfos");
			BPrivateRoom = parser.ParseField<int>(message, "bPrivateRoom");
			LOriginalStacks = parser.ParseField<long>(message, "lOriginalStacks");
			IBlindInterval = parser.ParseField<int>(message, "iBlindInterval");
			IGameRoomUserMaxNums = parser.ParseField<int>(message, "iGameRoomUserMaxNums");
			ESNGRoomType = parser.ParseEnum<SNG_ROOM_TYPE>(message, "eSNGRoomType");
			LMaxPot = parser.ParseField<long>(message, "lMaxPot");
			LTotalGameNum = parser.ParseField<long>(message, "lTotalGameNum");
			LLeagueID = parser.ParseField<long>(message, "lLeagueID");
			SLeagueName = parser.ParseField<string>(message, "sLeagueName");
			StGameUserStatisticsInfoNet = parser.ParseMessage<GameUserStatisticsInfoNet>(message, "stGameUserStatisticsInfoNet");
			LProfitStacks = parser.ParseField<long>(message, "lProfitStacks");
		}
	}
}
