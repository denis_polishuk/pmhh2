using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class ShareBillInfoNet : DomainModel
	{
		

		public BillInfoNet StBillInfo { get; set; }
		public long LShareUuid { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			StBillInfo = parser.ParseMessage<BillInfoNet>(message, "stBillInfo");
			LShareUuid = parser.ParseField<long>(message, "lShareUuid");
		}
	}
}
