using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class ChatRoomBaseInfo : DomainModel
	{
		

		public long LChatRoomId { get; set; }
		public string StrRoomName { get; set; }
		public int NMaxMembers { get; set; }
		public int NCurMembers { get; set; }
		public long LCreateUser { get; set; }
		public int IRoomType { get; set; }
		public int NOwnerInvite { get; set; }
		public int NOwnerCreate { get; set; }
		public string StrRoomIcon { get; set; }
		public string StrSmallRoomIcon { get; set; }
		public int IMute { get; set; }
		public int IVibrate { get; set; }
		public string StrRoomTempName { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			LChatRoomId = parser.ParseField<long>(message, "lChatRoomId");
			StrRoomName = parser.ParseField<string>(message, "strRoomName");
			NMaxMembers = parser.ParseField<int>(message, "nMaxMembers");
			NCurMembers = parser.ParseField<int>(message, "nCurMembers");
			LCreateUser = parser.ParseField<long>(message, "lCreateUser");
			IRoomType = parser.ParseField<int>(message, "iRoomType");
			NOwnerInvite = parser.ParseField<int>(message, "nOwnerInvite");
			NOwnerCreate = parser.ParseField<int>(message, "nOwnerCreate");
			StrRoomIcon = parser.ParseField<string>(message, "strRoomIcon");
			StrSmallRoomIcon = parser.ParseField<string>(message, "strSmallRoomIcon");
			IMute = parser.ParseField<int>(message, "iMute");
			IVibrate = parser.ParseField<int>(message, "iVibrate");
			StrRoomTempName = parser.ParseField<string>(message, "strRoomTempName");
		}
	}
}
