using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class SNGBlindsStructure : DomainModel
	{
		

		public long LSNGSmallBlinds { get; set; }
		public long LSNGBigBlinds { get; set; }
		public long LSNGAnte { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			LSNGSmallBlinds = parser.ParseField<long>(message, "lSNGSmallBlinds");
			LSNGBigBlinds = parser.ParseField<long>(message, "lSNGBigBlinds");
			LSNGAnte = parser.ParseField<long>(message, "lSNGAnte");
		}
	}
}
