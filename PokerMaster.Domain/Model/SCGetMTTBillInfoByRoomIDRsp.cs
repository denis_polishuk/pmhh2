using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class SCGetMTTBillInfoByRoomIDRsp : DomainModel
	{
		

		public ERROR_CODE_TYPE EErrCode { get; set; }
		public MTTBillInfoNet StMTTBillInfoNet { get; set; }
		public int IOffset { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			EErrCode = parser.ParseEnum<ERROR_CODE_TYPE>(message, "eErrCode");
			StMTTBillInfoNet = parser.ParseMessage<MTTBillInfoNet>(message, "stMTTBillInfoNet");
			IOffset = parser.ParseField<int>(message, "iOffset");
		}
	}
}
