using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class CSResetPwdVerifyCode : DomainModel
	{
		

		public string SPhoneNumber { get; set; }
		public string SVeryifyCode { get; set; }
		public string SNewPassword { get; set; }
		public string SCountryCode { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			SPhoneNumber = parser.ParseField<string>(message, "sPhoneNumber");
			SVeryifyCode = parser.ParseField<string>(message, "sVeryifyCode");
			SNewPassword = parser.ParseField<string>(message, "sNewPassword");
			SCountryCode = parser.ParseField<string>(message, "sCountryCode");
		}
	}
}
