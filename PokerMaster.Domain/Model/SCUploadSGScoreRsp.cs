using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class SCUploadSGScoreRsp : DomainModel
	{
		

		public ERROR_CODE_TYPE EErrCode { get; set; }
		public long LPopularity { get; set; }
		public long LMsg { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			EErrCode = parser.ParseEnum<ERROR_CODE_TYPE>(message, "eErrCode");
			LPopularity = parser.ParseField<long>(message, "lPopularity");
			LMsg = parser.ParseField<long>(message, "lMsg");
		}
	}
}
