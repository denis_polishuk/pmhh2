using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class SCUploadVerifyCodeRsp : DomainModel
	{
		

		public ERROR_CODE_TYPE EErrCode { get; set; }
		public UserBaseInfoNet StUserBaseInfo { get; set; }
		public DefaultSettingInfoNet StDefaultSettingInfo { get; set; }
		public string SAccessToken { get; set; }
		public string SEncryptKey { get; set; }
		public int BRegister { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			EErrCode = parser.ParseEnum<ERROR_CODE_TYPE>(message, "eErrCode");
			StUserBaseInfo = parser.ParseMessage<UserBaseInfoNet>(message, "stUserBaseInfo");
			StDefaultSettingInfo = parser.ParseMessage<DefaultSettingInfoNet>(message, "stDefaultSettingInfo");
			SAccessToken = parser.ParseField<string>(message, "sAccessToken");
			SEncryptKey = parser.ParseField<string>(message, "sEncryptKey");
			BRegister = parser.ParseField<int>(message, "bRegister");
		}
	}
}
