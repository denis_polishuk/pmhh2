using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class CSSetPreAction : DomainModel
	{
		

		public long Uuid { get; set; }
		public long LGameRoomID { get; set; }
		public long LBettingID { get; set; }
		public USER_GAME_STATE EPreUserGameState { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			Uuid = parser.ParseField<long>(message, "uuid");
			LGameRoomID = parser.ParseField<long>(message, "lGameRoomID");
			LBettingID = parser.ParseField<long>(message, "lBettingID");
			EPreUserGameState = parser.ParseEnum<USER_GAME_STATE>(message, "ePreUserGameState");
		}
	}
}
