using Google.ProtocolBuffers;

namespace PokerMaster.Domain.Model
{
	public class MTTUserBillInfoNet : DomainModel
	{
		

		public long Uuid { get; set; }
		public string SNickName { get; set; }
		public string SIcon { get; set; }
		public long IMTTRank { get; set; }
		public string SMTTObjectBonus { get; set; }
		public int IGender { get; set; }

		public override void Parse(DynamicMessage message, ProtoParser parser)
		{
			Uuid = parser.ParseField<long>(message, "uuid");
			SNickName = parser.ParseField<string>(message, "sNickName");
			SIcon = parser.ParseField<string>(message, "sIcon");
			IMTTRank = parser.ParseField<long>(message, "iMTTRank");
			SMTTObjectBonus = parser.ParseField<string>(message, "sMTTObjectBonus");
			IGender = parser.ParseField<int>(message, "iGender");
		}
	}
}
