﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Google.ProtocolBuffers;
using Google.ProtocolBuffers.Descriptors;
using PokerMaster.Domain.Model;
using PokerMaster.Domain.Proto;

namespace PokerMaster.Domain
{
    public class ProtoParser
    {
        public FileDescriptor Descriptor { get; }

        public ProtoParser()
        {
            var builder = new ProtoBuilder();
            Descriptor = FileDescriptor.BuildFrom(builder.Build(), new FileDescriptor[0]);
        }

        public DynamicMessage ParseMessage(byte[] bytes, string messageType)
        {
            var messageDesc = Descriptor.FindTypeByName<MessageDescriptor>(messageType);

            if (messageDesc != null)
            {
                DynamicMessage message = DynamicMessage.ParseFrom(messageDesc, bytes);

                return message;
            }

            throw new Exception($"Failed to find {messageType}");
        }

        public T Parse<T>(DynamicMessage message) where T : DomainModel, new()
        {
            T result = new T();
            result.Parse(message, this);
            return result;
        }

        private KeyValuePair<FieldDescriptor, object> GetField(DynamicMessage message, string fieldName)
        {
            return message.AllFields.FirstOrDefault(x => x.Key.Name == fieldName);
        } 

        public T ParseField<T>(DynamicMessage message, string fieldName)
        {
            var field = GetField(message, fieldName);

            if (field.Key == null) return default(T);

            return (T)field.Value;
        }

        public byte[] ParseByteArray(DynamicMessage message, string fieldName)
        {
            return ParseField<ByteString>(message, fieldName)?.ToByteArray();
        }

        public T ParseEnum<T>(DynamicMessage message, string fieldName) where T : struct
        {
            var field = GetField(message, fieldName);

            if (field.Key == null) return default(T);

            var en = (EnumValueDescriptor)field.Value;

            return (T)(object)en.Number;
        }

        public T ParseMessage<T>(DynamicMessage message, string fieldName) where T : DomainModel, new()
        {
            var field = GetField(message, fieldName);

            if (field.Key == null) return default(T);

            return Parse<T>(field.Value as DynamicMessage);
        }

        public T[] ParseFieldRepeated<T>(DynamicMessage message, string fieldName)
        {
            var field = GetField(message, fieldName);

            if (field.Key == null) return null;

            return ((ReadOnlyCollection<object>)field.Value).Select(x => (T)x).ToArray();
        }

        public byte[][] ParseByteArrayRepeated(DynamicMessage message, string fieldName)
        {
            return ParseFieldRepeated<ByteString>(message, fieldName)?.Select(x => x?.ToByteArray()).ToArray();
        }

        public T[] ParseEnumRepeated<T>(DynamicMessage message, string fieldName) where T : struct
        {
            var field = GetField(message, fieldName);

            if (field.Key == null) return null;

            var en = (ReadOnlyCollection<object>)field.Value;

            return en.Select(x => (T)(object)((EnumValueDescriptor)x).Number).ToArray();
        }

        public T[] ParseMessageRepeated<T>(DynamicMessage message, string fieldName) where T : DomainModel, new()
        {
            var field = GetField(message, fieldName);

            if (field.Key == null) return null;

            return ((IEnumerable<object>)field.Value).Select(x => Parse<T>((DynamicMessage)x)).ToArray();
        }
    }
}
