﻿using System;
using System.Diagnostics;
using System.Threading;
using PokerMaster.Engine;
using PokerMaster.Overlay;
using PokerMaster.Packets;
using PokerMaster.HandHistory;


namespace PokerMasterApp
{
    class Program
    {


        [STAThread]
        public static void Main(string[] args)
        {
#if DEBUG
            bool rep = true;
            if (!rep)
            {
                using (var engine = new Engine())
                {
                    engine.StartCapture(true, false);



                    while (true)
                    {
                        Thread.Sleep(1001);
                    }
                }
            }
            else
            {
                var replay = new Replay();
                replay.ReplayPackets("received");
                Console.WriteLine("Replay finished");
                HHSender.sendFilesToFTP(true);
                Thread.Sleep(112501);

            }

#else
            using (var engine = new Engine())
            {
                engine.StartCapture();



                while (true)
                {
                    Thread.Sleep(1001);
                }
            }
#endif



        }
    }
}
