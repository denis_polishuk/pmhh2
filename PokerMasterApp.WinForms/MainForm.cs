﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using PokerMaster.Engine;
using PokerMaster.Overlay;
using PokerMaster.Packets;
using SharpPcap;
using SharpPcap.AirPcap;
using SharpPcap.WinPcap;
using Timer = System.Windows.Forms.Timer;

namespace PokerMasterApp.WinForms
{
    public partial class MainForm : Form
    {
        private Engine Engine { get; } 
        private Timer Timer { get; set; }

        
        public MainForm()
        {
            InitializeComponent();

            string disableHUD = System.Configuration.ConfigurationManager.AppSettings["disableAutoHUD"];
            if (disableHUD == "1")
            {
                Engine = new Engine(false, InvokeAction);
            }
            else
            {
                Engine = new Engine(true, InvokeAction);
            }
            
            Console.SetOut(new ListBoxTextWriter(ConsoleListBox, InvokeAction));
        }

        void InvokeAction(Action action)
        {
            if (IsHandleCreated)
            {
                Invoke(action);
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            

            var devices = CaptureDeviceList.Instance;
            int i = 0;
            foreach (var dev in devices) {
                cb_devices.Items.Add(i.ToString() + " - " + dev.Description.ToString());
                i++;
            }

            var defaultAdapter = ConfigurationManager.AppSettings["default_network_adapter"];
            int defaultAdapterIndex;
            if(int.TryParse(defaultAdapter, out defaultAdapterIndex)) {
                if(cb_devices.Items.Count>cb_devices.SelectedIndex) 
                    cb_devices.SelectedIndex = defaultAdapterIndex;
            }


            if (System.Configuration.ConfigurationManager.AppSettings["sendHands"] == "1")
            {
                chb_sendHands.Checked = true;
            }else
            {
                chb_sendHands.Checked = false;
            }

            if (System.Configuration.ConfigurationManager.AppSettings["sendHandsHero"] == "1")
            {
                chb_sendHeroHands.Checked = true;
            }
            else
            {
                chb_sendHeroHands.Checked = false;
            }

            if (System.Configuration.ConfigurationManager.AppSettings["disableAutoHUD"] == "1")
            {
                chb_disableHUD.Checked = true;
                btn_updateHUD.Enabled = false;
            }
            else
            {
                chb_disableHUD.Checked = false;
            }



            tb_useName.Text = System.Configuration.ConfigurationManager.AppSettings["ftpUsername"];
            tb_password.Text = System.Configuration.ConfigurationManager.AppSettings["ftpPassword"];
            PokerMaster.HandHistory.HHSender.ftpUser = System.Configuration.ConfigurationManager.AppSettings["ftpUsername"];
            PokerMaster.HandHistory.HHSender.ftpPass = System.Configuration.ConfigurationManager.AppSettings["ftpPassword"];
            lbl_info.Text = "";

            Engine.StartCapture(chb_sendHands.Checked, chb_sendHeroHands.Checked);

            Timer = new Timer {Interval = 1000};
            Timer.Tick += Timer_Tick;
            Timer.Start();

            if (chb_sendHands.Checked)
            {
                int dlMiningEvery;
                if (!Int32.TryParse(ConfigurationManager.AppSettings["miningDLEverySec"], out dlMiningEvery))
                {
                    dlMiningEvery = 60;
                    Configuration config = ConfigurationManager.OpenExeConfiguration(Application.ExecutablePath);
                    config.AppSettings.Settings.Add("miningDLEverySec", "30");
                    config.Save(ConfigurationSaveMode.Minimal);
                }

#if !DEBUG
                if (dlMiningEvery < 180) {
                    dlMiningEvery = 180;
                }            
#endif

                timer_dlMining.Enabled = true;
                timer_dlMining.Interval = dlMiningEvery * 1000;
                timer_dlMining.Start();
            }
            



            //PokerMaster.HandHistory.HHSender.receiveFilesFromFTP();


        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            Engine.Maintenance();
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            Timer.Stop();
            Engine.Dispose();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                Engine.ProcessTracker?.ClearCache();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Failed to clear cache: {ex.Message}");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
        }

        private void button3_Click(object sender, EventArgs e)
        {

         
        }

        private void chb_sendHands_CheckedChanged(object sender, EventArgs e)
        {


            Configuration config = ConfigurationManager.OpenExeConfiguration(Application.ExecutablePath);
            config.AppSettings.Settings.Remove("sendHands");
            if (chb_sendHands.Checked)
            {
                config.AppSettings.Settings.Add("sendHands", "1");
            }
            else
            {
                config.AppSettings.Settings.Add("sendHands", "0");
            }
            lbl_info.Text = "Restart required for changes to take effect";
            config.Save(ConfigurationSaveMode.Minimal);

            
        }

        private void chb_sendHeroHands_CheckedChanged(object sender, EventArgs e)
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(Application.ExecutablePath);
            config.AppSettings.Settings.Remove("sendHandsHero");
            if (chb_sendHeroHands.Checked)
            {
                config.AppSettings.Settings.Add("sendHandsHero", "1");
            }
            else
            {
                config.AppSettings.Settings.Add("sendHandsHero", "0");
            }
            lbl_info.Text = "Restart required for changes to take effect";
            config.Save(ConfigurationSaveMode.Minimal);
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void tb_useName_TextChanged(object sender, EventArgs e)
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(Application.ExecutablePath);
            config.AppSettings.Settings.Remove("ftpUsername");
            config.AppSettings.Settings.Add("ftpUsername", tb_useName.Text);
            //lbl_info.Text = "Restart required for changes to take effect";
            PokerMaster.HandHistory.HHSender.ftpUser = tb_useName.Text;
            config.Save(ConfigurationSaveMode.Minimal);
        }

        private void tb_password_TextChanged(object sender, EventArgs e)
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(Application.ExecutablePath);
            config.AppSettings.Settings.Remove("ftpPassword");
            config.AppSettings.Settings.Add("ftpPassword", tb_password.Text);
            //lbl_info.Text = "Restart required for changes to take effect";
            PokerMaster.HandHistory.HHSender.ftpPass = tb_password.Text;
            config.Save(ConfigurationSaveMode.Minimal);
        }

        Timer updTestConnectionRes;
        int updTimes = 0;

        private void btn_testFtpConnection_Click(object sender, EventArgs e)
        {

            Thread ftp = new Thread(new ThreadStart(PokerMaster.HandHistory.HHSender.testConnection));
            btn_testFtpConnection.Enabled = false;
            PokerMaster.HandHistory.HHSender.testConnectionResult = null;
            lbl_ftpTest.Text = "";
            ftp.Start();



            updTestConnectionRes = new Timer();
            updTestConnectionRes.Interval = 1000;
            updTestConnectionRes.Tick += updateTestConnectionResults;
            updTestConnectionRes.Start();
            updTimes = 0;


        }

        private void updateTestConnectionResults(object sender, EventArgs e)
        {
            if (PokerMaster.HandHistory.HHSender.testConnectionResult == true)
            {
                lbl_ftpTest.Text = "Connection ok";
                updTestConnectionRes.Dispose();
                
                btn_testFtpConnection.Enabled = true;
            }
            else if((PokerMaster.HandHistory.HHSender.testConnectionResult == false)||(updTimes > 1000))
            {
                lbl_ftpTest.Text = "Connection failed";
                updTestConnectionRes.Dispose();
                btn_testFtpConnection.Enabled = true;
            }
            else
            {
                updTimes++;
            }
        }


        private void cb_devices_SelectedIndexChanged(object sender, EventArgs e)
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(Application.ExecutablePath);
            config.AppSettings.Settings.Remove("default_network_adapter");
            config.AppSettings.Settings.Add("default_network_adapter", cb_devices.SelectedIndex.ToString());
            lbl_info.Text = "Restart required for changes to take effect";
            config.Save(ConfigurationSaveMode.Minimal);
        }

        private void chb_disableHUD_CheckedChanged(object sender, EventArgs e)
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(Application.ExecutablePath);
            config.AppSettings.Settings.Remove("disableAutoHUD");
            if (chb_disableHUD.Checked)
            {
                config.AppSettings.Settings.Add("disableAutoHUD", "1");
            }
            else
            {
                config.AppSettings.Settings.Add("disableAutoHUD", "0");
            }
            lbl_info.Text = "Restart required for changes to take effect";
            config.Save(ConfigurationSaveMode.Minimal);
        }

        Timer dlMiningTimer;
        int dlTimes = 0;

        private void timer_dlMining_Tick(object sender, EventArgs e)
        {
            //ConsoleListBox.Items.Add("Connecting to FTP for");
            Thread ftp = new Thread(new ThreadStart(PokerMaster.HandHistory.HHSender.receiveFilesFromFTP));
            ftp.Start();

            Thread ftpStraddle = new Thread(new ThreadStart(PokerMaster.HandHistory.HHSender.receiveFilesFromFTPStraddle));
            ftpStraddle.Start();

            PokerMaster.HandHistory.HHSender.dlFromFTPError = null;
            dlTimes = 0;
            dlMiningTimer = new Timer();
            dlMiningTimer.Enabled = true;
            dlMiningTimer.Tick += updateDlResults;
            dlMiningTimer.Start();

        }

        private void updateDlResults(object sender, EventArgs e)
        {
            if (PokerMaster.HandHistory.HHSender.dlFromFTPError == false)
            {
                ConsoleListBox.Items.Add("Ftp download successful");
                dlMiningTimer.Stop();
                dlMiningTimer.Dispose();
            }
            else if ((PokerMaster.HandHistory.HHSender.dlFromFTPError == true)||(dlTimes>1000))
            {
                lbl_info.Text = "Error. Ftp connection problem. Files were not downloaded";
                dlMiningTimer.Stop();
                dlMiningTimer.Dispose();
            }
            else if(PokerMaster.HandHistory.HHSender.dlFromFTPError == null)
            {
                dlTimes++;
            }
        }
    }
}
