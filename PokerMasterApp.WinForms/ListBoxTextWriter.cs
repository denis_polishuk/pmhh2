﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PokerMasterApp.WinForms
{
    public class ListBoxTextWriter : TextWriter
    {
        private readonly ListBox _list;
        private readonly Action<Action> _uiInvoke;
        private StringBuilder _content = new StringBuilder();

        public ListBoxTextWriter(ListBox listBox, Action<Action> uiInvoke)
        {
            _list = listBox;
            _uiInvoke = uiInvoke;
        }

        public override void Write(char value)
        {
            base.Write(value);
            _content.Append(value);
            if (value == '\n')
            {
                _uiInvoke(() =>
                {
                    _list.Items.Add(_content.ToString());
                    _list.SelectedIndex = _list.Items.Count - 1;
                });

                _content = new StringBuilder();
            }
        }

        public override Encoding Encoding => Encoding.UTF8;
    }
}
