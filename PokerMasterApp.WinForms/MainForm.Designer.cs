﻿namespace PokerMasterApp.WinForms
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btn_updateHUD = new System.Windows.Forms.Button();
            this.ConsoleListBox = new System.Windows.Forms.ListBox();
            this.chb_sendHands = new System.Windows.Forms.CheckBox();
            this.chb_sendHeroHands = new System.Windows.Forms.CheckBox();
            this.tb_useName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tb_password = new System.Windows.Forms.TextBox();
            this.btn_testFtpConnection = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lbl_ftpTest = new System.Windows.Forms.Label();
            this.cb_devices = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.lbl_info = new System.Windows.Forms.Label();
            this.chb_disableHUD = new System.Windows.Forms.CheckBox();
            this.timer_dlMining = new System.Windows.Forms.Timer(this.components);
            this.flowLayoutPanel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.btn_updateHUD);
            this.flowLayoutPanel1.Controls.Add(this.ConsoleListBox);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.flowLayoutPanel1.Size = new System.Drawing.Size(806, 456);
            this.flowLayoutPanel1.TabIndex = 1;
            // 
            // btn_updateHUD
            // 
            this.btn_updateHUD.Location = new System.Drawing.Point(3, 3);
            this.btn_updateHUD.Name = "btn_updateHUD";
            this.btn_updateHUD.Size = new System.Drawing.Size(800, 50);
            this.btn_updateHUD.TabIndex = 1;
            this.btn_updateHUD.Text = "Update HUD";
            this.btn_updateHUD.UseVisualStyleBackColor = true;
            this.btn_updateHUD.Click += new System.EventHandler(this.button1_Click);
            // 
            // ConsoleListBox
            // 
            this.ConsoleListBox.FormattingEnabled = true;
            this.ConsoleListBox.Location = new System.Drawing.Point(3, 59);
            this.ConsoleListBox.Name = "ConsoleListBox";
            this.ConsoleListBox.Size = new System.Drawing.Size(800, 394);
            this.ConsoleListBox.TabIndex = 0;
            // 
            // chb_sendHands
            // 
            this.chb_sendHands.AutoSize = true;
            this.chb_sendHands.Checked = true;
            this.chb_sendHands.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chb_sendHands.Location = new System.Drawing.Point(13, 29);
            this.chb_sendHands.Name = "chb_sendHands";
            this.chb_sendHands.Size = new System.Drawing.Size(199, 17);
            this.chb_sendHands.TabIndex = 2;
            this.chb_sendHands.Text = "Отправлять историю раздач в пул";
            this.chb_sendHands.UseVisualStyleBackColor = true;
            this.chb_sendHands.CheckedChanged += new System.EventHandler(this.chb_sendHands_CheckedChanged);
            // 
            // chb_sendHeroHands
            // 
            this.chb_sendHeroHands.AutoSize = true;
            this.chb_sendHeroHands.Location = new System.Drawing.Point(13, 52);
            this.chb_sendHeroHands.Name = "chb_sendHeroHands";
            this.chb_sendHeroHands.Size = new System.Drawing.Size(273, 17);
            this.chb_sendHeroHands.TabIndex = 3;
            this.chb_sendHeroHands.Text = "Отправлять раздачи, в которых участвовал Hero";
            this.chb_sendHeroHands.UseVisualStyleBackColor = true;
            this.chb_sendHeroHands.CheckedChanged += new System.EventHandler(this.chb_sendHeroHands_CheckedChanged);
            // 
            // tb_useName
            // 
            this.tb_useName.Location = new System.Drawing.Point(105, 97);
            this.tb_useName.Name = "tb_useName";
            this.tb_useName.Size = new System.Drawing.Size(196, 20);
            this.tb_useName.TabIndex = 4;
            this.tb_useName.TextChanged += new System.EventHandler(this.tb_useName_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 100);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Usename";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 135);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Password";
            // 
            // tb_password
            // 
            this.tb_password.Location = new System.Drawing.Point(105, 132);
            this.tb_password.Name = "tb_password";
            this.tb_password.PasswordChar = '*';
            this.tb_password.Size = new System.Drawing.Size(196, 20);
            this.tb_password.TabIndex = 6;
            this.tb_password.TextChanged += new System.EventHandler(this.tb_password_TextChanged);
            // 
            // btn_testFtpConnection
            // 
            this.btn_testFtpConnection.Location = new System.Drawing.Point(13, 163);
            this.btn_testFtpConnection.Name = "btn_testFtpConnection";
            this.btn_testFtpConnection.Size = new System.Drawing.Size(288, 23);
            this.btn_testFtpConnection.TabIndex = 8;
            this.btn_testFtpConnection.Text = "Test Connection";
            this.btn_testFtpConnection.UseVisualStyleBackColor = true;
            this.btn_testFtpConnection.Click += new System.EventHandler(this.btn_testFtpConnection_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lbl_ftpTest);
            this.groupBox1.Controls.Add(this.tb_useName);
            this.groupBox1.Controls.Add(this.chb_sendHeroHands);
            this.groupBox1.Controls.Add(this.btn_testFtpConnection);
            this.groupBox1.Controls.Add(this.chb_sendHands);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.tb_password);
            this.groupBox1.Location = new System.Drawing.Point(812, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(320, 222);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Hand History sharing settings";
            // 
            // lbl_ftpTest
            // 
            this.lbl_ftpTest.AutoSize = true;
            this.lbl_ftpTest.Location = new System.Drawing.Point(16, 200);
            this.lbl_ftpTest.Name = "lbl_ftpTest";
            this.lbl_ftpTest.Size = new System.Drawing.Size(0, 13);
            this.lbl_ftpTest.TabIndex = 9;
            // 
            // cb_devices
            // 
            this.cb_devices.FormattingEnabled = true;
            this.cb_devices.Location = new System.Drawing.Point(825, 266);
            this.cb_devices.Name = "cb_devices";
            this.cb_devices.Size = new System.Drawing.Size(288, 21);
            this.cb_devices.TabIndex = 10;
            this.cb_devices.SelectedIndexChanged += new System.EventHandler(this.cb_devices_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(822, 250);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(96, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Сетевой адаптер:";
            // 
            // lbl_info
            // 
            this.lbl_info.AutoSize = true;
            this.lbl_info.ForeColor = System.Drawing.Color.Red;
            this.lbl_info.Location = new System.Drawing.Point(822, 440);
            this.lbl_info.Name = "lbl_info";
            this.lbl_info.Size = new System.Drawing.Size(0, 13);
            this.lbl_info.TabIndex = 12;
            // 
            // chb_disableHUD
            // 
            this.chb_disableHUD.AutoSize = true;
            this.chb_disableHUD.Location = new System.Drawing.Point(825, 311);
            this.chb_disableHUD.Name = "chb_disableHUD";
            this.chb_disableHUD.Size = new System.Drawing.Size(111, 17);
            this.chb_disableHUD.TabIndex = 13;
            this.chb_disableHUD.Text = "disable Auto HUD";
            this.chb_disableHUD.UseVisualStyleBackColor = true;
            this.chb_disableHUD.CheckedChanged += new System.EventHandler(this.chb_disableHUD_CheckedChanged);
            // 
            // timer_dlMining
            // 
            this.timer_dlMining.Tick += new System.EventHandler(this.timer_dlMining_Tick);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1144, 473);
            this.Controls.Add(this.chb_disableHUD);
            this.Controls.Add(this.lbl_info);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.cb_devices);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Name = "MainForm";
            this.Text = "Poker Master App";
            this.flowLayoutPanel1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button btn_updateHUD;
        private System.Windows.Forms.ListBox ConsoleListBox;
        private System.Windows.Forms.CheckBox chb_sendHands;
        private System.Windows.Forms.CheckBox chb_sendHeroHands;
        private System.Windows.Forms.TextBox tb_useName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tb_password;
        private System.Windows.Forms.Button btn_testFtpConnection;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox cb_devices;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lbl_info;
        private System.Windows.Forms.Label lbl_ftpTest;
        private System.Windows.Forms.CheckBox chb_disableHUD;
        private System.Windows.Forms.Timer timer_dlMining;
    }
}

