﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml.XPath;
using Google.ProtocolBuffers.Collections;
using Google.ProtocolBuffers.DescriptorProtos;
using Google.ProtocolBuffers.Descriptors;
using static Google.ProtocolBuffers.DescriptorProtos.FieldDescriptorProto.Types;

namespace ProtoToCsGenerator
{
    public class LuaParser
    {
        private string ToCamelCase(string value)
        {
            var split = value.Split('_');
            StringBuilder builder = new StringBuilder();
            foreach (var s in split)
            {
                builder.Append(char.ToUpper(s[0]));
                builder.Append(s.Substring(1));
            }

            return builder.ToString();
        }

        private string AddBuild(string values)
        {
            if (values.Length < 4) return values;

            return values.Replace(",", ".Build(),").Replace("}", ".Build()}");
        }

        public string Parse(string luaPath, List<string> messages, List<string> enums)
        {
            var lines = File.ReadAllLines(luaPath);

            StringBuilder builder = new StringBuilder();

            bool variablesSet = false;

            foreach (var l in lines)
            {
                if (l.StartsWith("-- ")) continue;
                if (l.StartsWith("local ")) continue;
                if (l.StartsWith("module(")) continue;
                if (string.IsNullOrWhiteSpace(l)) continue;

                var split = l.Split('=');
                var leftSide = split[0].Trim();
                var rightSide = split[1].Trim();

                if (leftSide.Contains("."))
                {
                    variablesSet = true;

                    if (leftSide.EndsWith(".full_name") || leftSide.EndsWith(".index") || leftSide.EndsWith(".cpp_type") || leftSide.EndsWith(".extensions") || leftSide.EndsWith(".is_extendable") || leftSide.EndsWith(".has_default_value") || leftSide.EndsWith(".default_value")) continue;

                    var lSplit = leftSide.Split('.');

                    if (leftSide.EndsWith(".values"))
                    {
                        builder.Append(lSplit[0]);
                        builder.Append(".AddRangeValue(new List<EnumValueDescriptorProto> ");
                        builder.Append(AddBuild(rightSide));
                        builder.AppendLine(");");
                    }
                    else if (leftSide.EndsWith(".fields"))
                    {
                        builder.Append(lSplit[0]);
                        builder.Append(".AddRangeField(new List<FieldDescriptorProto> ");
                        builder.Append(AddBuild(rightSide));
                        builder.AppendLine(");");
                    }
                    else if (leftSide.EndsWith(".nested_types"))
                    {
                        builder.Append(lSplit[0]);
                        builder.Append(".AddRangeNestedType(new List<DescriptorProto> ");
                        builder.Append(AddBuild(rightSide));
                        builder.AppendLine(");");
                    }
                    else if (leftSide.EndsWith(".enum_types"))
                    {
                        builder.Append(lSplit[0]);
                        builder.Append(".AddRangeEnumType(new List<EnumDescriptorProto> ");
                        builder.Append(AddBuild(rightSide));
                        builder.AppendLine(");");
                    }
                    else if (leftSide.EndsWith(".enum_type") || leftSide.EndsWith(".message_type"))
                    {
                        builder.Append(lSplit[0]);
                        builder.Append(".TypeName = ");
                        builder.Append(rightSide.Split('.').Last());
                        builder.AppendLine(".Name;");
                    }
                    else if (leftSide.EndsWith(".type"))
                    {
                        builder.Append(lSplit[0]);
                        builder.Append(".Type = (FieldDescriptorProto.Types.Type)");
                        builder.Append(rightSide);
                        builder.AppendLine(";");
                    }
                    else if (leftSide.EndsWith(".label"))
                    {
                        builder.Append(lSplit[0]);
                        builder.Append(".Label = (FieldDescriptorProto.Types.Label)");
                        builder.Append(rightSide);
                        builder.AppendLine(";");
                    }
                    else 
                    {
                        builder.Append(lSplit[0]);
                        builder.Append('.');
                        builder.Append(ToCamelCase(lSplit[1]));

                        builder.Append(" = ");
                        builder.Append(rightSide);
                        builder.AppendLine(";");
                    }
                }
                else
                {
                    if (variablesSet) continue;

                    if (rightSide.StartsWith("protobuf.EnumDescriptor()")) enums.Add(leftSide);
                    else if (rightSide.StartsWith("protobuf.Descriptor()")) messages.Add(leftSide); 

                    builder.Append("var ");
                    builder.Append(leftSide);
                    builder.Append(" = ");

                    var r = rightSide.Replace("protobuf.", "").Replace("();", "Proto.CreateBuilder();");
                    builder.AppendLine(r);
                }
            }

           
            
            return builder.ToString();
        }

        public string ParseAll(string luaDir)
        {
            StringBuilder builder = new StringBuilder();

            builder.AppendLine("using System.Collections.Generic;");
            builder.AppendLine("using Google.ProtocolBuffers.DescriptorProtos;");

            builder.AppendLine("namespace PokerMaster.Domain.Proto");
            builder.AppendLine("{");
            builder.AppendLine("\tpublic class ProtoBuilder");
            builder.AppendLine("\t{");
            builder.AppendLine("\t\tpublic FileDescriptorProto Build()");
            builder.AppendLine("\t\t{");
    
            List<string> messages = new List<string>();
            List<string> enums = new List<string>();

            builder.AppendLine(Parse(Path.Combine(luaDir, "TexasPokerCmd_pb.lua"), messages, enums));
            builder.AppendLine(Parse(Path.Combine(luaDir, "TexasPokerCommon_pb.lua"), messages, enums));
            builder.AppendLine(Parse(Path.Combine(luaDir, "TexasPokerHttpUser_pb.lua"), messages, enums));
            builder.AppendLine(Parse(Path.Combine(luaDir, "TexasPokerHttpBill_pb.lua"), messages, enums));
            builder.AppendLine(Parse(Path.Combine(luaDir, "TexasPokerHttpBillMTT_pb.lua"), messages, enums));
            builder.AppendLine(Parse(Path.Combine(luaDir, "TexasPoker_pb.lua"), messages, enums));

            builder.AppendLine();
            builder.AppendLine("var proto = FileDescriptorProto.CreateBuilder();");

            builder.Append("proto.AddRangeEnumType(new List<EnumDescriptorProto>() {");
            builder.Append(string.Join(", ", enums.Select(x => x + ".Build()")));
            builder.AppendLine("});");

            builder.Append("proto.AddRangeMessageType(new List<DescriptorProto>() {");
            builder.Append(string.Join(", ", messages.Select(x => x + ".Build()")));
            builder.AppendLine("});");

            builder.AppendLine("");

            builder.AppendLine("return proto.Build();");
            builder.AppendLine("\t\t}");
            builder.AppendLine("\t}");
            builder.AppendLine("}");

            var result = builder.ToString().Split(new [] { "\r\n" }, StringSplitOptions.None).ToList();
            for (int i = 0; i < result.Count; i++)
            {
                var l = result[i].Split('=');
                if (l.Length < 2) continue;

                var s = l[0].Split('.');

                if (s.Length > 1 && s[1] == "Name ")
                {
                    for (int j = 0; j < i; j++)
                    {
                        if (result[j].StartsWith("var " + s[0] + " = "))
                        {
                            var temp = result[i];
                            result.RemoveAt(i);
                            result.Insert(j + 1, temp);
                            break;
                        }
                    }
                }
            }

            var res =  string.Join("\r\n", result);

            return res;
        }
    }
}
