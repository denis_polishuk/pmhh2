﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Google.ProtocolBuffers.DescriptorProtos;
using Google.ProtocolBuffers.Descriptors;
using PokerMaster.Domain;

namespace ProtoToCsGenerator
{
    public class Generator
    {
        private string GetFieldType(FieldDescriptor descriptor)
        {
            switch (descriptor.FieldType.ToString())
            {
                case "Int32":
                    return "int";
                case "Int64":
                    return "long";
                case "Bytes":
                    return "byte[]";
                case "String":
                    return "string";
                case "Enum":
                    return descriptor.EnumType.Name;
                case "Message":
                    return descriptor.MessageType.Name;
                default:
                    throw new Exception("Unknown type");
            }
        }

        private void GenerateFields(MessageDescriptor messageDescriptor, StringBuilder builder)
        {
            foreach (var f in messageDescriptor.Fields)
            {
                var fieldType = GetFieldType(f);
                builder.Append("\t\tpublic ");
                builder.Append(fieldType);
                if (f.IsRepeated) builder.Append("[]");
                builder.Append(" ");
                builder.Append(f.CSharpOptions.PropertyName);
                builder.AppendLine(" { get; set; }");
            }
        }

        private void GenerateFieldSetter(FieldDescriptor f, StringBuilder builder)
        {
            builder.Append($"\t\t\t{f.CSharpOptions.PropertyName} = parser.");

            var fieldType = f.FieldType.ToString();

            switch (fieldType)
            {
                case "Message":
                    builder.AppendLine($"ParseMessage{(f.IsRepeated ? "Repeated" : "")}<{GetFieldType(f)}>(message, \"{f.Name}\");");
                    break;
                case "Enum":
                    builder.AppendLine($"ParseEnum{(f.IsRepeated ? "Repeated" : "")}<{GetFieldType(f)}>(message, \"{f.Name}\");");
                    break;
                case "Bytes":
                    builder.AppendLine($"ParseByteArray{(f.IsRepeated ? "Repeated" : "")}(message, \"{f.Name}\");");
                    break;
                default:
                    builder.AppendLine($"ParseField{(f.IsRepeated ? "Repeated" : "")}<{GetFieldType(f)}>(message, \"{f.Name}\");");
                    break;
            }
        }

        private void GenerateParseMethod(MessageDescriptor messageDescriptor, StringBuilder builder)
        {
            builder.AppendLine("\t\tpublic override void Parse(DynamicMessage message, ProtoParser parser)");
            builder.AppendLine("\t\t{");
            foreach (var f in messageDescriptor.Fields)
            {
                GenerateFieldSetter(f, builder);
            }
            builder.AppendLine("\t\t}");
        }

        private void WriteHeader(StringBuilder builder, bool addUsings = true)
        {
            if (addUsings)
            {
                builder.AppendLine("using Google.ProtocolBuffers;");
                builder.AppendLine();
            }

            builder.AppendLine("namespace PokerMaster.Domain.Model");
            builder.AppendLine("{");
        }

        private void WriteFooter(StringBuilder builder)
        {
            builder.AppendLine("\t}");
            builder.AppendLine("}");
        }

        public string Generate(MessageDescriptor messageDescriptor)
        {
            var builder = new StringBuilder();

            WriteHeader(builder);

            builder.AppendLine($"\tpublic class {messageDescriptor.Name} : DomainModel");
            builder.AppendLine("\t{");

            GenerateFields(messageDescriptor, builder);

            builder.AppendLine();

            GenerateParseMethod(messageDescriptor, builder);

            WriteFooter(builder);

            var res = builder.ToString();
            return res;
        }

        private void GenerateEnumValue(EnumValueDescriptor value, StringBuilder builder, bool isLast = false)
        {
            builder.Append($"\t\t{value.Name} = {value.Number}");

            if (!isLast)
                builder.Append(",");

            builder.AppendLine();
        }

        public string Generate(EnumDescriptor enumDescriptor)
        {
            var builder = new StringBuilder();

            WriteHeader(builder, false);

            builder.AppendLine($"\tpublic enum {enumDescriptor.Name}");
            builder.AppendLine("\t{");

            var last = enumDescriptor.Values.Last();

            foreach (var v in enumDescriptor.Values)
            {
                GenerateEnumValue(v, builder, v == last);
            }

            WriteFooter(builder);

            var res = builder.ToString();
            return res;
        }

        private void SaveFile(string code, string outDirectory, string name)
        {
            var path = Path.Combine(outDirectory, $"{name}.cs");

            if (File.Exists(path)) throw new Exception($"File already exists: {name} in {outDirectory}");

            File.WriteAllText(path, code);
        }

        public void GenerateClasses(FileDescriptor fileDescriptor, string outDirectory)
        {
            foreach (var m in fileDescriptor.MessageTypes)
            {
                var code = Generate(m);
                SaveFile(code, outDirectory, m.Name);
            }

            foreach (var m in fileDescriptor.EnumTypes)
            {
                var code = Generate(m);
                SaveFile(code, outDirectory, m.Name);
            }
        }

        public void GenerateAll(string outputDirectory)
        {
            var parser = new ProtoParser();
            GenerateClasses(parser.Descriptor, outputDirectory);
        }

        public void GenerateAll(FileDescriptorProto proto, string outputDirectory)
        {
            GenerateClasses(FileDescriptor.BuildFrom(proto, new FileDescriptor[0]), outputDirectory);
        }
    }
}
