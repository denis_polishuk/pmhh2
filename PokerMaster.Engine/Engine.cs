﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using PokerMaster.Overlay;
using PokerMaster.Packets;
using SharpPcap;
using SharpPcap.WinPcap;

namespace PokerMaster.Engine
{
    public class Engine : IDisposable
    {
        private static string[] _ips = { "47.89.247.177", "47.91.91.52", "47.52.92.161" };

        private readonly object _queueLock = new object();
        private List<RawCapture> _packetQueue = new List<RawCapture>();
        private bool _backgroundThreadStop = false;
        private bool _trackProcesses;
        private Thread _parseThread;
        private Thread _captureThread;
        private WindowManager _windowManager;
        private ProcessTracker _processTracker;
        private MessageProcessor _receiveProcessor;
        
        private WinPcapDevice _device;

        public ProcessTracker ProcessTracker { get { return _processTracker; } }

        private Action<Action> InvokeAction { get; }

        public Engine(bool trackProcesses = false, Action<Action> invokeAction = null)
        {
            _trackProcesses = trackProcesses;
            InvokeAction = invokeAction;

            if (trackProcesses)
            {
                _windowManager = new WindowManager("ATL:09B24C38", InvokeAction);
                _processTracker = new ProcessTracker(_windowManager);
            }
        }

        public void StartCapture(bool sendHH = false, bool sendHeroHH = false)
        {
            if (_trackProcesses)
                _processTracker.ScanProcesses();

            _receiveProcessor = new MessageProcessor(_processTracker, "received", sendHH, sendHeroHH);

            /* Retrieve the device list */
            var devices = CaptureDeviceList.Instance;

            /*If no device exists, print error */
            if (devices.Count < 1)
            {
                Console.WriteLine("No device found on this machine");
                return;
            }

            int i = 0;

            if (devices.Count > 1)
            {
                var defaultAdapter = ConfigurationManager.AppSettings["default_network_adapter"];
                if (!int.TryParse(defaultAdapter, out i) || i >= devices.Count)
                {
                    Console.WriteLine("The following devices are available on this machine:");
                    Console.WriteLine("----------------------------------------------------");
                    Console.WriteLine();

                    /* Scan the list printing every entry */
                    foreach (var dev in devices)
                    {
                        /* Description */
                        Console.WriteLine("{0}) {1} {2}", i, dev.Name, dev.Description);
                        i++;
                    }

                    Console.WriteLine();
                    Console.WriteLine("-- Please choose a device to capture. Copy its index to config file and restart the application");
                    return;
                }
            }

            _device = (WinPcapDevice)devices[i];

            _parseThread = new Thread(Parse);
            _parseThread.Start();

            _captureThread = new Thread(Capture);
            _captureThread.Start();
        }

        public void Maintenance()
        {
            if (_trackProcesses)
            {
                _processTracker.ScanProcesses();
                _windowManager.Maintenance();
                if (_windowManager.clearCacheNeeded)
                {
                    _processTracker.ClearCache();
                    _windowManager.clearCacheNeeded = false;
                }
                    
            }
        }

        /// <summary>
        /// Prints the time, length, src ip, src port, dst ip and dst port
        /// for each TCP/IP packet received on the network
        /// </summary>
        private void device_OnPacketArrival(CaptureEventArgs e)
        {
            lock (_queueLock)
            {
                _packetQueue.Add(e.Packet);
            }
        }

        private void Parse()
        {
            while (!_backgroundThreadStop)
            {
                bool shouldSleep = true;

                lock (_queueLock)
                {
                    if (_packetQueue.Count != 0)
                    {
                        shouldSleep = false;
                    }
                }

                if (shouldSleep)
                {
                    Thread.Sleep(250);
                }
                else // should process the queue
                {
                    List<RawCapture> ourQueue;
                    lock (_queueLock)
                    {
                        // swap queues, giving the capture callback a new one
                        ourQueue = _packetQueue;
                        _packetQueue = new List<RawCapture>();
                    }

                    foreach (var e in ourQueue)
                    {
                        var packet = PacketDotNet.Packet.ParsePacket(e.LinkLayerType, e.Data);

                        var tcpPacket = (PacketDotNet.TcpPacket)packet.Extract(typeof(PacketDotNet.TcpPacket));
                        if (tcpPacket != null)
                        {
                            int dstPort = tcpPacket.DestinationPort;

                            if (tcpPacket.PayloadData.Length > 0)
                            {
                                _receiveProcessor.OnPacket(dstPort, tcpPacket.PayloadData);
                            }
                        }
                    }
                }
            }
        }

        private void Capture()
        {
            //Register our handler function to the 'packet arrival' event
            _device.OnPacketArrival +=
                (sender, eventArgs) => device_OnPacketArrival(eventArgs);

            // Open the device for capturing
            int readTimeoutMilliseconds = 1000;
            _device.Open(DeviceMode.Normal, readTimeoutMilliseconds);

            //tcpdump filter to capture only TCP/IP packets
            string filter = "ip and tcp and (src net " + string.Join(") or (src net ", _ips) + ")";
            _device.Filter = filter;

            Console.WriteLine();
            #if DEBUG
                Console.WriteLine
                    ("-- The following tcpdump filter will be applied: \"{0}\"",
                        filter);
            #endif
            Console.WriteLine
                ("-- Listening on {0}, hit 'Ctrl-C' to exit...",
                    _device.Description);


            _device.Capture();
        }

        public void Dispose()
        {
            try
            {
                _receiveProcessor.Dispose();
            }
            catch
            {
                // ignore
            }

            try
            {
                _windowManager.Dispose();
            }
            catch
            {
                // ignore
            }

            try
            {
                _device.StopCapture();
            }
            catch
            {
                // ignore 
            }

            try
            {
                _backgroundThreadStop = true;
                _parseThread.Join(5000);
            }
            catch
            {
                // ignore
            }

            try
            {
                _device.Close();
            }
            catch
            {
                // ignore 
            }
        }
    }
}
